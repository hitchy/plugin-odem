---
prev: false
next: glossary.md
---

# About

[Hitchy](https://hitchy.org/) is a server-side framework purely written in Javascript. It consists of a core which comes with limited abilities as it is basically capable of essentially handling requests via HTTP and discovering plugins to provide some actual request handlers.

The **Odem** plugin is adding a document-oriented database to a Hitchy-based project. It does not feature any request handlers, but provides an API for managing structured data on server-side. 

* It focuses on non-relational data. 
* It intends to provide a simple way for defining data structures.
* It supports an API to adopt database APIs for persistently storing structured data.
* It offers a dedicated API for accessing structured data using server-side code.

:::tip Remote access support
APIs for remotely accessing this database are implemented by dedicated plugins you need to add to your project. 

* An HTTP-based REST API is implemented by [@hitchy/plugin-odem-rest](https://www.npmjs.com/package/@hitchy/plugin-odem-rest). 
* Accessing data via some web socket is provided by [@hitchy/plugin-odem-socket.io](https://www.npmjs.com/package/@hitchy/plugin-odem-socket.io).
:::

## Structured data?

In a document-oriented database you define types of data often referred to as _classes_ or _models_. Every such model describes a collection of _documents_, _instances_ or _items_ which are simply records of actual data each complying with a certain structure which is part of a model's definition. It is a set of _properties_, each of a certain _type_ of information and with optionally applied _constraints_. On handling items of a model, every property of that model has an individual value for either item. 

::: tip Example
The quite common example for implementing a simple web application is a blog. A blog has _posts_, _comments_ and _users_ that act in different _roles_. These all are **models** of that application. 

Every model consists of common **properties**. A blog always has a _title_ and some _content_, a _time of creation_ and an _author_. In opposition to that a comment _belongs to a post_, but has an _author_, some _content_ and a _time of creation_ as well. Users have a unique _login name_, a secret _password_ and some _mail address_. 

As explained Some properties are meant to be _unique_ or to be _always_ there. Such keywords indicate **constraints** that might apply on a property. Constraints are rules to be met by either item prior to saving it in a database or similar. This helps to assure the integrity of all data.

All this describes similarities in records of actual data. It does not describe any actual data of your application, though, such as a user named Bob or Alice or any of either user's posts in particular. Information describing Bob and Alice as users are instances a.k.a. items of the model _user_.
:::
