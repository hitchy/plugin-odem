# Configuration

In [runtime configuration](https://core.hitchy.org/api/hitchy.html#configuration), options specific to Odem are in section `database`.

:::code-group
```javascript [config/database.js]
export default function() {
	const api = this;

	return {
		database: {
			// TODO put your Odem-specific configuration here ...
        }
    };
}
```
:::

Several options described here can be controlled via command-line arguments or via environment variables. Providing a custom configuration is always an option, too. However, when doing so, those opportunities for using the command line or environment variables is gone unless they are re-implemented as part of your configuration.

## default

This option selects the [adapter](adapter.md) to use with models by default.

When omitted, an instance of `OdemAdapterMemory` is set up to "persist" items of models in local runtime's volatile memory. This default works for testing and developing, but usually does not work for production setups.

## disableCache <Badge type="info" text="0.10.0+"></Badge>

This boolean option globally disables any [local caching](model.md#caching-support).

It's default can be controlled via command line by setting option `--odem-disable-cache` or via environment by setting variable `ODEM_DISABLE_CACHE`.

## eagerCache <Badge type="info" text="0.11.0+"></Badge>

On start, Odem is setting up all _defined_ indices for its models. This requires fetching all items of either model from datasource. When caching is enabled for a model, this opportunity is used to instantly cache all the items in local runtime, too. However, if a model does not have any index, no caching is set up on start either.

This boolean option can be set to make sure local caching is set up on start even for models without any index.

It's default can be controlled via command line by setting option `--odem-eager-cache` or via environment by setting variable `ODEM_EAGER_CACHE`.

## cacheTTL <Badge type="info" text="0.10.0+"></Badge>

By default, caching relies on local copies managed indefinitely. Using this option, any caching ages over time causing existing cache records to be ignored. Once outdated, Odem is going to fetch records from datasource again. Eventually, this causes caches to rebuilt themselves from time to time.

The default behavior can be globally controlled via command line by setting option `--odem-cache-ttl` or via environment by setting variable `ODEM_CACHE_TTL`. In both cases, the cache's _time to live_ must be given as number of seconds since last time a model's cache has been fully established. Once passed, the caching is considered outdated and has to be re-established again.

## maxListenersPerModel <Badge type="info" text="0.10.0+"></Badge>

This option is used to invoke [setMaxListeners()](https://nodejs.org/docs/latest/api/events.html#emittersetmaxlistenersn) once per model on setting up its [static notifications stub](model.md#model-notifications) for listening to model-related events.

Its value isn't limiting the number of accepted listeners. However, on exceeding the number of listeners configured here causes the runtime to emit warnings to the console as it might indicate memory leaks. 

You may want to increase the default of `100` in case you get a lot of such warnings while being sure there is no memory leak and want to mitigate them. Usually, the value should be as high as the number of instances that may exist in runtime memory at the same time. The default can be adjusted 
