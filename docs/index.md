---
layout: home

hero:
  name: Hitchy Odem
  text: a document-oriented database manager
  tagline: |
    Odem manages data without any separate service storing its data in memory, in the local filesystem, in an etcd cluster or wherever you want ...
  actions:
    - theme: brand
      text: About
      link: ./introduction.md
    - theme: brand
      text: Glossary
      link: ./glossary.md
    - theme: brand
      text: Getting started
      link: ./guides/quickstart.md
    - theme: brand
      text: Model definition
      link: ./guides/defining-models.md
    - theme: brand
      text: Using models
      link: ./guides/using-models.md
    - theme: brand
      text: API reference
      link: ./api/index.md
---

### MIT License

Copyright &copy; 2017 cepharum GmbH

:::details Click to see the full text ...
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
:::
