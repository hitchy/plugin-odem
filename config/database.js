import { env } from "node:process";

/** */
export default function( options ) {
	const api = this;
	const { services: Services } = api;
	const { arguments: args = {} } = options;

	const disableCache = Boolean( args["odem-disable-cache"] ?? env.ODEM_DISABLE_CACHE );
	const eagerCache = Boolean( args["odem-eager-cache"] ?? env.ODEM_EAGER_CACHE );
	const cacheTTL = parseInt( args["odem-cache-ttl"] ?? env.ODEM_CACHE_TTL ) || 0;
	const maxListenersPerModel = parseInt( args["odem-max-listeners-per-model"] ?? env.ODEM_MAX_LISTENERS_PER_MODEL ) || 100;

	const OdemDefaults = {
		/**
		 * Configures to explicitly disable all caching of model data in current
		 * runtime or not.
		 */
		disableCache,

		/**
		 * A model's cache - if enabled - is populated while setting up the
		 * model's indices at start of Hitchy. If a model does not define any
		 * indices, the cache's population is postponed until some request is
		 * trying to find some item.
		 *
		 * By setting this option, the cache is always populated on starting
		 * Hitchy no matter there is an index defined or not.
		 */
		eagerCache: !disableCache && eagerCache,

		/**
		 * A fully populated cache may be considered outdated after the number
		 * of seconds configured here. An outdated cache isn't used until the
		 * next request for finding items without index.
		 *
		 * The value is 0 by default to disable any aging of a cache.
		 */
		cacheTTL,

		/**
		 * Defines soft limit on number of notification listeners per model. The
		 * default is 100.
		 *
		 * If your backend code is loading many instances of a model and listens
		 * to change notifications on them simultaneously, this soft limit may
		 * be hit resulting in warnings logged to the console. If you see this
		 * warning it might also indicate memory leaks in your code.
 		 */
		maxListenersPerModel,
	};

	Object.defineProperties( OdemDefaults, {
		/**
		 * Defines backend to use by default on storing model data persistently.
		 *
		 * @name api.config.database.default
		 * @property {Services.OdemAdapter}
		 * @readonly
		 */
		default: {
			get: () => {
				const adapter = new Services.OdemAdapterMemory();

				Object.defineProperties( OdemDefaults, {
					default: {
						value: adapter,
						enumerable: true,
						configurable: true,
					},
				} );

				return adapter;
			},
			enumerable: true,
			configurable: true,
		},
	} );

	return { database: OdemDefaults };
};
