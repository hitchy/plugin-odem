# Hitchy Odem [![pipeline status](https://gitlab.com/hitchy/plugin-odem/badges/master/pipeline.svg)](https://gitlab.com/hitchy/plugin-odem/-/commits/master)

_a document-oriented database for Hitchy_

## License

[MIT](LICENSE)

## About

[Hitchy](https://core.hitchy.org/) is a lean server-side application framework focusing on these features:

* Easily discover plugins.
* Quickly dispatch incoming requests to handlers of those plugins.

Hitchy requires a plugin like this one to conveniently manage data. Hitchy Odem provides a document-oriented database for Hitchy. It is working on top of a key-value store relying on customizable backend adapters. There are two backends included with this plugin:

 * A memory-based adapter manages data in volatile runtime memory mostly for developing and testing purposes. It is the default backend.
 * A filesystem-based adapter is storing all data in a customizable folder of your local filesystem. This backend is suitable for persistently storing data in a single-server context.

In addition, there is [a separate plugin](https://www.npmjs.com/package/@hitchy/plugin-odem-etcd) providing an adapter for using an etcd3 cluster as backend in a multiple-server context.

### Performance and memory consumption

Hitchy Odem supports index-based searching to significantly improve its performance on finding items when used with a filesystem-based data store or some dedicated etcd cluster. Indexes are rebuilt in runtime memory on every start of your Hitchy-based service. For that, all items of models with declared indexes are fetched from those stores on start. This results in increased memory consumption and boot time closely related to the number of items in your data store.

Of course, Hitchy Odem is not designed to compete with established database engines like MySQL or MongoDB. However, most applications work with rather small sets of data and thus do not benefit much from those engines for the memory consumption and boot time are still rather low. On the other hand, using Hitchy in combination with Hitchy Odem comes with very little prerequisites for the server they are running on. For example, it takes a single Docker container to run your application. There is no need to have composition of containers combining a sophisticated database engine with your application. That's following one of the core-principles of Hitchy: getting rid of dependencies as good as possible.

## Documentation

The latest documentation is available at https://odem.hitchy.org/. 
