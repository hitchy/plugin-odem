import UUIDFn from "../../../api/services/odem/utility/uuid";
import IndexFn from "../../../api/services/odem/model/indexer/equality";
import PromiseUtils from "promise-essentials";

const UUID = UUIDFn();
const Index = IndexFn();

/**
 * runs an resilience test with different values and types
 * @return {Promise<Array>} resolves if test is finished
 */
function test() {
	console.log( '"records"; "different values"; "type"; "rss"; "heapTotal"; "heapUsed"; "external"' ); // eslint-disable-line no-console
	const Values = [
		[ "integer", num => new Array( num ).fill( 0, 0, num ).map( ( _, index ) => index ) ],
		[ "string", num => new Array( num ).fill( 0, 0, num ).map( ( _, index ) => `prefix${index}suffix` ) ],
	];

	return PromiseUtils.each( [ 1000, 10000, 100000, 500000 ], NumRecords => {
		const uuids = new Array( NumRecords );

		return new Promise( ( resolve, reject ) => {
			const create = index => {
				if ( index >= NumRecords ) {
					resolve();
				} else {
					UUID.create()
						.then( uuid => {
							if ( uuids.indexOf( uuid ) < 0 ) {
								uuids[index] = uuid;
								create( index + 1 );
							} else {
								create( index );
							}
						} )
						.catch( reject );
				}
			};
			create( 0 );
		} ).then( () => {
			return PromiseUtils.each( [ 1, 2, 10, 100, 1000, 10000, 100000, 500000 ], numValues => {
				if ( numValues > NumRecords ) {
					return Promise.resolve();
				}

				return PromiseUtils.each( Values,( [ valueType, valueGenerator ] ) => {
					const values = valueGenerator( numValues );
					let memoryUsageBefore;

					const MyIndex = new Index( { revision: 0 } );

					return gc()
						.then( function() {
							memoryUsageBefore = process.memoryUsage();

							for ( let i = 0; i < NumRecords; i++ ) {
								MyIndex.add( uuids[i], values[i % numValues] );
							}

							return UUID.create()
								.then( id => {
									MyIndex.add( id, values[0] );
								} ).then( () => {
									const memoryUsage = process.memoryUsage();
									console.error( "computing:", `${NumRecords} records with ${numValues} different values of type ${valueType}` );

									const diff = {
										rss: memoryUsage.rss - memoryUsageBefore.rss,
										heapTotal: memoryUsage.heapTotal - memoryUsageBefore.heapTotal,
										heapUsed: memoryUsage.heapUsed - memoryUsageBefore.heapUsed,
										external: memoryUsage.external - memoryUsageBefore.external,
									};

									console.log( `${NumRecords}; ${numValues}; ${valueType}; ${diff.rss}; ${diff.heapTotal}; ${diff.heapUsed}; ${diff.external}` ); // eslint-disable-line no-console
								} );
						} );
				} );
			} );
		} );
	} )
		.catch( error => {
			console.error( `FAILED: ${error.stack}` );
		} );
}

test();

/**
 * Invokes garbage collection and waits a moment.
 *
 * @param {int} delayMs number of milliseconds to wait after triggering GC
 * @returns {Promise} promises garbage collection triggered and delay passed
 */
function gc( delayMs = 5000 ) {
	if ( !global.gc ) {
		return Promise.reject( new Error( "run node with option --expose-gc (e.g. `node --expose-gc index.js`)" ) );
	}

	return new Promise( resolve => {
		global.gc( true );

		setTimeout( resolve, delayMs );
	} );
}
