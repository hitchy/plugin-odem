export default {
	props: {
		someString: {},
		someInteger: {
			type: "integer",
		},
	},
	hooks: {
		beforeValidate() {
			this.someString = Object.keys( this.$api.models ).join( "," );
		},
	},
};
