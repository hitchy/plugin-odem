import BasicData from "./basic-data.js";

/**
 * Defines model while relying on Hitchy's CMP.
 *
 * @returns {Object} generator for actual model definition
 */
export default function() {
	return BasicData;
};
