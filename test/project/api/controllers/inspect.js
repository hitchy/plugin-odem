import ModelFn from "../../../../api/services/model.js";

const RawModel = ModelFn.call( {
	config: {},
	log: () => {},
} );

export default {
	models( req, res ) {
		res.json( Object.keys( this.api.models ) );
	},

	modelClass( req, res ) {
		const { Model } = this.api.services;

		res.json( Boolean( Model && Model.define && Model.list && Model.find ) );
	},

	modelImplicitInstance( req, res ) {
		const { BasicData } = this.api.models;

		const item = new BasicData();

		return item.save()
			.then( () => {
				res.json( item.toObject() );
			} );
	},

	modelImplicitCmfpInstance( req, res ) {
		const { CmfpRegular } = this.api.models;

		const item = new CmfpRegular();

		return item.save()
			.then( () => {
				res.json( item.toObject() );
			} );
	},

	modelExplicitInstance( req, res ) {
		const ExtraModel = this.api.services.Model.define( "RawModel", {
			props: {
				someString: {},
			},
			hooks: {
				beforeValidate() {
					this.someString = Object.keys( this.$api.models ).join( "," );
				},
			},
		} );

		const item = new ExtraModel();

		return item.save()
			.then( () => {
				res.json( item.toObject() );
			} );
	},

	modelExplicitRawInstance( req, res ) {
		const ExtraRawModel = RawModel.define( "RawModel", {
			props: {
				someString: {},
			},
			hooks: {
				beforeValidate() {
					this.someString = Object.keys( this.$api.models ).join( "," );
				},
			},
		} );

		const item = new ExtraRawModel();

		return item.save()
			.then( () => {
				res.json( item.toObject() );
			} );
	},
};
