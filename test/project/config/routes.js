export const routes = {
	"/models": "inspect.models",
	"/modelClass": "inspect.modelClass",
	"/modelImplicitInstance": "inspect.modelImplicitInstance",
	"/modelImplicitCmfpInstance": "inspect.modelImplicitCmfpInstance",
	"/modelExplicitInstance": "inspect.modelExplicitInstance",
	"/modelExplicitRawInstance": "inspect.modelExplicitRawInstance",
};
