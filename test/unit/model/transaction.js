import { describe, beforeEach, afterEach, it } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";
import "should";

const Test = await SDT( Core );

const config = {
	plugin: true,
	projectFolder: false,
	files: {
		"api/model/foo.js": "export default { props: { name: {} } }",
		"api/model/bar.js": "export default { props: { label: {} } }",
	},
};

describe( "Transactions", () => {
	for ( const [ adapterClass, dbConfig ] of [
		[ "OdemAdapterMemory", "new this.service.OdemAdapterMemory()" ],
		[ "OdemAdapterFile", `new this.service.OdemAdapterFile( { dataSource: "data" } )` ],
	] ) {
		describe( `with ${adapterClass}`, () => {
			const ctx = {};

			afterEach( Test.after( ctx ) );
			beforeEach( Test.before( ctx, {
				...config,
				files: {
					...config.files,
					"config/local.js": `export default function() { return { database: { default: ${dbConfig} } }; }`,
				}
			} ) );

			beforeEach( async() => {
				const foo = new ctx.hitchy.api.model.Foo();
				foo.name = "John Doe";

				await foo.save();

				ctx.uuid = foo.uuid;
			} );

			const wait = ms => new Promise( resolve => setTimeout( resolve, ms ) );

			const waitThenChange = async( ms, value, tx ) => {
				const { Foo } = tx || ctx.hitchy.api.model;

				await wait( ms );

				const foo = new Foo( ctx.uuid );
				await foo.load();
				foo.name = value;
				await foo.save();
			};

			const read = async tx => {
				const { Foo } = tx || ctx.hitchy.api.model;

				const foo = new Foo( ctx.uuid );
				await foo.load();

				return foo.name;
			};

			it( "are using proper type of adapter", () => {
				const { Foo, Bar } = ctx.hitchy.api.model;

				Foo.adapter.should.be.instanceOf( ctx.hitchy.api.service[adapterClass] );
				Bar.adapter.should.be.instanceOf( ctx.hitchy.api.service[adapterClass] );
			} );

			it( "are available via static method per model", () => {
				ctx.hitchy.api.model.Foo.transaction.should.be.a.Function();
				ctx.hitchy.api.model.Bar.transaction.should.be.a.Function();
			} );

			it( "require provision of a callback when invoking ", () => {
				const { Foo, Bar } = ctx.hitchy.api.model;

				for ( const model of [ Foo, Bar ] ) {
					model.transaction().should.be.rejectedWith( /\bcallback\b/ );
					model.transaction( null ).should.be.rejectedWith( /\bcallback\b/ );
					model.transaction( undefined ).should.be.rejectedWith( /\bcallback\b/ );
					model.transaction( false ).should.be.rejectedWith( /\bcallback\b/ );
					model.transaction( true ).should.be.rejectedWith( /\bcallback\b/ );
					model.transaction( "" ).should.be.rejectedWith( /\bcallback\b/ );
					model.transaction( "hello" ).should.be.rejectedWith( /\bcallback\b/ );
					model.transaction( 0 ).should.be.rejectedWith( /\bcallback\b/ );
					model.transaction( 1 ).should.be.rejectedWith( /\bcallback\b/ );
					model.transaction( [] ).should.be.rejectedWith( /\bcallback\b/ );
					model.transaction( {} ).should.be.rejectedWith( /\bcallback\b/ );

					model.transaction( () => {} ).should.be.resolved();
				}
			} );

			it( "provide custom collection of models in first argument to provided callback each bound to the transaction", async() => {
				let models;

				await ctx.hitchy.api.model.Foo.transaction( tx => {
					models = tx;
				} );

				models.should.have.ownProperty( "Foo" );
				models.should.have.ownProperty( "Bar" );

				new models.Foo().should.be.instanceOf( ctx.hitchy.api.model.Foo );
				new models.Bar().should.be.instanceOf( ctx.hitchy.api.model.Bar );

				models.Foo.should.not.be.equal( ctx.hitchy.api.model.Foo );
				models.Bar.should.not.be.equal( ctx.hitchy.api.model.Bar );

				models.Foo.adapter.should.be.equal( models.Bar.adapter );
				ctx.hitchy.api.model.Foo.adapter.should.be.equal( ctx.hitchy.api.model.Bar.adapter );
				models.Foo.adapter.should.not.be.equal( ctx.hitchy.api.model.Foo.adapter );
				models.Bar.adapter.should.not.be.equal( ctx.hitchy.api.model.Bar.adapter );

				models.Foo.adapter.should.be.instanceof( ctx.hitchy.api.model.Foo.adapter.constructor );
				models.Bar.adapter.should.be.instanceof( ctx.hitchy.api.model.Bar.adapter.constructor );
			} );

			it( "bind models provided in first argument to provided callback to a transaction", async() => {
				let models;

				await ctx.hitchy.api.model.Foo.transaction( tx => {
					models = tx;
				} );

				models.should.have.ownProperty( "Foo" );
				models.should.have.ownProperty( "Bar" );
			} );

			it( "prevent data changed while processing a transaction", async() => {
				await Promise.all( [
					waitThenChange( 200, "john" ),
					waitThenChange( 50, "jane" ),
				] );

				( await read() ).should.be.equal( "john" );

				await Promise.all( [
					ctx.hitchy.api.model.Foo.transaction( tx => waitThenChange( 200, "jack", tx ) ),
					waitThenChange( 50, "jill" ),
				] );

				await read().should.be.resolvedWith( "jill" );
			} );

			it( "rolls back creation of item when transaction has failed", async() => {
				let uuid;

				await ctx.hitchy.api.model.Foo.transaction( async tx => {
					const foo = new tx.Foo();
					foo.name = "jason";
					await foo.save();
					uuid = foo.uuid;

					await wait( 100 );
					throw new Error( "transaction failed" );
				} ).should.be.rejectedWith( "transaction failed" );

				const test = new ctx.hitchy.api.model.Foo( uuid );
				await test.$exists.should.be.resolvedWith( false );
			} );

			it( "commits creation of item when transaction completes", async() => {
				let uuid;

				await ctx.hitchy.api.model.Foo.transaction( async tx => {
					const foo = new tx.Foo();
					foo.name = "jason";
					await foo.save();
					uuid = foo.uuid;

					await wait( 100 );
				} ).should.be.resolved();

				const test = new ctx.hitchy.api.model.Foo( uuid );
				await test.$exists.should.be.resolvedWith( true );
			} );

			it( "rolls back made changes when transaction has failed", async() => {
				await Promise.all( [
					wait( 40 ).then( read ).should.be.resolvedWith( "John Doe" ),
					ctx.hitchy.api.model.Foo.transaction( async tx => {
						await waitThenChange( 10, "jack", tx );

						await wait( 100 );
						throw new Error( "transaction failed" );
					} ).should.be.rejectedWith( "transaction failed" ),
				] );

				await read().should.be.resolvedWith( "John Doe" );
			} );

			it( "commits made changes when transaction completes", async() => {
				await Promise.all( [
					wait( 40 ).then( read ).should.be.resolvedWith( "John Doe" ),
					ctx.hitchy.api.model.Foo.transaction( async tx => {
						await waitThenChange( 10, "jack", tx );

						await wait( 100 );
					} ).should.be.resolved(),
				] );

				await read().should.be.resolvedWith( "jack" );
			} );

			it( "does not eventually remove item deleted in a failing transaction", async() => {
				await Promise.all( [
					wait( 40 ).then( read ).should.be.resolvedWith( "John Doe" ),
					ctx.hitchy.api.model.Foo.transaction( async tx => {
						const foo = new tx.Foo( ctx.uuid );
						await foo.$exists;
						await foo.remove();

						await wait( 100 );
						throw new Error( "transaction failed" );
					} ).should.be.rejectedWith( "transaction failed" ),
				] );

				await read().should.be.resolvedWith( "John Doe" );
			} );

			it( "commits item removed when transaction completes", async() => {
				await Promise.all( [
					wait( 40 ).then( read ).should.be.resolvedWith( "John Doe" ),
					ctx.hitchy.api.model.Foo.transaction( async tx => {
						const foo = new tx.Foo( ctx.uuid );
						await foo.$exists;
						await foo.remove();

						await wait( 100 );
					} ).should.be.resolved(),
				] );

				await read().should.be.rejectedWith( /no such record/ );
			} );

			it( "rejects to start another transaction from inside of a running transaction", async() => {
				await ctx.hitchy.api.model.Foo.transaction( async tx => {
					await tx.Foo.transaction( () => {} );
				} ).should.be.rejected();
			} );
		} );
	}
} );
