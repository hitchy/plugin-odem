import EventEmitter from "node:events";

import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

const Test = await SDT( Core );

const changeToPromise = ( item, fnOrTimeout ) => new Promise( ( resolve, reject ) => {
	item.$notifications
		.once( "created", reject )
		.once( "changed", ( ...args ) => resolve( args ) )
		.once( "removed", reject );

	if ( typeof fnOrTimeout === "function" ) {
		Promise.resolve( fnOrTimeout() ).catch( reject );
	} else if ( fnOrTimeout > 0 ) {
		setTimeout( resolve, parseInt( fnOrTimeout ) );
	}
} );


describe( "Notifications support", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		useTmpPath: true,
		projectFolder: false,
	} ) );

	before( prepareModel( ctx ) );

	it( "is exposed as static property of type EventEmitter in context of a defined model", () => {
		ctx.SomeModel.should.have.property( "notifications" );
		ctx.SomeModel.notifications.should.be.instanceOf( EventEmitter );
	} );

	it( "is exposed as EventEmitter in context of model instance saved in backend before", () => {
		const item1 = new ctx.SomeModel( ctx.first );

		( () => item1.should.not.have.property( "$notifications" ) ).should.not.throw( /backend/ );
		item1.should.have.ownProperty( "$notifications" );
		item1.$notifications.should.be.instanceOf( EventEmitter );
	} );

	it( "is not accessible in context of freshly created but unsaved model instance", () => {
		const item1 = new ctx.SomeModel();

		( () => item1.should.not.have.property( "$notifications" ) ).should.throw( /backend/ );
	} );

	it( "is separately exposed as property per instance of a defined model", () => {
		const item1 = new ctx.SomeModel( ctx.first );
		const item2 = new ctx.SomeModel( ctx.first );

		item1.should.not.be.equal( item2 );
		item1.should.have.ownProperty( "$notifications" );
		item2.should.have.ownProperty( "$notifications" );
		item1.$notifications.should.not.be.equal( item2.$notifications );
	} );

	it( "keeps exposing same emitter per model instance", () => {
		const item1 = new ctx.SomeModel( ctx.first );
		const item2 = new ctx.SomeModel( ctx.first );

		item1.$notifications.should.be.equal( item1.$notifications );
		item2.$notifications.should.be.equal( item2.$notifications );
	} );
} );

for ( const [ name, config ] of [
	[ "memory", {} ],
	[ "file system", {
		files: {
			"config/database.js": `export default function( options ) {
	return {
		database: {
			default: new this.services.OdemAdapterFile( {
				dataSource: this.folder( "data" ),
			} ),
		},
	};
};
`,
		}
	} ],
] ) {
	describe( `On a model managed in ${name}, notifications support`, () => {
		const ctx = {};

		after( Test.after( ctx ) );
		before( Test.before( ctx, Object.assign( {
			plugin: true,
			useTmpPath: true,
			projectFolder: false,
		}, config ) ) );

		before( prepareModel( ctx ) );

		describe( "reports on creating records in backend and thus", () => {
			it( "emits 'created' notification via model on saving new instance without locally changing any property", async() => {
				const item = new ctx.SomeModel();

				await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "created", resolve ).once( "changed", reject ).once( "removed", reject );

					item.save().catch( reject );
				} ).should.be.resolved();

				cleanup( ctx );
			} );

			it( "emits 'created' notification via model on saving new instance with locally changing any property", async() => {
				const item = new ctx.SomeModel();

				await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "created", resolve ).once( "changed", reject ).once( "removed", reject );

					item.name = "custom";
					item.save().catch( reject );
				} ).should.be.resolved();

				cleanup( ctx );
			} );

			it( "provides current record and its freshly assigned UUID when emitting 'created' notification via model", async() => {
				const item = new ctx.SomeModel();

				const info = await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "created", ( ...args ) => resolve( args ) );

					item.name = "boohoo";
					item.save().catch( reject );
				} ).should.be.resolved();

				info.should.be.Array().which.has.length( 3 );
				info[0].should.be.instanceOf( Buffer ).which.has.length( 16 );
				info[1].should.be.Object().which.has.properties( "name" );
				info[2].should.be.Function().which.has.length( 0 );

				// UUID properly transform to string here
				String( info[0] ).should.have.length( 36 );
				String( info[0] ).should.match( /^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}$/i );

				info[0].equals( ctx.SomeModel.normalizeUUID( ctx.first ) ).should.be.false();
				info[1].name.should.equal( "boohoo" );

				cleanup( ctx, item );
			} );

			it( "provides current record in its serialized form when emitting 'created' notification via model", async() => {
				const item = new ctx.SomeModel();

				const info = await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "created", ( ...args ) => resolve( args ) );

					item.updated = new Date();
					item.related = Buffer.from( [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ] );
					item.save().catch( reject );
				} ).should.be.resolved();

				// exposed properties of item have been deserialized
				item.updated.should.be.Date();
				item.related.should.be.instanceOf( Buffer );

				// in reported records those properties are still serialized
				info[1].should.have.property( "updated" ).which.is.a.String();
				info[1].should.have.property( "related" ).which.is.a.String();

				cleanup( ctx, item );
			} );

			it( "provides callback on emitting 'created' notification via model to promise instance of model representing that created record", async() => {
				const item1 = new ctx.SomeModel();
				let item2;

				await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications
						.once( "created", async( uuid, data, asyncGeneratorFn ) => {
							item2 = await asyncGeneratorFn();
							resolve();
						} );

					item1.name = "custom";
					item1.save().catch( reject );
				} ).should.be.resolved();

				( item2 === item1 ).should.be.false();
				item2.$uuid.equals( item1.$uuid ).should.be.true();
				item2.name.should.be.equal( item1.name );

				cleanup( ctx );
			} );
		} );

		describe( "reports on updating records in backend and thus", () => {
			it( "does not emit any notification on saving existing instance without locally changing any property", async() => {
				// two of these items are representing same backend item by intention
				const item1 = new ctx.SomeModel( ctx.first );
				const item2 = new ctx.SomeModel( ctx.first );
				const item3 = new ctx.SomeModel( ctx.second );

				await item1.load();
				await item2.load();
				await item3.load();

				await new Promise( ( resolve, reject ) => {
					setTimeout( resolve, 500 );

					item1.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );
					item2.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );
					item3.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );
					ctx.SomeModel.notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );

					item1.save().catch( reject );
					item2.save().catch( reject );
					item3.save().catch( reject );
				} ).should.be.resolved();

				cleanup( ctx, item1, item2, item3 );
			} );

			it( "does not emit any notification on locally re-assigning same values to properties of existing instance without saving", async() => {
				// both items are representing same backend item by intention
				const item1 = new ctx.SomeModel( ctx.first );
				const item2 = new ctx.SomeModel( ctx.first );

				await item1.load();
				await item2.load();

				await new Promise( ( resolve, reject ) => {
					setTimeout( resolve, 500 );

					item1.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );
					item2.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );
					ctx.SomeModel.notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );

					item1.name = "foo";
					item1.label = "bar";
					item1.weight = 10.55;
					item1.age = 3;
					item1.occupied = true;
					item1.updated = "2022-04-10T09:35:00Z";
					item1.related = "12345678-1234-1234-1234-123456789012";

					item2.name = "foo";
					item2.label = "bar";
					item2.weight = 10.55;
					item2.age = 3;
					item2.occupied = true;
					item2.updated = "2022-04-10T09:35:00Z";
					item2.related = "12345678-1234-1234-1234-123456789012";
				} ).should.be.resolved();

				cleanup( ctx, item1, item2 );
			} );

			it( "does not emit any notification on locally re-assigning same values to properties of existing instance and saving", async() => {
				// both items are representing same backend item by intention
				const item1 = new ctx.SomeModel( ctx.first );
				const item2 = new ctx.SomeModel( ctx.first );

				await item1.load();
				await item2.load();

				await new Promise( ( resolve, reject ) => {
					setTimeout( resolve, 500 );

					item1.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );
					item2.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );
					ctx.SomeModel.notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );

					item1.name = "foo";
					item1.label = "bar";
					item1.weight = 10.55;
					item1.age = 3;
					item1.occupied = true;
					item1.updated = "2022-04-10T09:35:00Z";
					item1.related = "12345678-1234-1234-1234-123456789012";

					item2.name = "foo";
					item2.label = "bar";
					item2.weight = 10.55;
					item2.age = 3;
					item2.occupied = true;
					item2.updated = "2022-04-10T09:35:00Z";
					item2.related = "12345678-1234-1234-1234-123456789012";

					item1.save().catch( reject );
					item2.save().catch( reject );
				} ).should.be.resolved();

				cleanup( ctx, item1, item2 );
			} );

			it( "does not emit any notification on locally changing values of existing instance's properties without saving", async() => {
				// both items are representing same backend item by intention
				const item1 = new ctx.SomeModel( ctx.first );
				const item2 = new ctx.SomeModel( ctx.first );

				await item1.load();
				await item2.load();

				await new Promise( ( resolve, reject ) => {
					setTimeout( resolve, 500 );

					item1.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );
					item2.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );
					ctx.SomeModel.notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );

					item1.name = "1foo";
					item1.label = "1bar";
					item1.weight = 110.55;
					item1.age = 13;
					item1.occupied = false;
					item1.updated = "2022-04-10T09:35:00Z";
					item1.related = "22345678-1234-1234-1234-123456789012";

					item2.name = "2foo";
					item2.label = "2bar";
					item2.weight = 210.55;
					item2.age = 23;
					item2.occupied = null;
					item2.updated = "2022-04-10T09:35:00Z";
					item2.related = "32345678-1234-1234-1234-123456789012";
				} ).should.be.resolved();

				cleanup( ctx, item1, item2 );
			} );

			it( "emits 'changed' notification via model on saving actually changed properties of existing instances", async() => {
				const item1 = new ctx.SomeModel( ctx.first );
				const item2 = new ctx.SomeModel( ctx.first );
				const item3 = new ctx.SomeModel( ctx.second );

				await item1.load();
				await item2.load();
				await item3.load();

				let uuid = await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "created", reject ).once( "changed", resolve ).once( "removed", reject );

					item1.name = "goo";
					item1.save().catch( reject );
				} ).should.be.resolved();
				uuid.equals( ctx.SomeModel.normalizeUUID( ctx.first ) ).should.be.true();
				uuid.equals( ctx.SomeModel.normalizeUUID( ctx.second ) ).should.be.false();

				// UUID properly transform to string here
				String( uuid ).should.have.length( 36 );
				String( uuid ).should.match( /^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}$/i );

				uuid = await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "created", reject ).once( "changed", resolve ).once( "removed", reject );

					item1.name = "goo";
					item1.save().catch( reject );
				} ).should.be.resolved();
				uuid.equals( ctx.SomeModel.normalizeUUID( ctx.first ) ).should.be.true();
				uuid.equals( ctx.SomeModel.normalizeUUID( ctx.second ) ).should.be.false();

				// UUID properly transform to string again
				String( uuid ).should.have.length( 36 );
				String( uuid ).should.match( /^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}$/i );

				uuid = await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "created", reject ).once( "changed", resolve ).once( "removed", reject );

					item2.name = "goo";
					item2.save().catch( reject );
				} ).should.be.resolved();
				uuid.equals( ctx.SomeModel.normalizeUUID( ctx.first ) ).should.be.true();
				uuid.equals( ctx.SomeModel.normalizeUUID( ctx.second ) ).should.be.false();

				uuid = await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "created", reject ).once( "changed", resolve ).once( "removed", reject );

					item3.name = "bam";
					item3.save().catch( reject );
				} ).should.be.resolved();
				uuid.equals( ctx.SomeModel.normalizeUUID( ctx.first ) ).should.be.false();
				uuid.equals( ctx.SomeModel.normalizeUUID( ctx.second ) ).should.be.true();

				cleanup( ctx, item1, item2, item3 );
			} );

			it( "emits 'changed' notification via instances associated with a single pre-existing item when saving either instance after local change", async() => {
				const item1 = new ctx.SomeModel( ctx.first );
				const item2 = new ctx.SomeModel( ctx.first );

				await item1.load();
				await item2.load();

				// same instance on same record in backend
				let info = await changeToPromise( item1, () => {
					item1.name = "baz";
					return item1.save();
				} ).should.be.resolved();

				info[0].name.should.be.equal( "baz" );
				info[1].name.should.be.equal( "goo" );

				// make sure change notification on second item has passed, too
				await changeToPromise( item2, 200 );

				info = await changeToPromise( item2, () => {
					item2.label = "foo";
					return item2.save();
				} ).should.be.resolved();

				info[0].label.should.be.equal( "foo" );
				info[1].label.should.be.equal( "bar" );

				// make sure change notification on second item has passed, too
				await changeToPromise( item1, 200 );


				// different instance on same record in backend (one way)
				info = await changeToPromise( item1, () => {
					item2.age = 12;
					return item2.save();
				} ).should.be.resolved();

				info[0].age.should.be.equal( 12 );
				info[1].age.should.be.equal( 3 );

				// make sure change notification on second item has passed, too
				await changeToPromise( item2, 200 );

				info = await changeToPromise( item2, () => {
					item1.weight = 28;
					return item1.save();
				} ).should.be.resolved();

				info[0].weight.should.be.equal( 28 );
				info[1].weight.should.be.equal( 10.55 );

				// make sure change notification on second item has passed, too
				await changeToPromise( item1, 200 );

				cleanup( ctx, item1, item2 );
			} );

			it( "provides previous and current record when emitting 'changed' notification via model", async() => {
				const item = new ctx.SomeModel( ctx.first );
				await item.load();

				const oldName = item.name;

				const info = await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "changed", ( ...args ) => resolve( args ) );

					item.name = "boohoo";
					item.save().catch( reject );
				} ).should.be.resolved();

				info.should.be.Array().which.has.length( 4 );
				info[0].should.be.instanceOf( Buffer ).which.has.length( 16 );
				info[1].should.be.Object().which.has.properties( "name", "label", "age", "weight", "updated", "related" );
				info[2].should.be.Object().which.has.properties( "name", "label", "age", "weight", "updated", "related" );
				info[3].should.be.Function().which.has.length( 0 );

				info[0].equals( ctx.SomeModel.normalizeUUID( ctx.first ) ).should.be.true();
				info[1].name.should.equal( "boohoo" );
				info[2].name.should.not.equal( "boohoo" ).and.equal( oldName );

				cleanup( ctx );
			} );

			it( "provides previous and current record in their serialized form when emitting 'changed' notification via model", async() => {
				const item = new ctx.SomeModel( ctx.first );
				await item.load();

				const info = await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "changed", ( ...args ) => resolve( args ) );

					item.age++;
					item.save().catch( reject );
				} ).should.be.resolved();

				// exposed properties of item have been unserialized
				item.updated.should.be.Date();
				item.related.should.be.instanceOf( Buffer );

				// in reported records those properties are still serialized
				info[1].should.have.property( "updated" ).which.is.a.String();
				info[2].should.have.property( "updated" ).which.is.a.String();
				info[1].should.have.property( "related" ).which.is.a.String();
				info[2].should.have.property( "related" ).which.is.a.String();

				cleanup( ctx );
			} );

			it( "provides previous and current record when emitting 'changed' notification via instance", async() => {
				const item = new ctx.SomeModel( ctx.first );
				await item.load();

				const oldName = item.name;

				const info = await new Promise( ( resolve, reject ) => {
					item.$notifications.once( "changed", ( ...args ) => resolve( args ) );

					item.name = "pow";
					item.save().catch( reject );
				} ).should.be.resolved();

				info.should.be.Array().which.has.length( 2 );
				info[0].should.be.Object().which.has.properties( "name", "label", "age", "weight", "updated", "related" );
				info[1].should.be.Object().which.has.properties( "name", "label", "age", "weight", "updated", "related" );

				info[0].name.should.equal( "pow" );
				info[1].name.should.not.equal( "pow" ).and.equal( oldName );

				cleanup( ctx, item );
			} );

			it( "provides previous and current record in their serialized form when emitting 'changed' notification via instance", async() => {
				const item = new ctx.SomeModel( ctx.first );
				await item.load();

				const info = await new Promise( ( resolve, reject ) => {
					item.$notifications.once( "changed", ( ...args ) => resolve( args ) );

					item.age++;
					item.save().catch( reject );
				} ).should.be.resolved();

				// exposed properties of item have been unserialized
				item.updated.should.be.Date();
				item.related.should.be.instanceOf( Buffer );

				// in reported records those properties are still serialized
				info[0].should.have.property( "updated" ).which.is.a.String();
				info[1].should.have.property( "updated" ).which.is.a.String();
				info[0].should.have.property( "related" ).which.is.a.String();
				info[1].should.have.property( "related" ).which.is.a.String();

				cleanup( ctx, item );
			} );

			it( "provides callback on emitting 'changed' notification via model to promise another instance of model representing that updated record", async() => {
				const item1 = new ctx.SomeModel( ctx.first );
				let item2;

				await item1.load();

				await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications
						.once( "changed", async( uuid, data, oldData, asyncGeneratorFn ) => {
							item2 = await asyncGeneratorFn();
							resolve();
						} );

					item1.name = "Custom";
					item1.save().catch( reject );
				} ).should.be.resolved();

				( item2 === item1 ).should.be.false();
				item2.$uuid.equals( item1.$uuid ).should.be.true();
				item2.name.should.be.equal( item1.name ).and.be.equal( "Custom" );
				item2.updated.should.be.Date();
				item2.related.should.be.instanceOf( Buffer );
				item2.updated.getTime().should.be.equal( item1.updated.getTime() );
				item2.related.equals( item1.related ).should.be.true();

				cleanup( ctx );
			} );

			it( "instantly updates properties on all instances listening for 'changed' notification when saving either instance after local change", async() => {
				const item1 = new ctx.SomeModel( ctx.first );
				const item2 = new ctx.SomeModel( ctx.first );

				await item1.load();
				await item2.load();

				await new Promise( ( resolve, reject ) => {
					item1.$notifications.once( "changed", resolve );

					item2.name = "BAZ";
					item2.save().catch( reject );
				} ).should.be.resolved();

				cleanup( ctx, item1, item2 );

				item1.name.should.equal( "BAZ" );
				item2.name.should.equal( "BAZ" );

				// props of item1 and item2 are not synced any other way
				item2.name = "BAR";
				await item2.save();

				item1.name.should.not.equal( "BAR" );
				item2.name.should.equal( "BAR" );
			} );

			it( "does not emit any notification via instance associated with a different pre-existing item when saving after local change", async() => {
				const item1 = new ctx.SomeModel( ctx.first );
				const item2 = new ctx.SomeModel( ctx.first );
				const item3 = new ctx.SomeModel( ctx.second );

				await item1.load();
				await item2.load();
				await item3.load();

				await new Promise( ( resolve, reject ) => {
					setTimeout( resolve, 500 );

					item1.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );
					item2.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );

					item3.name = "baz";
					item3.save().catch( reject );
				} ).should.be.resolved();

				cleanup( ctx, item1, item2 );
			} );
		} );

		describe( "reports on removing records from backend and thus", () => {
			it( "emits 'removed' notification via model on removing existing instances from backend", async() => {
				const item = new ctx.SomeModel();
				await item.save();

				const uuid = await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "created", reject ).once( "changed", reject ).once( "removed", resolve );

					item.remove().catch( reject );
				} ).should.be.resolved();

				uuid.equals( item.$uuid ).should.be.true();

				// UUID properly transform to string here
				String( uuid ).should.have.length( 36 );
				String( uuid ).should.match( /^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}$/i );
				String( uuid ).should.equal( item.uuid );

				cleanup( ctx, item );
			} );

			it( "emits 'removed' notification via instance removing pre-existing item from backend", async() => {
				const item = new ctx.SomeModel();
				await item.save();

				await new Promise( ( resolve, reject ) => {
					item.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", resolve );

					item.remove().catch( reject );
				} ).should.be.resolved();

				cleanup( ctx, item );
			} );

			it( "emits 'removed' notification via instances associated with same pre-existing item on removing it from backend", async() => {
				const item1 = new ctx.SomeModel();
				await item1.save();

				const item2 = new ctx.SomeModel( item1.uuid );

				await new Promise( ( resolve, reject ) => {
					item2.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", resolve );

					item1.remove().catch( reject );
				} ).should.be.resolved();

				cleanup( ctx, item1, item2 );
			} );

			it( "does not emit 'removed' notification via instances associated with different pre-existing items on removing one from backend", async() => {
				const item1 = new ctx.SomeModel();
				await item1.save();

				const item2 = new ctx.SomeModel( ctx.first );
				const item3 = new ctx.SomeModel( ctx.second );

				await new Promise( ( resolve, reject ) => {
					setTimeout( resolve, 500 );

					item2.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );
					item3.$notifications.once( "created", reject ).once( "changed", reject ).once( "removed", reject );

					item1.remove().catch( reject );
				} ).should.be.resolved();

				cleanup( ctx, item1, item2, item3 );
			} );

			it( "emits 'removed' notification via model on purging backend", async() => {
				const item = new ctx.SomeModel();
				await item.save();

				const uuid = await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "created", reject ).once( "changed", reject ).once( "removed", resolve );

					ctx.SomeModel.adapter.purge().catch( reject );
				} ).should.be.resolved();

				uuid.should.be.instanceOf( Buffer ).which.has.length( 16 );

				cleanup( ctx, item );
			} );

			it( "emits 'removed' notification via instance on purging backend", async() => {
				const item = new ctx.SomeModel();
				await item.save();

				await new Promise( ( resolve, reject ) => {
					ctx.SomeModel.notifications.once( "created", reject ).once( "changed", reject ).once( "removed", resolve );

					ctx.SomeModel.adapter.purge().catch( reject );
				} ).should.be.resolved();

				cleanup( ctx, item );
			} );

		} );
	} );
}


function prepareModel( ctx ) {
	return async() => {
		ctx.SomeModel = ctx.hitchy.api.services.Model.define( "SomeModel", {
			props: {
				name: {},
				label: {},
				weight: { type: "number" },
				age: { type: "integer" },
				occupied: { type: "boolean" },
				updated: { type: "time" },
				related: { type: "uuid" },
			}
		} );

		const first = new ctx.SomeModel();
		first.name = "foo";
		first.label = "bar";
		first.weight = 10.55;
		first.age = 3;
		first.occupied = true;
		first.updated = "2022-04-10T09:35:00Z";
		first.related = "12345678-1234-1234-1234-123456789012";

		await first.save();
		ctx.first = first.uuid;

		const second = new ctx.SomeModel();
		second.name = "foo";
		second.label = "bar";
		second.weight = 10.55;
		second.age = 3;
		second.occupied = true;
		second.updated = "2022-04-10T09:35:00Z";
		second.related = "12345678-1234-1234-1234-123456789012";

		await second.save();
		ctx.second = second.uuid;
	};
}

function cleanup( ctx, ...instances ) {
	for ( const instance of instances ) {
		if ( instance.uuid ) {
			instance.$notifications.removeAllListeners();
		}
	}

	ctx.SomeModel.notifications.removeAllListeners();
}
