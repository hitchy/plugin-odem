import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";
import "should";

const Test = await SDT( Core );

describe( "Deriving a model", () => {
	const ctx = {};
	let Root, Intermittent, Sub;
	let Model;

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
	} ) );

	before( () => {
		( { Model } = ctx.hitchy.api.service );
	} );

	before( "defining models", () => {
		Root = Model.define( "root", {
			props: {
				rootName: {},
				name: {},
			},
			computed: {
				rootConverted() { return `root: ${this.rootName}`; },
				converted() { return `root: ${this.rootName}`; },
			},
			methods: {
				rootProcessed() { return `root processed: ${this.rootName}`; },
				processed() { return `root processed: ${this.rootName}`; },
			},
		} );

		Intermittent = Model.define( "intermittent", {
			props: {
				intermittentName: {},
				name: { type: "number" },
			},
			computed: {
				intermittentConverted() { return `intermittent: ${this.rootName}`; },
				converted() { return `intermittent: ${this.intermittentName}`; },
			},
			methods: {
				intermittentProcessed() { return `intermittent processed: ${this.rootName}`; },
				processed() { return `intermittent processed: ${this.rootName}`; },
			},
		}, Root );

		Sub = Model.define( "sub", {
			props: {
				subName: {},
				name: { type: "integer" },
			},
			computed: {
				subConverted() { return `sub: ${this.rootName}`; },
				converted() { return `sub: ${this.subName}`; },
			},
			methods: {
				subProcessed() { return `sub processed: ${this.rootName}`; },
				processed() { return `sub processed: ${this.subName}`; },
			},
		}, Intermittent );
	} );

	it( "results in all defined models derive from Model", () => {
		Root.prototype.should.be.instanceOf( Model );
		Intermittent.prototype.should.be.instanceOf( Model );
		Sub.prototype.should.be.instanceOf( Model );
	} );

	it( "results in derived models derive from Root", () => {
		Root.prototype.should.not.be.instanceOf( Root );
		Intermittent.prototype.should.be.instanceOf( Root );
		Sub.prototype.should.be.instanceOf( Root );
	} );

	it( "results in deepest model derive from Intermittent", () => {
		Root.prototype.should.not.be.instanceOf( Intermittent );
		Intermittent.prototype.should.not.be.instanceOf( Intermittent );
		Sub.prototype.should.be.instanceOf( Intermittent );
	} );

	it( "results in instance of either model being instance of Model", () => {
		( new Root ).should.be.instanceOf( Model );
		( new Intermittent ).should.be.instanceOf( Model );
		( new Sub ).should.be.instanceOf( Model );
	} );

	it( "results in instance of either model being instance of Root", () => {
		( new Root ).should.be.instanceOf( Root );
		( new Intermittent ).should.be.instanceOf( Root );
		( new Sub ).should.be.instanceOf( Root );
	} );

	it( "results in instance of derived models being instance of Intermittent", () => {
		( new Root ).should.not.be.instanceOf( Intermittent );
		( new Intermittent ).should.be.instanceOf( Intermittent );
		( new Sub ).should.be.instanceOf( Intermittent );
	} );

	it( "results in instance of deepest model being instance of Sub, only", () => {
		( new Root ).should.not.be.instanceOf( Sub );
		( new Intermittent ).should.not.be.instanceOf( Sub );
		( new Sub ).should.be.instanceOf( Sub );
	} );

	it( "results in either model's class exposing its defined name", () => {
		Root.name.should.be.equal( "root" );
		Intermittent.name.should.be.equal( "intermittent" );
		Sub.name.should.be.equal( "sub" );
	} );

	it( "results in either model's class exposing reference on class it derives from", () => {
		Root.derivesFrom.should.be.equal( Model );
		Intermittent.derivesFrom.should.be.equal( Root );
		Sub.derivesFrom.should.be.equal( Intermittent );
	} );

	it( "results in schema of a derived model exposing non-overloaded properties of its base classes", () => {
		Root.schema.props.should.have.property( "rootName" );
		Root.schema.props.should.not.have.property( "intermittentName" );
		Root.schema.props.should.not.have.property( "subName" );

		Intermittent.schema.props.should.have.property( "rootName" );
		Intermittent.schema.props.should.have.property( "intermittentName" );
		Intermittent.schema.props.should.not.have.property( "subName" );

		Sub.schema.props.should.have.property( "rootName" );
		Sub.schema.props.should.have.property( "intermittentName" );
		Sub.schema.props.should.have.property( "subName" );
	} );

	it( "results in schema of a derived model exposing properties of its base classes overloaded", () => {
		Root.schema.props.should.have.property( "name" );
		Intermittent.schema.props.should.have.property( "name" );
		Sub.schema.props.should.have.property( "name" );

		Root.schema.props.name.type.should.be.equal( "string" );
		Intermittent.schema.props.name.type.should.be.equal( "number" );
		Sub.schema.props.name.type.should.be.equal( "integer" );
	} );

	it( "results in schema of a derived model exposing non-overloaded properties of its base classes", () => {
		Root.schema.props.should.have.property( "rootName" );
		Root.schema.props.should.not.have.property( "intermittentName" );
		Root.schema.props.should.not.have.property( "subName" );

		Intermittent.schema.props.should.have.property( "rootName" );
		Intermittent.schema.props.should.have.property( "intermittentName" );
		Intermittent.schema.props.should.not.have.property( "subName" );

		Sub.schema.props.should.have.property( "rootName" );
		Sub.schema.props.should.have.property( "intermittentName" );
		Sub.schema.props.should.have.property( "subName" );
	} );

	it( "results in schema of a derived model exposing properties of its base classes overloaded", () => {
		Root.schema.props.should.have.property( "name" );
		Intermittent.schema.props.should.have.property( "name" );
		Sub.schema.props.should.have.property( "name" );

		Root.schema.props.name.type.should.be.equal( "string" );
		Intermittent.schema.props.name.type.should.be.equal( "number" );
		Sub.schema.props.name.type.should.be.equal( "integer" );
	} );

	it( "results in derived model's instances exposing own properties and all non-overloaded properties of derived models", () => {
		( new Root ).should.have.property( "rootName" );
		( new Root ).should.have.property( "name" );
		( new Root ).should.not.have.property( "intermittentName" );
		( new Root ).should.not.have.property( "subName" );

		( new Intermittent ).should.have.property( "rootName" );
		( new Intermittent ).should.have.property( "name" );
		( new Intermittent ).should.have.property( "intermittentName" );
		( new Intermittent ).should.not.have.property( "subName" );

		( new Sub ).should.have.property( "rootName" );
		( new Sub ).should.have.property( "name" );
		( new Sub ).should.have.property( "intermittentName" );
		( new Sub ).should.have.property( "subName" );
	} );

	it( "results in derived model's instances exposing own computed properties and all non-overloaded computed properties of derived models", () => {
		( new Root ).should.have.property( "rootConverted" );
		( new Root ).should.have.property( "converted" );
		( new Root ).should.not.have.property( "intermittentConverted" );
		( new Root ).should.not.have.property( "subConverted" );

		( new Intermittent ).should.have.property( "rootConverted" );
		( new Intermittent ).should.have.property( "converted" );
		( new Intermittent ).should.have.property( "intermittentConverted" );
		( new Intermittent ).should.not.have.property( "subConverted" );

		( new Sub ).should.have.property( "rootConverted" );
		( new Sub ).should.have.property( "converted" );
		( new Sub ).should.have.property( "intermittentConverted" );
		( new Sub ).should.have.property( "subConverted" );
	} );

	it( "re-defines overloaded properties in prototype of deriving model", () => {
		Object.getOwnPropertyNames( Root.prototype ).should.containEql( "name" );
		Object.getOwnPropertyNames( Intermittent.prototype ).should.containEql( "name" );
		Object.getOwnPropertyNames( Sub.prototype ).should.containEql( "name" );
	} );

	it( "does not re-define non-overloaded properties in prototype of deriving model but relies on prototype chain", () => {
		Object.getOwnPropertyNames( Root.prototype ).should.containEql( "rootName" );
		Object.getOwnPropertyNames( Intermittent.prototype ).should.not.containEql( "rootName" );
		Object.getOwnPropertyNames( Sub.prototype ).should.not.containEql( "rootName" );
	} );
} );
