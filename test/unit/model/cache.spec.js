import { env } from "node:process";
import { describe, it, afterEach } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";

const Test = await SDT( Core );

const configuresCacheInEnv = env.ODEM_DISABLE_CACHE || env.ODEM_FORCE_CACHE || env.ODEM_CACHE_TTL;

( configuresCacheInEnv ? describe.skip : describe )( "Caching runtime models", function() {
	this.timeout( 30000 );

	const ctx = {};

	const config = {
		plugin: true,
		projectFolder: false,
		files: {
			"api/model/foo.cjs": `module.exports = { props: { value: { type: "number" } } };`,
			"api/model/bar.cjs": `module.exports = { props: { value: { type: "number" } }, options: { cache: false } };`,
			"config/database.cjs": `
module.exports = function( options ) {
	return {
		database: {
			default: new this.services.OdemAdapterFile( {
				dataSource: require( "node:path" ).resolve( options.projectFolder, "data" ),
			} ),
		},
	};
};
`,
		},
	};

	afterEach( Test.after( ctx ) );

	// prepare a linear but shuffled sequence of integers
	const values = new Array( 1000 )
		.fill( 0 )
		.map( ( _, i ) => i )
		.sort( () => Math.random() * 2 - 1 );

	const prepareModel = async( model = "Foo" ) => {
		for ( let i = 0; i < 1000; i++ ) {
			const item = new ctx.hitchy.api.model[model]();
			item.value = values[i];
			await item.save(); // eslint-disable-line no-await-in-loop
		}
	};

	const listAllSorted = async( model = "Foo", repetitions = 5, pause = 0 ) => {
		const first = await ctx.hitchy.api.model[model].list( { sortBy: "value" }, { loadRecords: true } );
		first.should.have.length( 1000 );

		for ( let i = 0; i < repetitions; i++ ) {
			const items = await ctx.hitchy.api.model[model].list( { sortBy: "value" }, { loadRecords: true } ); // eslint-disable-line no-await-in-loop
			items.should.have.length( 1000 );

			first.map( j => j.value ).should.deepEqual( items.map( j => j.value ) );

			if ( pause > 0 ) {
				await delay( pause ); // eslint-disable-line no-await-in-loop
			}
		}
	};

	const delay = ms => new Promise( resolve => setTimeout( resolve, ms ) );

	it( "gets tested on a model prepared to have 1000 entries", async() => {
		await Test.before( ctx, { ...config } )();
		await prepareModel();
	} );

	it( "is enabled by default improving retrieval of all items", async() => {
		await Test.before( ctx, { ...config } )();
		await prepareModel();
		await listAllSorted();

		Boolean( ctx.hitchy.api.model.Foo._cache ).should.be.true();
	} );

	it( "is not enabled by default when using an adapter that does not benefit from caching", async() => {
		await Test.before( ctx, {
			...config,
			files: {
				...config.files,
				"config/database.cjs": `module.exports = function() { return {}; };`,
			},
		} )();
		await prepareModel();
		await listAllSorted();

		Boolean( ctx.hitchy.api.model.Foo._cache ).should.be.false();
	} );

	it( "can be populated eagerly improving retrieval of all items", async() => {
		await Test.before( ctx, {
			...config,
			options: {
				arguments: {
					"odem-eager-cache": true,
				}
			}
		} )();
		await prepareModel();
		await listAllSorted();
	} );

	it( "can be globally disabled preventing improvements for the retrieval of all items", async() => {
		await Test.before( ctx, {
			...config,
			options: {
				arguments: {
					"odem-disable-cache": true,
				}
			}
		} )();
		await prepareModel();
		await listAllSorted();
	} );

	it( "can be disabled per model preventing improvements for the retrieval of all items", async() => {
		await Test.before( ctx, { ...config } )();
		await prepareModel( "Bar" );
		await listAllSorted( "Bar" );
	} );

	it( "does not age by default improving retrieval of all items", async() => {
		await Test.before( ctx, { ...config } )();
		await prepareModel();
		await listAllSorted( "Foo", 10, 500 );
	} );

	it( "can be globally configured to age having an impact on retrieval of all items from time to time", async() => {
		await Test.before( ctx, {
			...config,
			options: {
				arguments: {
					"odem-cache-ttl": "1", // caches are outdated after 1 second
				}
			}
		} )();
		await prepareModel();
		await listAllSorted( "Foo", 10, 500 );
	} );
} );
