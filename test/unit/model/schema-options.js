import { describe, it, before, after, beforeEach, afterEach } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";
import "should";

const Test = await SDT( Core );

describe( "A model's schema may define", () => {
	const ctx = {};
	let Model;

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
	} ) );

	before( () => {
		( { Model } = ctx.hitchy.api.service );
	} );

	it( "a model that fails by default on assigning twice to property w/o saving intermittently", () => {
		const MyModel = Model.define( "MyModel", {
			props: { someProp: {} },
		} );

		( () => {
			const item = new MyModel();
			item.someProp = "a";
			item.someProp = "b";
		} ).should.throw();
	} );

	describe( "option `onUnsaved` which", () => {
		let origHandler = null;
		let captured;

		beforeEach( () => {
			origHandler = console.error;
			console.error = ( ...args ) => {
				captured.push( args );
			};
			captured = [];
		} );

		afterEach( () => {
			console.error = origHandler;
		} );

		it( "might be set 'ignore' to prevent failure or log message on assigning twice to property w/o saving intermittently", () => {
			const MyModel = Model.define( "MyModel", {
				props: { someProp: {} },
				options: {
					onUnsaved: "ignore",
				},
			} );

			( () => {
				const item = new MyModel();
				item.someProp = "a";
				item.someProp = "b";
			} ).should.not.throw();

			captured.should.be.empty();
		} );

		it( "might be set 'IGNORE' to prevent failure or log message on assigning twice to property w/o saving intermittently", () => {
			const MyModel = Model.define( "MyModel", {
				props: { someProp: {} },
				options: {
					onUnsaved: "IGNORE",
				},
			} );

			( () => {
				const item = new MyModel();
				item.someProp = "a";
				item.someProp = "b";
			} ).should.not.throw();

			captured.should.be.empty();
		} );

		it( "might be set 'warn' to prevent failure, but log message on assigning twice to property w/o saving intermittently", () => {
			const MyModel = Model.define( "MyModel", {
				props: { someProp: {} },
				options: {
					onUnsaved: "warn",
				},
			} );

			( () => {
				const item = new MyModel();
				item.someProp = "a";
				item.someProp = "b";
			} ).should.not.throw();

			captured.should.not.be.empty();
		} );

		it( "might be set 'WARN' to prevent failure, but log message on assigning twice to property w/o saving intermittently", () => {
			const MyModel = Model.define( "MyModel", {
				props: { someProp: {} },
				options: {
					onUnsaved: "WARN",
				},
			} );

			( () => {
				const item = new MyModel();
				item.someProp = "a";
				item.someProp = "b";
			} ).should.not.throw();

			captured.should.not.be.empty();
		} );

		it( "might be set 'fail' to fail w/o logging on assigning twice to property w/o saving intermittently", () => {
			const MyModel = Model.define( "MyModel", {
				props: { someProp: {} },
				options: {
					onUnsaved: "fail",
				},
			} );

			( () => {
				const item = new MyModel();
				item.someProp = "a";
				item.someProp = "b";
			} ).should.throw();

			captured.should.be.empty();
		} );

		it( "might be set 'FAIL' to fail w/o logging on assigning twice to property w/o saving intermittently", () => {
			const MyModel = Model.define( "MyModel", {
				props: { someProp: {} },
				options: {
					onUnsaved: "FAIL",
				},
			} );

			( () => {
				const item = new MyModel();
				item.someProp = "a";
				item.someProp = "b";
			} ).should.throw();

			captured.should.be.empty();
		} );

		it( "causes Model.define() to throw when assigning unknown value", () => {
			( () => Model.define( "MyModel", {
				props: { someProp: {} },
				options: {
					onUnsaved: "something",
				},
			} ) ).should.throw();
		} );
	} );
} );
