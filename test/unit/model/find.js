/* eslint-disable max-nested-callbacks */
import PromiseUtil from "promise-essentials";
import { describe, it, before, beforeEach, after, afterEach } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";
import "should";

const Test = await SDT( Core );

const NumRecords = 100;
const Integers = { from: -5000, to: 5000 };
const Numbers = { from: -5000000, to: 5000000 };
const TextSize = 20;
const Chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

const Properties = [
	[ "fastInteger", "integers" ],
	[ "slowInteger", "integers" ],
	[ "fastNumber", "numbers" ],
	[ "slowNumber", "numbers" ],
	[ "fastString", "texts" ],
	[ "slowString", "texts" ],
	[ "fastUUID", "uuids" ],
	[ "slowUUID", "uuids" ],
	[ "derivedString", "texts", stringDeriver ],
	[ "derivedInteger", "integers", integerDeriver ],
	[ "derivedUUID", "uuids", uuidDeriver ],
];


/**
 * Derives integer value from actual property.
 *
 * @returns {number} derived integer
 */
function integerDeriver() { return 2 * this.slowInteger; }

/**
 * Derives string value from actual property.
 *
 * @returns {number} derived string
 */
function stringDeriver() { return ">> " + this.slowString; }

/**
 * Derives UUID from actual property.
 *
 * @returns {Buffer} derived string
 */
function uuidDeriver() {
	return Buffer.concat( [ Buffer.from( [ 0, 0 ] ), this.slowUUID.slice( 2 ) ] );
}

/**
 * Generates array of values using provided value generator.
 *
 * The resulting array is guaranteed to consist of different values, only.
 *
 * @param {function} generator generates next value to use
 * @returns {array} resulting set of data
 */
function fill( generator ) {
	const data = [];

	for ( let i = 0; i < NumRecords; i++ ) {
		const value = generator();
		if ( data.findIndex( v => ( Buffer.isBuffer( value ) ? value.equals( v ) : value === v ) ) < 0 ) {
			data.push( value );
		} else {
			i--;
		}
	}

	return data;
}

/**
 * Detects if provided list of records is sorted.
 *
 * @param {Array} records records to be inspected
 * @param {boolean} up true if records are expected to be sorted in ascending order
 * @returns {boolean} true if records are sorted as expected
 */
function isSorted( records, up = true ) {
	let latest = NaN;

	for ( let i = 0, num = records.length; i < num; i++ ) {
		const value = records[i].index;

		if ( !isNaN( latest ) ) {
			if ( isNaN( value ) ) {
				throw new Error( "got NaN index" );
			}

			if ( !( up ? value > latest : value < latest ) ) {
				return false;
			}
		}

		latest = value;
	}

	return true;
}

/**
 * Detects if provided list of records is a straight sorted excerpt.
 *
 * @param {Array} records records to be inspected
 * @param {boolean} up true if records are expected to be sorted in ascending order
 * @returns {boolean} true if records are consecutive sorted excerpt as expected
 */
function isStraight( records, up = true ) {
	let latest = NaN;

	for ( let i = 0, num = records.length; i < num; i++ ) {
		const value = records[i].index;

		if ( !isNaN( latest ) ) {
			if ( isNaN( value ) ) {
				throw new Error( "got NaN index" );
			}

			if ( !( up ? value > latest : value < latest ) ) {
				return false;
			}
		}

		latest = value;
	}

	return true;
}

/**
 * Invokes provided function in context of proxy that's redirecting any access
 * on `this` returning provided value.`
 *
 * @param {function} providerFn function to invoke
 * @param {*} value value to provide whenever function is accessing property of `this`
 * @returns {*} value returned by invoked function
 */
function anyThis( providerFn, value ) {
	return providerFn.call( new Proxy( {}, { get: () => value, } ) );
}


describe( "Inspecting collection of a model's items", function() {
	this.timeout( 5000 );

	const ctx = {};
	let Model;
	let MyModel;

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
	} ) );

	before( () => {
		( { Model } = ctx.hitchy.api.service );
	} );

	// prepare data for feeding model
	const recordDriver = new Array( NumRecords ).fill( 0 );
	const textDriver = new Array( TextSize ).fill( 0 );

	const indexes = recordDriver.map( ( _, index ) => index );
	const integers = fill( () => Integers.from + Math.round( Math.random() * ( Integers.to - Integers.from ) ) );
	const numbers = fill( () => Numbers.from + ( Math.random() * ( Numbers.to - Numbers.from ) ) );
	const texts = fill( () => textDriver.map( () => Chars[Math.floor( Math.random() * Chars.length )] ).join( "" ) );
	const uuids = fill( () => Buffer.from( ( "0000" + Math.round( Math.random() * 65536 ).toString( 16 ) ).slice( -4 ).repeat( 8 ), "hex" ) );
	const data = { integers, numbers, texts, uuids };

	indexes.sort( () => Math.round( Math.random() * 11 ) - 5 );
	integers.sort( ( l, r ) => l - r );
	numbers.sort( ( l, r ) => l - r );
	texts.sort( ( l, r ) => l.localeCompare( r ) );
	uuids.sort( ( l, r ) => l.compare( r ) );

	before( "create model", () => {
		MyModel = Model.define( "MyModel", {
			props: {
				index: { type: "integer" },
				slowInteger: { type: "integer" },
				fastInteger: { type: "integer", index: "eq" },
				slowNumber: { type: "number" },
				fastNumber: { type: "number", index: "eq" },
				slowString: { type: "string" },
				fastString: { type: "string", index: "eq" },
				slowUUID: { type: "uuid" },
				fastUUID: { type: "uuid", index: "eq" },
			},
			computed: {
				derivedInteger: integerDeriver,
				derivedString: { code: stringDeriver, type: "string" },
				derivedUUID: { code: uuidDeriver, type: "uuid" },
			},
			indices: {
				derivedInteger: true,
				derivedString: {
					propertyType: "string",
				},
				derivedUUID: {
					propertyType: "uuid",
				},
			}
		} );

		return MyModel.adapter.purge().then( () => MyModel.indexLoaded );
	} );

	beforeEach( "creating test data", () => {
		return PromiseUtil.each( recordDriver, ( _, index ) => {
			const i = indexes[index];
			const item = new MyModel;

			item.index = i;
			item.slowInteger = integers[i];
			item.fastInteger = integers[i];
			item.slowNumber = numbers[i];
			item.fastNumber = numbers[i];
			item.slowString = texts[i];
			item.fastString = texts[i];
			item.slowUUID = uuids[i];
			item.fastUUID = uuids[i];

			return item.save();
		} );
	} );

	afterEach( () => {
		MyModel.adapter.purge();
		MyModel.indices.forEach( entry => entry.handler.clear() );
	} );



	it( "lists all generated records in unsorted order by default", () => {
		return MyModel.list()
			.then( records => {
				records.should.be.Array().which.has.length( NumRecords );
			} );
	} );

	it( "lists all generated records in unsorted order by default with metaCollector", () => {
		const metaCollector = {};

		return MyModel.list( undefined, { metaCollector } )
			.then( records => {
				records.should.be.Array().which.has.length( NumRecords );
				metaCollector.count.should.be.eql( NumRecords );
			} );
	} );

	[ 1, 2, 5, 10, 20, 34 ].forEach( limit => {
		[ 0, 1, 2, 5, 10, 20, 34 ].forEach( offset => {
			it( `lists excerpt of ${limit} record(s) of generated records in UNSORTED order, skipping ${offset} record(s)`, () => {
				return MyModel.list( { offset, limit } )
					.then( records => {
						records.should.be.Array().which.has.length( limit );
					} );
			} );
		} );
	} );

	Properties.forEach( ( [propertyName] ) => {
		[ 0, 1, 2, 5, 10, 20, 34 ].forEach( limit => {
			[ true, false ].forEach( dir => {
				let lastStart = dir ? -Infinity : Infinity;

				[ 0, 1, 2, 5, 10, 20, 34 ].forEach( offset => {
					it( `skips ${offset} record(s), then lists ${limit || "all left"} record(s) SORTED by ${propertyName} in ${dir ? "ascending" : "descending"} order on demand`, () => {
						return MyModel.list( { offset, limit: limit || Infinity, sortBy: propertyName, sortAscendingly: dir } )
							.then( records => {
								records.should.be.Array().which.has.length( limit || ( NumRecords - offset ) );

								if ( limit >= 5 ) {
									isSorted( records ).should.be[dir ? "true" : "false"]();
									isSorted( records, false ).should.be[dir ? "false" : "true"]();

									isStraight( records ).should.be[dir ? "true" : "false"]();
									isStraight( records, false ).should.be[dir ? "false" : "true"]();
								}

								records[0].index.should.not.be.equal( lastStart );
								lastStart = records[0].index;
							} );
					} );
				} );
			} );
		} );
	} );

	Properties.forEach( ( [ propertyName, dataName, deriver ] ) => {
		it( `retrieves single match when searching records with ${propertyName} equal every value used on filling database`, () => {
			return PromiseUtil.each( data[dataName], value => MyModel.find( { eq: {
				property: propertyName,
				value: deriver ? anyThis( deriver, value ) : value,
			} } )
				.then( records => {
					records.should.be.Array().which.has.length( 1 );
				} ) );
		} );
	} );

	Properties.forEach( ( [ propertyName, dataName, deriver ] ) => {
		it( `retrieves all but one record when searching records with ${propertyName} unequal every value used on filling database`, () => {
			const values = data[dataName];

			return PromiseUtil.each( values, value => MyModel.find( { neq: {
				name: propertyName,
				value: deriver ? anyThis( deriver, value ) : value,
			} } )
				.then( records => {
					records.should.be.Array().which.has.length( NumRecords - 1 );
				} ) );
		} );
	} );

	Properties.forEach( ( [ propertyName, dataName, deriver ] ) => {
		it( `retrieves multiple matches when searching records with ${propertyName} less than high values used on filling database`, () => {
			const values = data[dataName].slice( Math.floor( data[dataName].length / 2 ) );

			return PromiseUtil.each( values, value => MyModel.find( { lt: {
				name: propertyName,
				value: deriver ? anyThis( deriver, value ) : value,
			} } )
				.then( records => {
					records.should.be.Array();
					records.length.should.be.greaterThan( 1 );
				} ) );
		} );
	} );

	Properties.forEach( ( [ propertyName, dataName, deriver ] ) => {
		it( `retrieves multiple matches when searching records with ${propertyName} greater than high values used on filling database`, () => {
			const values = data[dataName].slice( 0, Math.floor( data[dataName].length / 2 ) );

			return PromiseUtil.each( values, value => MyModel.find( { gt: {
				name: propertyName,
				value: deriver ? anyThis( deriver, value ) : value,
			} } )
				.then( records => {
					records.should.be.Array();
					records.length.should.be.greaterThan( 1 );
				} ) );
		} );
	} );

	Properties.forEach( ( [ propertyName, dataName, deriver ] ) => {
		if ( propertyName.startsWith( "slow" ) ) {
			return;
		}

		it( `retrieves multiple matches when searching records with ${propertyName} between two distant values used on filling database`, () => {
			const lowers = data[dataName].slice( 0, Math.floor( data[dataName].length / 3 ) );
			const uppers = data[dataName].slice( Math.floor( data[dataName].length / 3 ) );
			const values = lowers.map( ( lower, i ) => [
				deriver ? anyThis( deriver, lower ) : lower,
				deriver ? anyThis( deriver, uppers[i] ) : uppers[i],
			] );

			return PromiseUtil.each( values, ( [ lower, upper ] ) => MyModel.find( { between: { name: propertyName, lower, upper } } )
				.then( records => {
					records.should.be.Array();
					records.length.should.be.greaterThan( 1 );
				} ) );
		} );

		it( `delivers records with ${propertyName} values in range on searching matches between two distant values used on filling database`, () => {
			const lowers = data[dataName].slice( 0, Math.floor( data[dataName].length / 3 ) );
			const uppers = data[dataName].slice( Math.floor( data[dataName].length / 3 ) );
			const values = lowers.map( ( lower, i ) => [
				deriver ? anyThis( deriver, lower ) : lower,
				deriver ? anyThis( deriver, uppers[i] ) : uppers[i],
			] );

			return PromiseUtil.each( values, ( [ lower, upper ] ) => MyModel.find( {
				between: { name: propertyName, lower, upper }
			}, undefined, { loadRecords: true } )
				.then( records => {
					records.should.be.Array();
					records.length.should.be.greaterThan( 1 );

					if ( dataName === "texts" ) {
						records.some( r => r[propertyName].localeCompare( lower ) < 0 || r[propertyName].localeCompare( upper ) > 0 ).should.be.false();
					} else if ( dataName === "uuids" ) {
						records.some( r => r[propertyName].compare( lower ) < 0 || r[propertyName].compare( upper ) > 0 ).should.be.false();
					} else {
						records.some( r => r[propertyName] < lower || r[propertyName] > upper ).should.be.false();
					}
				} ) );
		} );
	} );

	Properties.forEach( ( [ propertyName, dataName, deriver ] ) => {
		it( `retrieves two matches when searching records with ${propertyName} equal any of two solely existing values`, () => {
			const lowest = data[dataName][0];
			const highest = data[dataName][data[dataName].length - 1];

			return MyModel.find( { or: [
				{ eq: { [propertyName]: deriver ? anyThis( deriver, lowest ) : lowest } },
				{ eq: { [propertyName]: deriver ? anyThis( deriver, highest ) : highest } }
			] } )
				.then( records => {
					records.should.be.Array().which.has.length( 2 );

					if ( !deriver ) {
						const a = records.find( i => ( Buffer.isBuffer( i[propertyName] ) ? i[propertyName].equals( lowest ) : i[propertyName] === lowest ) );
						const b = records.find( i => ( Buffer.isBuffer( i[propertyName] ) ? i[propertyName].equals( highest ) : i[propertyName] === highest ) );

						a.should.be.ok();
						b.should.be.ok();
						a.should.not.equal( b );
					}
				} );
		} );
	} );

	Properties.forEach( ( [ propertyName, dataName, deriver ] ) => {
		it( `retrieves no matches when searching records with ${propertyName} equal both of two solely existing values`, () => {
			const lowest = data[dataName][0];
			const highest = data[dataName][data[dataName].length - 1];

			return MyModel.find( { and: [
				{ eq: { [propertyName]: deriver ? anyThis( deriver, lowest ) : lowest } },
				{ eq: { [propertyName]: deriver ? anyThis( deriver, highest ) : highest } },
			] } )
				.then( records => {
					records.should.be.Array().which.has.length( 0 );
				} );
		} );
	} );

	Properties.forEach( ( [ propertyName, dataName, deriver ] ) => {
		it( `supports nested combinations of tests`, () => {
			const lowest = data[dataName][0];
			const highest = data[dataName][data[dataName].length - 1];

			return MyModel.find( { and: [
				{ eq: { [propertyName]: deriver ? anyThis( deriver, lowest ) : lowest } },
				{ or: [
					{ eq: { [propertyName]: deriver ? anyThis( deriver, highest ) : highest } },
					{ true: true },
				] },
			] } )
				.then( records => {
					records.should.be.Array().which.has.length( 1 );

					if ( !deriver ) {
						const value = records[0][propertyName];

						if ( Buffer.isBuffer( value ) ) {
							value.equals( lowest ).should.be.true();
						} else {
							value.should.be.ok().and.equal( lowest );
						}
					}
				} );
		} );
	} );

	Properties.forEach( ( [ propertyName, dataName, deriver ] ) => {
		it( `retrieves records with ${propertyName} matching multiple gte/lte tests`, () => {
			const numValues = data[dataName].length;

			const lowerIndex = Math.floor( numValues * 0.25 );
			const upperIndex = Math.ceil( numValues * 0.75 );

			const lower = data[dataName][lowerIndex];
			const upper = data[dataName][upperIndex];

			return MyModel.find( { and: [
				{ gte: { [propertyName]: deriver ? anyThis( deriver, lower ) : lower } },
				{ lte: { [propertyName]: deriver ? anyThis( deriver, upper ) : upper } },
			] } )
				.then( records => {
					records.should.be.Array().which.has.length( upperIndex - lowerIndex + 1 );
				} );
		} );
	} );

	Properties.forEach( ( [ propertyName, dataName, deriver ] ) => {
		it( `retrieves records with ${propertyName} matching one of multiple values`, () => {
			const numValues = data[dataName].length;

			const lowerIndex = Math.floor( numValues * 0.25 );
			const upperIndex = Math.ceil( numValues * 0.75 );

			let lower = data[dataName][lowerIndex];
			let upper = data[dataName][upperIndex];

			if ( deriver ) {
				lower = anyThis( deriver, lower );
				upper = anyThis( deriver, upper );
			}

			return MyModel.find( { in: { [propertyName]: [ lower, upper ] } } )
				.then( records => {
					records.should.be.Array().which.has.length( 2 );
				} );
		} );
	} );
} );
