import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";
import "should";

const Test = await SDT( Core );

describe( "Definition of a model's schema", () => {
	const ctx = {};
	let Model, OdemModelType, OdemModelTypeInteger;

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
	} ) );

	before( () => {
		( { Model, OdemModelType, OdemModelTypeInteger } = ctx.hitchy.api.service );
	} );

	it( "may include anything but an index", () => {
		const MyModel = Model.define( "MyModel", {
			props: { someProp: {} },
		} );

		MyModel.schema.indices.should.be.Object().which.is.empty();

		return MyModel.indexLoaded
			.then( indices => {
				indices.should.be.Array().which.is.empty();
			} );
	} );

	it( "may have empty section of indices", () => {
		const MyModel = Model.define( "MyModel", {
			props: { someProp: {} },
			indices: {},
		} );


		MyModel.schema.indices.should.be.Object().which.is.empty();
		return MyModel.indexLoaded
			.then( indices => {
				indices.should.be.Array().which.is.empty();
			} );
	} );

	it( "may declare a property's index locally", () => {
		const MyModel = Model.define( "MyModel", {
			props: { someProp: { index: true } },
		} );

		MyModel.schema.indices.should.be.Object().which.is.not.empty();
		return MyModel.indexLoaded
			.then( indices => {
				indices.should.be.Array().which.is.not.empty();
			} );
	} );

	it( "may declare a property with its index declared in separate section", () => {
		const MyModel = Model.define( "MyModel", {
			props: { someProp: {} },
			indices: {
				someProp: true
			},
		} );

		MyModel.schema.indices.should.be.Object().which.is.not.empty();
		return MyModel.indexLoaded
			.then( indices => {
				indices.should.be.Array().which.is.not.empty();
			} );
	} );

	it( "may declare computed property with related index declared in separate section", () => {
		const MyModel = Model.define( "MyModel", {
			props: { someProp: {} },
			computed: {
				derived() { return String( this.someProp ).toLowerCase(); },
			},
			indices: {
				derived: true
			},
		} );

		MyModel.schema.indices.derived.property.should.be.equal( "derived" );
		MyModel.schema.indices.derived.type.should.be.equal( "eq" );
		( MyModel.schema.indices.derived.reducer == null ).should.be.true();

		return MyModel.indexLoaded
			.then( indices => {
				indices.should.be.Array().which.is.not.empty();
			} );
	} );

	it( "may declare computed property with its index declared in separate section using reducer", () => {
		const MyModel = Model.define( "MyModel", {
			props: { someProp: {} },
			computed: {
				derived() { return String( this.someProp ).toLowerCase(); },
			},
			indices: {
				derived( value ) { return value; },
			},
		} );

		MyModel.schema.indices.derived.property.should.be.equal( "derived" );
		MyModel.schema.indices.derived.type.should.be.equal( "eq" );
		( MyModel.schema.indices.derived.propertyType == null ).should.be.true();
		MyModel.schema.indices.derived.$type.should.be.equal( OdemModelType );
		MyModel.schema.indices.derived.reducer.should.be.Function();

		return MyModel.indexLoaded
			.then( indices => {
				indices.should.be.Array().which.is.not.empty();
			} );
	} );

	it( "may declare computed property with its index declared in separate section selecting type of expected values", () => {
		const MyModel = Model.define( "MyModel", {
			props: { someProp: {} },
			computed: {
				derived() { return String( this.someProp ).toLowerCase(); },
			},
			indices: {
				derived: {
					propertyType: "integer",
				},
			},
		} );

		MyModel.schema.indices.derived.property.should.be.equal( "derived" );
		MyModel.schema.indices.derived.type.should.be.equal( "eq" );
		MyModel.schema.indices.derived.propertyType.should.be.equal( "integer" );
		MyModel.schema.indices.derived.$type.should.be.equal( OdemModelTypeInteger );
		( MyModel.schema.indices.derived.reducer == null ).should.be.true();

		return MyModel.indexLoaded
			.then( indices => {
				indices.should.be.Array().which.is.not.empty();
			} );
	} );

	it( "must not declare index of same type for property twice in local and separate definition", () => {
		( () => Model.define( "MyModel", {
			props: { someProp: { index: true } },
			indices: {
				someProp: true,
			},
		} ) ).should.throw();
	} );

	it( "must not declare index of same type for property twice in separate section using mixture of shortcut and different index names", () => {
		( () => Model.define( "MyModel", {
			props: { someProp: {} },
			indices: {
				someProp: true,
				namedIndex: { property: "someProp" },
			},
		} ) ).should.throw();
	} );

	it( "must not declare index of same type for property twice in separate section using two named index definitions", () => {
		( () => Model.define( "MyModel", {
			props: { someProp: {} },
			indices: {
				namedIndex: { property: "someProp" },
				anotherNamedIndex: { property: "someProp" },
			},
		} ) ).should.throw();
	} );
} );
