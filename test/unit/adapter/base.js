import { join } from "node:path";

import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";
import Should from "should";

const Test = await SDT( Core );

describe( "Abstract Adapter", function() {
	const ctx = {};
	let OdemAdapter;

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
	} ) );

	before( () => {
		( { OdemAdapter } = ctx.hitchy.api.service );
	} );

	it( "is exposed in property `Adapter`", function() {
		Should( OdemAdapter ).be.ok();
	} );

	it( "can be used to create instance", function() {
		( () => new OdemAdapter() ).should.not.throw();
	} );

	it( "exposes instance methods of Adapter API", function() {
		const instance = new OdemAdapter();

		instance.should.have.property( "create" ).which.is.a.Function().of.length( 2 );
		instance.should.have.property( "has" ).which.is.a.Function().of.length( 1 );
		instance.should.have.property( "read" ).which.is.a.Function().of.length( 1 );
		instance.should.have.property( "write" ).which.is.a.Function().of.length( 2 );
		instance.should.have.property( "remove" ).which.is.a.Function().of.length( 1 );
		instance.should.have.property( "purge" ).which.is.a.Function().of.length( 0 );
		instance.should.have.property( "stream" ).which.is.a.Function().of.length( 0 );
		instance.should.have.property( "transaction" ).which.is.a.Function().of.length( 1 );
	} );

	it( "exposes capabilities of adapter instance revoking all possible capabilities by default", () => {
		const instance = new OdemAdapter();

		instance.can.should.be.Object().which.is.deepEqual( {
			test: false,
			read: false,
			write: false,
			remove: false,
			watch: false,
			stream: false,
			transact: false,
		} );
	} );

	it( "exposes class/static methods of Adapter API", function() {
		OdemAdapter.should.have.property( "keyToPath" ).which.is.a.Function().of.length( 1 );
		OdemAdapter.should.have.property( "pathToKey" ).which.is.a.Function().of.length( 1 );
	} );

	it( "returns promise on invoking create() which is rejected for being abstract base class", function() {
		const instance = new OdemAdapter();

		return instance.create( "model/%u", {} ).should.be.Promise().which.is.rejected();
	} );

	it( "returns promise on invoking has() which is rejected for being abstract base class", function() {
		const instance = new OdemAdapter();

		return instance.has( "model/some-id" ).should.be.Promise().which.is.rejected();
	} );

	it( "returns promise on invoking read() which is rejected for being abstract base class", function() {
		const instance = new OdemAdapter();

		return instance.read( "model/some-id" ).should.be.Promise().which.is.rejected();
	} );

	it( "returns promise on invoking write() which is rejected for being abstract base class", function() {
		const instance = new OdemAdapter();

		return instance.write( "model/some-id", {} ).should.be.Promise().which.is.rejected();
	} );

	it( "returns promise on invoking remove() which is rejected for being abstract base class", function() {
		const instance = new OdemAdapter();

		return instance.remove( "model/some-id" ).should.be.Promise().which.is.rejected();
	} );

	it( "returns promise on invoking transaction() which is rejected for being abstract base class", function() {
		const instance = new OdemAdapter();

		return instance.transaction( () => {} ).should.be.Promise().which.is.rejectedWith( /not supported/ );
	} );

	it( "returns keys w/o UUID as given on request for mapping it into some path name", function() {
		[
			"",
			"a",
			"some/test",
		].forEach( key => OdemAdapter.keyToPath( key ).replace( /\\/g, "/" ).should.be.String().which.is.equal( key ) );
	} );

	it( "returns keys w/ UUID as given on request for mapping it into some path name", function() {
		[
			"01234567-89ab-cdef-fedc-ba9876543210",
			"item/00000000-1111-2222-4444-888888888888",
		].forEach( key => OdemAdapter.keyToPath( key ).replace( /\\/g, "/" ).should.be.String().which.is.equal( key ) );
	} );

	it( "returns path names not related to some UUID as given on request for mapping it into some key", function() {
		[
			"",
			"a",
			join( "some", "test" ),
		].forEach( key => OdemAdapter.pathToKey( key ).should.be.String().which.is.equal( key ) );
	} );

	it( "returns path names related to some UUID as given on request for mapping it into some key", function() {
		[
			join( "0", "12", "34567-89ab-cdef-fedc-ba9876543210" ),
			join( "item", "0", "00", "00000-1111-2222-4444-888888888888" ),
		].forEach( key => OdemAdapter.pathToKey( key ).should.be.String().which.is.equal( key ) );
	} );
} );
