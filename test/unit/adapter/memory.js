import { join } from "node:path";
import { Readable } from "node:stream";

import { describe, it, before, after, beforeEach } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";
import Should from "should";

const Test = await SDT( Core );

describe( "OdemAdapterMemory", function() {
	const ctx = {};
	let OdemAdapter, OdemAdapterMemory;

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
	} ) );

	before( () => {
		( { OdemAdapter, OdemAdapterMemory } = ctx.hitchy.api.service );
	} );

	it( "is exposed as service component", function() {
		Should( OdemAdapterMemory ).be.ok();
	} );

	it( "can be used to create instance", function() {
		( () => new OdemAdapterMemory() ).should.not.throw();
	} );

	it( "is derived from basic Adapter", function() {
		new OdemAdapterMemory().should.be.instanceOf( OdemAdapter );
	} );

	it( "exposes instance methods of Adapter API", function() {
		const instance = new OdemAdapterMemory();

		instance.should.have.property( "create" ).which.is.a.Function().of.length( 2 );
		instance.should.have.property( "has" ).which.is.a.Function().of.length( 1 );
		instance.should.have.property( "read" ).which.is.a.Function().of.length( 1 );
		instance.should.have.property( "write" ).which.is.a.Function().of.length( 2 );
		instance.should.have.property( "remove" ).which.is.a.Function().of.length( 1 );
		instance.should.have.property( "purge" ).which.is.a.Function().of.length( 0 );
		instance.should.have.property( "stream" ).which.is.a.Function().of.length( 0 );
		instance.should.have.property( "transaction" ).which.is.a.Function().of.length( 1 );
	} );

	it( "exposes capabilities of adapter instance granting all possible capabilities by default", () => {
		const instance = new OdemAdapterMemory();

		instance.can.should.be.Object().which.is.deepEqual( {
			test: true,
			read: true,
			write: true,
			remove: true,
			watch: true,
			stream: true,
			transact: true,
		} );
	} );

	it( "exposes class/static methods of Adapter API", function() {
		OdemAdapterMemory.should.have.property( "keyToPath" ).which.is.a.Function().of.length( 1 );
		OdemAdapterMemory.should.have.property( "pathToKey" ).which.is.a.Function().of.length( 1 );
	} );

	it( "returns promise on invoking create() which is resolved with key of created record", function() {
		const instance = new OdemAdapterMemory();

		const myData = { someProperty: "its value" };

		return instance.create( "model/%u", myData ).should.be.Promise().which.is.resolved()
			.then( key => {
				key.should.be.String().which.is.not.empty().and.startWith( "model/" ).and.not.endWith( "%u" );

				return instance.read( key ).should.be.Promise().which.is.resolvedWith( myData );
			} );
	} );

	it( "returns promise on invoking read() which is rejected on missing record and resolved with data on existing record", function() {
		const instance = new OdemAdapterMemory();

		const myData = { someProperty: "its value" };

		return instance.read( "model/some-id" ).should.be.Promise().which.is.rejected()
			.then( () => instance.write( "model/some-id", myData ) )
			.then( () => instance.read( "model/some-id" ).should.be.Promise().which.is.resolvedWith( myData ) );
	} );

	it( "promises provided fallback value on trying to read() missing record", function() {
		const instance = new OdemAdapterMemory();

		const myFallbackData = { someProperty: "its value" };

		return instance.read( "model/some-id" ).should.be.Promise().which.is.rejected()
			.then( () => instance.read( "model/some-id", { ifMissing: myFallbackData } ).should.be.Promise().which.is.resolvedWith( myFallbackData ) );
	} );

	it( "returns promise on invoking write() which is resolved with written data", function() {
		const instance = new OdemAdapterMemory();

		const myData = { someProperty: "its value" };

		return instance.write( "model/some-id", myData ).should.be.Promise().which.is.resolved()
			.then( result => {
				result.should.equal( myData );
			} );
	} );

	it( "returns promise on invoking has() which is resolved with information on having selected record or not", function() {
		const instance = new OdemAdapterMemory();

		return instance.has( "model/some-id" ).should.be.Promise().which.is.resolved()
			.then( exists => {
				Should( exists ).be.false();
			} )
			.then( () => instance.write( "model/some-id", {} ) )
			.then( () => instance.has( "model/some-id" ).should.be.Promise().which.is.resolved() )
			.then( exists => {
				Should( exists ).be.true();
			} );
	} );

	it( "returns promise on invoking remove() which is resolved with key of record no matter if record exists or not", function() {
		const instance = new OdemAdapterMemory();

		return instance.remove( "model/some-id" ).should.be.Promise().which.is.resolvedWith( "model/some-id" )
			.then( instance.write( "model/some-id", { someProperty: "its value" } ) )
			.then( instance.remove( "model/some-id" ).should.be.Promise().which.is.resolvedWith( "model/some-id" ) );
	} );

	describe( "returns promise on invoking transaction() which", () => {
		it( "is rejected when invoked without transaction callback", async() => {
			const instance = new OdemAdapterMemory();

			await instance.transaction().should.be.rejectedWith( /callback/ );
		} );

		it( "is always resolved with undefined", async() => {
			const instance = new OdemAdapterMemory();

			await instance.transaction( () => {} ).should.be.resolvedWith( undefined );
			await instance.transaction( () => ( {} ) ).should.be.resolvedWith( undefined );
		} );

		it( "is rejected if provided transaction callback is throwing", function() {
			const instance = new OdemAdapterMemory();

			return instance.transaction( () => {
				throw new Error( "foo" );
			} ).should.be.Promise().which.is.rejectedWith( "foo" );
		} );

		it( "is rejected if provided transaction callback tries to start another transaction", function() {
			const instance = new OdemAdapterMemory();

			return instance.transaction( tx => tx.transaction( () => {} ) ).should.be.Promise().which.is.rejectedWith( /transaction/ );
		} );
	} );

	it( "exposes different adapter with limited capabilities in argument to a provided transaction callback", async() => {
		const instance = new OdemAdapterMemory();

		await instance.transaction( tx => {
			tx.should.not.equal( instance );
			tx.purge.should.be.Function();
			tx.has.should.be.Function();
			tx.read.should.be.Function();
			tx.write.should.be.Function();
			tx.remove.should.be.Function();
			tx.stream.should.be.Function();
			tx.transaction.should.be.Function();
			tx.can.should.deepEqual( {
				test: true,
				read: true,
				write: true,
				remove: true,
				stream: true,
				watch: true,
				transact: false,
			} );
		} ).should.be.resolved();
	} );

	it( "returns keys w/o UUID as given on request for mapping it into some path name", function() {
		[
			"",
			"a",
			"some/test",
		].forEach( key => OdemAdapterMemory.keyToPath( key ).replace( /\\/g, "/" ).should.be.String().which.is.equal( key ) );
	} );

	it( "returns keys w/ UUID as given on request for mapping it into some path name", function() {
		[
			"01234567-89ab-cdef-fedc-ba9876543210",
			"item/00000000-1111-2222-4444-888888888888",
		].forEach( key => OdemAdapterMemory.keyToPath( key ).replace( /\\/g, "/" ).should.be.String().which.is.equal( key ) );
	} );

	it( "returns path names not related to some UUID as given on request for mapping it into some key", function() {
		[
			"",
			"a",
			join( "some", "test" ),
		].forEach( key => OdemAdapterMemory.pathToKey( key ).should.be.String().which.is.equal( key ) );
	} );

	it( "returns path names related to some UUID as given on request for mapping it into some key", function() {
		[
			join( "0", "12", "34567-89ab-cdef-fedc-ba9876543210" ),
			join( "item", "0", "00", "00000-1111-2222-4444-888888888888" ),
		].forEach( key => OdemAdapterMemory.pathToKey( key ).should.be.String().which.is.equal( key ) );
	} );

	describe( "provides `keyStream()` which", function() {
		let adapter;

		beforeEach( function() {
			adapter = new OdemAdapterMemory( {} );

			return adapter.write( "some/key/without/uuid-1", { id: "first" } )
				.then( () => adapter.write( "some/key/without/uuid-2", { id: "second" } ) )
				.then( () => adapter.write( "some/other/key/without/uuid-3", { id: "third" } ) )
				.then( () => adapter.write( "some/key/with/uuid/12345678-1234-1234-1234-1234567890ab", { id: "fourth" } ) )
				.then( () => adapter.write( "some/key/with/uuid/00000000-0000-0000-0000-000000000000", { id: "fifth" } ) );
		} );

		it( "is a function", function() {
			adapter.should.have.property( "keyStream" ).which.is.a.Function();
		} );

		it( "returns a readable stream", function() {
			return new Promise( resolve => {
				const stream = adapter.keyStream();

				stream.should.be.instanceOf( Readable );
				stream.on( "end", resolve );
				stream.resume();
			} );
		} );

		it( "generates keys of all records in selected datasource by default", function() {
			return new Promise( resolve => {
				const streamed = [];
				const stream = adapter.keyStream();

				stream.should.be.instanceOf( Readable );
				stream.on( "data", data => streamed.push( data ) );
				stream.on( "end", () => {
					streamed.should.be.Array().which.has.length( 5 );

					streamed.sort();

					streamed.should.eql( [
						"some/key/with/uuid/00000000-0000-0000-0000-000000000000",
						"some/key/with/uuid/12345678-1234-1234-1234-1234567890ab",
						"some/key/without/uuid-1",
						"some/key/without/uuid-2",
						"some/other/key/without/uuid-3",
					] );

					resolve();
				} );
			} );
		} );

		it( "generates keys of all records in selected datasource matching some selected prefix", function() {
			return new Promise( resolve => {
				const streamed = [];
				const stream = adapter.keyStream( {
					prefix: "some/key/without",
				} );

				stream.should.be.instanceOf( Readable );
				stream.on( "data", data => streamed.push( data ) );
				stream.on( "end", () => {
					streamed.should.be.Array().which.has.length( 2 );

					streamed.sort();

					streamed.should.eql( [
						"some/key/without/uuid-1",
						"some/key/without/uuid-2",
					] );

					resolve();
				} );
			} );
		} );

		it( "generates no key if prefix does not select any folder or single record in backend", function() {
			return new Promise( resolve => {
				const streamed = [];
				const stream = adapter.keyStream( {
					prefix: "some/missing/key",
				} );

				stream.should.be.instanceOf( Readable );
				stream.on( "data", data => streamed.push( data ) );
				stream.on( "end", () => {
					streamed.should.be.Array().which.is.empty();
					resolve();
				} );
			} );
		} );

		it( "generates no key if prefix partially matching key of some folder in backend, only", function() {
			return new Promise( resolve => {
				const streamed = [];
				const stream = adapter.keyStream( {
					prefix: "some/key/wit",
				} );

				stream.should.be.instanceOf( Readable );
				stream.on( "data", data => streamed.push( data ) );
				stream.on( "end", () => {
					streamed.should.be.Array().which.is.empty();
					resolve();
				} );
			} );
		} );

		it( "generates some matching record's key used as prefix, only", function() {
			return new Promise( resolve => {
				const streamed = [];
				const stream = adapter.keyStream( {
					prefix: "some/key/without/uuid-1",
				} );

				stream.should.be.instanceOf( Readable );
				stream.on( "data", data => streamed.push( data ) );
				stream.on( "end", () => {
					streamed.should.be.Array().which.has.length( 1 );
					streamed.should.eql( ["some/key/without/uuid-1"] );
					resolve();
				} );
			} );
		} );

		it( "generates keys of all records in selected datasource up to some requested maximum depth", function() {
			return new Promise( resolve => {
				const streamed = [];
				const stream = adapter.keyStream( {
					maxDepth: 4,
				} );

				stream.should.be.instanceOf( Readable );
				stream.on( "data", data => streamed.push( data ) );
				stream.on( "end", () => {
					streamed.should.be.Array().which.has.length( 2 );

					streamed.sort();

					streamed.should.eql( [
						"some/key/without/uuid-1",
						"some/key/without/uuid-2",
					] );

					resolve();
				} );
			} );
		} );

		it( "generates keys of all records in selected datasource with requested maximum depth considered relative to given prefix", function() {
			return new Promise( resolve => {
				const streamed = [];
				const stream = adapter.keyStream( {
					prefix: "some/key",
					maxDepth: 2,
				} );

				stream.should.be.instanceOf( Readable );
				stream.on( "data", data => streamed.push( data ) );
				stream.on( "end", () => {
					streamed.should.be.Array().which.has.length( 2 );

					streamed.sort();

					streamed.should.eql( [
						"some/key/without/uuid-1",
						"some/key/without/uuid-2",
					] );

					resolve();
				} );
			} );
		} );

		it( "obeys key depth instead of backend path depth which is higher due to splitting contained UUIDs into several segments", function() {
			return adapter.write( "some/12345678-1234-1234-1234-1234567890ab", {} )
				.then( () => adapter.write( "some/00000000-0000-0000-0000-000000000000", {} ) )
				.then( () => adapter.write( "some/non-UUID", {} ) )
				.then( () => adapter.write( "some/deeper/00000000-0000-0000-0000-000000000000", {} ) )
				.then( () => new Promise( resolve => {
					const streamed = [];
					const stream = adapter.keyStream( {
						prefix: "some",
						maxDepth: 1,
					} );

					stream.should.be.instanceOf( Readable );
					stream.on( "data", data => streamed.push( data ) );
					stream.on( "end", () => {
						streamed.should.be.Array().which.has.length( 3 );

						streamed.sort();

						streamed.should.eql( [
							"some/00000000-0000-0000-0000-000000000000",
							"some/12345678-1234-1234-1234-1234567890ab",
							"some/non-UUID",
						] );

						resolve();
					} );
				} ) );
		} );
	} );
} );
