import { env } from "node:process";
import { describe, it, afterEach } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

const Test = await SDT( Core );

const configuresCacheInEnv = env.ODEM_DISABLE_CACHE || env.ODEM_FORCE_CACHE || env.ODEM_CACHE_TTL;

( configuresCacheInEnv ? describe.skip : describe )( "Hitchy runtime configuration", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );

	it( "does not disable all model-related runtime-caching by default", async() => {
		await Test.before( ctx, {
			plugin: true,
		} )();

		ctx.hitchy.api.config.database.disableCache.should.be.false();
	} );

	it( "supports disabling all model-related runtime-caching via CLI argument", async() => {
		await Test.before( ctx, {
			plugin: true,
			options: {
				arguments: {
					"odem-disable-cache": true,
				}
			}
		} )();

		ctx.hitchy.api.config.database.disableCache.should.be.true();
	} );

	it( "does not enable eager model-related runtime-caching by default", async() => {
		await Test.before( ctx, {
			plugin: true,
		} )();

		ctx.hitchy.api.config.database.eagerCache.should.be.false();
	} );

	it( "supports enabling eager model-related runtime-caching via CLI argument", async() => {
		await Test.before( ctx, {
			plugin: true,
			options: {
				arguments: {
					"odem-eager-cache": true,
				}
			}
		} )();

		ctx.hitchy.api.config.database.eagerCache.should.be.true();
	} );

	it( "disables aging of model-related runtime-caching by default", async() => {
		await Test.before( ctx, {
			plugin: true,
		} )();

		ctx.hitchy.api.config.database.cacheTTL.should.be.equal( 0 );
	} );

	it( "supports enabling aging of model-related runtime-caching via CLI argument", async() => {
		await Test.before( ctx, {
			plugin: true,
			options: {
				arguments: {
					"odem-cache-ttl": "300",
				}
			}
		} )();

		ctx.hitchy.api.config.database.cacheTTL.should.be.equal( 300 );
	} );
} );
