import { describe, it, beforeEach, afterEach } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";
import "should";

const Test = await SDT( Core );

const mockedPluginAuthFiles = {
	"initialize.js": `export default function() { this.plugins.authentication = true; }`,
	"api/service/auth-manager.js": ` export const adminRole = "admin";`,
	"api/service/authorization-tree.js": `export default function() {
		const api = this;
		
		return { current: { 
			isAuthorized: ( resource, user, roles ) => {
				(api.data.testedAuthorizations ??= []).push( { resource, user, roles } );
	
				return Boolean( api.data.mockedAuthorization ); 
			} 
		} };
	};`,
};

const modelFiles = {
	"api/model/foo.js": `export default { props: { a: {}, b: { public: true }, c: { protected: true }, d: { private: true }, e: { readonly: true }, f: { readonly: true, public: true }, g: { readonly: true, protected: true }, h: { readonly: true, private: true } } }`,
	"api/model/bar.js": `export default { props: { a: {}, b: { public: true }, c: { protected: true }, d: { private: true }, e: { readonly: true }, f: { readonly: true, public: true }, g: { readonly: true, protected: true }, h: { readonly: true, private: true } }, options: { expose: "public", promote: "public" } }`,
	"api/model/baz.js": `export default { props: { a: {}, b: { public: true }, c: { protected: true }, d: { private: true }, e: { readonly: true }, f: { readonly: true, public: true }, g: { readonly: true, protected: true }, h: { readonly: true, private: true } }, options: { expose: "protected", promote: "protected" } }`,
	"api/model/bam.js": `export default { props: { a: {}, b: { public: true }, c: { protected: true }, d: { private: true }, e: { readonly: true }, f: { readonly: true, public: true }, g: { readonly: true, protected: true }, h: { readonly: true, private: true } }, options: { expose: "private", promote: "private" } }`,

	"api/model/boo.js": `export default { props: { a: {}, b: { public: true }, c: { protected: "custom.auth.rule" }, d: { private: true } } }`,
	"api/model/far.js": `export default { props: { a: {}, b: { public: true }, c: { protected: "custom.auth.rule" }, d: { private: true } }, options: { expose: "public", promote: "public" } }`,
	"api/model/faz.js": `export default { props: { a: {}, b: { public: true }, c: { protected: "custom.auth.rule" }, d: { private: true } }, options: { expose: "protected", promote: "protected" } }`,
	"api/model/fam.js": `export default { props: { a: {}, b: { public: true }, c: { protected: "custom.auth.rule" }, d: { private: true } }, options: { expose: "private", promote: "private" } }`,
};

const regularAdmin = { name: "admin", roles: [ { name: "user" }, { name: "maintainer" }, { name: "owner" } ] };
const privilegedAdmin = { name: "admin", roles: [{ name: "admin" }] };
const regularJohn = { name: "john.doe" };
const privilegedJohn = { name: "john.doe", roles: [{ name: "admin" }] };

const regularAdminRequest = { user: regularAdmin };
const privilegedAdminRequest = { user: privilegedAdmin };
const regularJohnRequest = { user: regularJohn };
const privilegedJohnRequest = { user: privilegedJohn };

const someRegularUser = [ regularJohn, regularAdmin ];
const somePrivilegedUser = [ privilegedJohn, privilegedAdmin ];
const someUser = [ ...someRegularUser, ...somePrivilegedUser ];
const anyUser = [ undefined, ...someUser ];

const someRegularUserRequest = [ regularJohnRequest, regularAdminRequest ];
const somePrivilegedUserRequest = [ privilegedJohnRequest, privilegedAdminRequest ];
const someUserRequest = [ ...someRegularUserRequest, ...somePrivilegedUserRequest ];
const anyUserRequest = [ {}, ...someUserRequest ];

const combine2 = ( a, b ) => {
	const result = [];

	for ( const i of a ) {
		for ( const j of b ) {
			result.push( [ i, j ] );
		}
	}

	return result;
};

const combine3 = ( a, b, c ) => {
	const result = [];

	for ( const i of a ) {
		for ( const j of b ) {
			for ( const k of c ) {
				result.push( [ i, j, k ] );
			}
		}
	}

	return result;
};

describe( "In a project without authentication plugin, OdemSchema service", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );
	beforeEach( Test.before( ctx, {
		projectFolder: false,
		plugin: true,
		files: {
			...mockedPluginAuthFiles,
			...modelFiles,
			"initialize.js": `export default function() {}`,
		}
	} ) );

	it( "is exposed", () => {
		ctx.hitchy.api.service.OdemSchema.should.be.ok();
	} );

	describe( "has method isAdmin() which", () => {
		it( "can be invoked", () => {
			( () => ctx.hitchy.api.service.OdemSchema.isAdmin() ).should.not.throw();
		} );

		it( "returns false when invoked without a user", () => {
			ctx.hitchy.api.service.OdemSchema.isAdmin().should.be.false();
		} );

		it( "returns false when invoked with a user assumed to have no role assigned", () => {
			ctx.hitchy.api.service.OdemSchema.isAdmin( { roles: [] } ).should.be.false();
		} );

		it( "returns false when invoked with a user assumed to have non-admin roles assigned", () => {
			ctx.hitchy.api.service.OdemSchema.isAdmin( { roles: [ "user", "maintainer", "owner" ] } ).should.be.false();
		} );

		it( "returns false when invoked with a user assumed to have role 'admin' assigned", () => {
			ctx.hitchy.api.service.OdemSchema.isAdmin( { roles: ["admin"] } ).should.be.false();
		} );

		it( "returns false when invoked with a user assumed to have several roles including 'admin' assigned", () => {
			ctx.hitchy.api.service.OdemSchema.isAdmin( { roles: [ "user", "maintainer", "owner", "admin" ] } ).should.be.false();
		} );
	} );

	describe( "has method mayBeExposed() which ", () => {
		it( "requires an optionally authenticated request and a model on invocation", () => {
			( () => ctx.hitchy.api.service.OdemSchema.mayBeExposed() ).should.throw();
			( () => ctx.hitchy.api.service.OdemSchema.mayBeExposed( {} ) ).should.throw();

			( () => ctx.hitchy.api.service.OdemSchema.mayBeExposed( {}, ctx.hitchy.api.model.Foo ) ).should.not.throw();
			( () => ctx.hitchy.api.service.OdemSchema.mayBeExposed( regularJohnRequest, ctx.hitchy.api.model.Foo ) ).should.not.throw();
		} );

		it( "returns true when invoked on an implicitly public model no matter the requesting user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			anyUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBeExposed( req, ctx.hitchy.api.model.Foo ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true when invoked on an explicitly public model no matter the user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			anyUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBeExposed( req, ctx.hitchy.api.model.Bar ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false when invoked on a protected model without an authenticated user in given request", () => {
			ctx.hitchy.api.data.mockedAuthorization = true;

			ctx.hitchy.api.service.OdemSchema.mayBeExposed( {}, ctx.hitchy.api.model.Baz ).should.be.false();

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true when invoked on a protected model with any authenticated user in given request", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			ctx.hitchy.api.service.OdemSchema.mayBeExposed( regularJohnRequest, ctx.hitchy.api.model.Baz ).should.be.true();
			ctx.hitchy.api.service.OdemSchema.mayBeExposed( privilegedJohnRequest, ctx.hitchy.api.model.Baz ).should.be.true();
			ctx.hitchy.api.service.OdemSchema.mayBeExposed( regularAdminRequest, ctx.hitchy.api.model.Baz ).should.be.true();
			ctx.hitchy.api.service.OdemSchema.mayBeExposed( privilegedAdminRequest, ctx.hitchy.api.model.Baz ).should.be.true();

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false when invoked on a private model no matter the user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = true;

			anyUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBeExposed( req, ctx.hitchy.api.model.Bam ).should.be.false() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );
	} );

	describe( "has method mayBePromoted() which ", () => {
		it( "requires an optionally authenticated request and a model on invocation", () => {
			( () => ctx.hitchy.api.service.OdemSchema.mayBePromoted() ).should.throw();
			( () => ctx.hitchy.api.service.OdemSchema.mayBePromoted( {} ) ).should.throw();

			( () => ctx.hitchy.api.service.OdemSchema.mayBePromoted( {}, ctx.hitchy.api.model.Foo ) ).should.not.throw();
			( () => ctx.hitchy.api.service.OdemSchema.mayBePromoted( regularJohnRequest, ctx.hitchy.api.model.Foo ) ).should.not.throw();
		} );

		it( "returns true when invoked on an implicitly public model no matter the user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			anyUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBePromoted( req, ctx.hitchy.api.model.Foo ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true when invoked on an explicitly public model no matter the user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			anyUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBePromoted( req, ctx.hitchy.api.model.Bar ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false when invoked on a protected model without an authenticated user in given request", () => {
			ctx.hitchy.api.data.mockedAuthorization = true;

			ctx.hitchy.api.service.OdemSchema.mayBePromoted( {}, ctx.hitchy.api.model.Baz ).should.be.false();

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true when invoked on a protected model with any authenticated user in given request", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			someUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBePromoted( req, ctx.hitchy.api.model.Baz ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false when invoked on a private model no matter the user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = true;

			anyUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBePromoted( req, ctx.hitchy.api.model.Bam ).should.be.false() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );
	} );

	describe( "has method mayAccessScope() which", () => {
		it( "can be invoked", () => {
			( () => ctx.hitchy.api.service.OdemSchema.mayAccessScope() ).should.not.throw();
		} );

		it( "returns false when invoked without any arguments", () => {
			ctx.hitchy.api.service.OdemSchema.mayAccessScope().should.be.false();
		} );

		it( "returns true for public scope no matter the resource or the given user", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			ctx.hitchy.api.service.OdemSchema.mayAccessScope( "public" ).should.be.true();
			ctx.hitchy.api.service.OdemSchema.mayAccessScope( "public", "some.resource" ).should.be.true();
			someUser.forEach( user => ctx.hitchy.api.service.OdemSchema.mayAccessScope( "public", "some.resource", user ).should.be.true() );
		} );

		it( "does not check authorization for public scope no matter the resource or the given user", () => {
			ctx.hitchy.api.service.OdemSchema.mayAccessScope( "public" );
			ctx.hitchy.api.service.OdemSchema.mayAccessScope( "public", "some.resource" );
			someUser.forEach( user => ctx.hitchy.api.service.OdemSchema.mayAccessScope( "public", "some.resource", user ) );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false for protected scope no matter the resource and without a user given", () => {
			ctx.hitchy.api.service.OdemSchema.mayAccessScope( "protected" ).should.be.false();
			ctx.hitchy.api.service.OdemSchema.mayAccessScope( "protected", "some.resource" ).should.be.false();
		} );

		it( "returns true for protected scope no matter the resource and with a user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			someUser.forEach( user => ctx.hitchy.api.service.OdemSchema.mayAccessScope( "protected", "some.resource", user ).should.be.true() );
		} );

		it( "does not check authorization for protected scope no matter the resource or the given user", () => {
			ctx.hitchy.api.service.OdemSchema.mayAccessScope( "protected" );
			ctx.hitchy.api.service.OdemSchema.mayAccessScope( "protected", "some.resource" );
			someUser.forEach( user => ctx.hitchy.api.service.OdemSchema.mayAccessScope( "protected", "some.resource", user ) );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false for private scope no matter the resource or the given user", () => {
			ctx.hitchy.api.data.mockedAuthorization = true;

			ctx.hitchy.api.service.OdemSchema.mayAccessScope( "private" ).should.be.false();
			ctx.hitchy.api.service.OdemSchema.mayAccessScope( "private", "some.resource" ).should.be.false();
			someUser.forEach( user => ctx.hitchy.api.service.OdemSchema.mayAccessScope( "private", "some.resource", user ).should.be.false() );
		} );

		it( "does not check authorization for private scope no matter the resource or the given user", () => {
			ctx.hitchy.api.service.OdemSchema.mayAccessScope( "private" );
			ctx.hitchy.api.service.OdemSchema.mayAccessScope( "private", "some.resource" );
			someUser.forEach( user => ctx.hitchy.api.service.OdemSchema.mayAccessScope( "private", "some.resource", user ).should.be.false() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );
	} );

	describe( "has method mayAccessProperty() which", () => {
		it( "requires a model on invocation", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo } = ctx.hitchy.api.model;

			( () => OdemSchema.mayAccessProperty() ).should.throw();
			( () => OdemSchema.mayAccessProperty( undefined, undefined ) ).should.throw();

			combine3( [ "read", "write" ], [ undefined, "propName" ], anyUser ).forEach( ( [ mode, prop, user ] ) => {
				( () => OdemSchema.mayAccessProperty( prop, user, undefined ) ).should.throw();
				( () => OdemSchema.mayAccessProperty( prop, user, undefined, mode ) ).should.throw();

				( () => OdemSchema.mayAccessProperty( prop, user, Foo ) ).should.not.throw();
				( () => OdemSchema.mayAccessProperty( prop, user, Foo, mode ) ).should.not.throw();
			} );
		} );

		it( "returns false for testing nullishly named property of any model no matter the given user or access mode or the model's exposure", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = true;

			combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ "read", "write" ], anyUser ).forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( undefined, user, model ).should.be.false( `${user?.name} ${undefined}ing ${model.name}.undefined` );
				OdemSchema.mayAccessProperty( undefined, user, model, mode ).should.be.false( `${user?.name} ${mode}ing ${model.name}.undefined` );

				OdemSchema.mayAccessProperty( null, user, model ).should.be.false( `${user?.name} ${undefined}ing ${model.name}.null` );
				OdemSchema.mayAccessProperty( null, user, model, mode ).should.be.false( `${user?.name} ${mode}ing ${model.name}.null` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true for properties not defined in any of the tested models no matter the given user or access mode or the model's exposure", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = false;

			combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ "read", "write" ], anyUser ).forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "propName", user, model ).should.be.true( `${user?.name} ${undefined}ing ${model.name}.propName` );
				OdemSchema.mayAccessProperty( "propName", user, model, mode ).should.be.true( `${user?.name} ${mode}ing ${model.name}.propName` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true for implicitly public properties of any model no matter the given user or access mode or the model's exposure", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = false;

			combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ "read", "write" ], anyUser ).forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "a", user, model ).should.be.true( `${user?.name} ${undefined}ing ${model.name}.a` );
				OdemSchema.mayAccessProperty( "a", user, model, mode ).should.be.true( `${user?.name} ${mode}ing ${model.name}.a` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true for explicitly public properties of any model no matter the given user or access mode or the model's exposure", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = false;

			combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ "read", "write" ], anyUser ).forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "b", user, model ).should.be.true( `${user?.name} ${undefined}ing ${model.name}.b` );
				OdemSchema.mayAccessProperty( "b", user, model, mode ).should.be.true( `${user?.name} ${mode}ing ${model.name}.b` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false for protected properties of any model for guest users no matter the given access mode or the model's exposure", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = true;

			combine2( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ "read", "write" ] ).forEach( ( [ model, mode ] ) => {
				OdemSchema.mayAccessProperty( "c", undefined, model ).should.be.false( `${undefined} ${undefined}ing ${model.name}.c` );
				OdemSchema.mayAccessProperty( "c", undefined, model, mode ).should.be.false( `${undefined} ${mode}ing ${model.name}.c` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true for protected properties of any model for authenticated users no matter the given access mode or the model's exposure", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = false;

			combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ "read", "write" ], someUser ).forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "c", user, model ).should.be.true( `${user?.name} ${undefined}ing ${model.name}.c` );
				OdemSchema.mayAccessProperty( "c", user, model, mode ).should.be.true( `${user?.name} ${mode}ing ${model.name}.c` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false for private properties of any model no matter the given user or access mode or the model's exposure", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = true;

			combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ "read", "write" ], anyUser ).forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "d", user, model ).should.be.false( `${user?.name} ${undefined}ing ${model.name}.d` );
				OdemSchema.mayAccessProperty( "d", user, model, mode ).should.be.false( `${user?.name} ${mode}ing ${model.name}.d` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );
	} );

	describe( "has method filterItem() which", () => {
		it( "requires an item to process and a request descriptor on invocation", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			( () => OdemSchema.filterItem() ).should.throw();
			( () => OdemSchema.filterItem( {} ) ).should.throw();
			( () => OdemSchema.filterItem( {}, {} ) ).should.not.throw();
		} );

		it( "requires a model on processing a non-empty item", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				( () => OdemSchema.filterItem( { foo: null }, request ) ).should.throw();
				( () => OdemSchema.filterItem( { foo: null }, request, undefined, "read" ) ).should.throw();
				( () => OdemSchema.filterItem( { foo: null }, request, undefined, "write" ) ).should.throw();

				( () => OdemSchema.filterItem( { foo: null }, request, model ) ).should.not.throw();
				( () => OdemSchema.filterItem( { foo: null }, request, model, "read" ) ).should.not.throw();
				( () => OdemSchema.filterItem( { foo: null }, request, model, "write" ) ).should.not.throw();
			} );
		} );

		it( "does not require a model on processing an empty item for a guest' or a regular user's request", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			( () => OdemSchema.filterItem( {}, {} ) ).should.not.throw();
			( () => OdemSchema.filterItem( {}, regularJohnRequest ) ).should.not.throw();
			( () => OdemSchema.filterItem( {}, regularJohnRequest, undefined, "read" ) ).should.not.throw();
			( () => OdemSchema.filterItem( {}, regularJohnRequest, undefined, "write" ) ).should.not.throw();
		} );

		it( "keeps properties not declared by given model in provided item no matter the model's own visibility", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { foo: null }, request, model ).should.have.property( "foo" );
				OdemSchema.filterItem( { z: null }, request, model ).should.have.property( "z" );
			} );
		} );

		it( "keeps implicitly public properties of given model in provided item no matter the model's own visibility", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { a: null }, request, model ).should.have.property( "a" );
			} );
		} );

		it( "keeps explicitly public properties of given model in provided item no matter the model's own visibility", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { b: null }, request, model ).should.have.property( "b" );
			} );
		} );

		it( "keeps protected properties of given model in provided item no matter the model's own visibility for regular users", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( someRegularUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { c: null }, request, model ).should.have.property( "c" );
			} );
		} );

		it( "keeps protected properties of given model in provided item no matter the model's own visibility for regular users", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( somePrivilegedUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { c: null }, request, model ).should.have.property( "c" );
			} );
		} );

		it( "removes protected properties of given model in provided item no matter the model's own visibility for guests", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( [{}], [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { c: null }, request, model ).should.not.have.property( "c" );
			} );
		} );

		it( "removes private properties of given model in provided item no matter the model's own visibility", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { d: null }, request, model ).should.not.have.property( "d" );
			} );
		} );

		it( "removes properties declared as `readonly` in `write` mode without regard to the user or the visibility of the property or the model", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { e: null }, request, model ).should.have.property( "e" );
				OdemSchema.filterItem( { e: null }, request, model, "read" ).should.have.property( "e" );
				OdemSchema.filterItem( { e: null }, request, model, "write" ).should.not.have.property( "e" );
				OdemSchema.filterItem( { f: null }, request, model, "write" ).should.not.have.property( "f" );
				OdemSchema.filterItem( { g: null }, request, model, "write" ).should.not.have.property( "g" );
				OdemSchema.filterItem( { h: null }, request, model, "write" ).should.not.have.property( "h" );
			} );
		} );
	} );
} );

describe( "In a project with authentication plugin, OdemSchema service", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );
	beforeEach( Test.before( ctx, {
		projectFolder: false,
		plugin: true,
		files: {
			...mockedPluginAuthFiles,
			...modelFiles,
		}
	} ) );

	it( "is exposed", () => {
		ctx.hitchy.api.service.OdemSchema.should.be.ok();
	} );

	it( "is assuming plugin-auth exists", () => {
		ctx.hitchy.api.plugins.authentication.should.be.true();
	} );

	describe( "has method isAdmin() which", () => {
		it( "can be invoked", () => {
			( () => ctx.hitchy.api.service.OdemSchema.isAdmin() ).should.not.throw();
		} );

		it( "returns false when invoked without a user", () => {
			ctx.hitchy.api.service.OdemSchema.isAdmin().should.be.false();
		} );

		it( "returns false when invoked with a user assumed to have no role assigned", () => {
			ctx.hitchy.api.service.OdemSchema.isAdmin( { roles: [] } ).should.be.false();
		} );

		it( "returns false when invoked with a user assumed to have non-admin roles assigned", () => {
			ctx.hitchy.api.service.OdemSchema.isAdmin( { roles: [ "user", "maintainer", "owner" ] } ).should.be.false();
		} );

		it( "returns true when invoked with a user assumed to have role 'admin' assigned", () => {
			ctx.hitchy.api.service.OdemSchema.isAdmin( { roles: [{ name: "admin" }] } ).should.be.true();
		} );

		it( "returns true when invoked with a user assumed to have several roles including 'admin' assigned", () => {
			ctx.hitchy.api.service.OdemSchema.isAdmin( { roles: [ { name: "user" }, { name: "maintainer" }, { name: "owner" }, { name: "admin" } ] } ).should.be.true();
		} );
	} );

	describe( "has method mayBeExposed() which ", () => {
		it( "requires an optionally authenticated request and a model on invocation", () => {
			( () => ctx.hitchy.api.service.OdemSchema.mayBeExposed() ).should.throw();
			( () => ctx.hitchy.api.service.OdemSchema.mayBeExposed( {} ) ).should.throw();

			( () => ctx.hitchy.api.service.OdemSchema.mayBeExposed( {}, ctx.hitchy.api.model.Foo ) ).should.not.throw();
			( () => ctx.hitchy.api.service.OdemSchema.mayBeExposed( regularJohnRequest, ctx.hitchy.api.model.Foo ) ).should.not.throw();
		} );

		it( "returns true when invoked on an implicitly public model no matter the user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			anyUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBeExposed( req, ctx.hitchy.api.model.Foo ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true when invoked on an explicitly public model no matter the user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			anyUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBeExposed( req, ctx.hitchy.api.model.Bar ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false based on plugin-auth authorization when invoked on a protected model with or without an authenticated regular user in given request", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			const tests = [ {}, ...someRegularUserRequest ];

			tests.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBeExposed( req, ctx.hitchy.api.model.Baz ).should.be.false() );

			ctx.hitchy.api.data.testedAuthorizations.should.deepEqual( tests.map( ( { user } ) => ( {
				resource: "@hitchy.odem.model.Baz.expose",
				user: user?.name,
				roles: user?.roles?.map( r => r.name )
			} ) ) );
		} );

		it( "returns true when invoked on a protected model with an authenticated privileged user in given request even though authorizations would revoke", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			somePrivilegedUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBeExposed( req, ctx.hitchy.api.model.Baz ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true based on plugin-auth authorization when invoked on a protected model with or without an authenticated regular user in given request", () => {
			ctx.hitchy.api.data.mockedAuthorization = true;

			const tests = [ {}, ...someRegularUserRequest ];

			tests.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBeExposed( req, ctx.hitchy.api.model.Baz ).should.be.true() );

			ctx.hitchy.api.data.testedAuthorizations.should.deepEqual( tests.map( ( { user } ) => ( {
				resource: "@hitchy.odem.model.Baz.expose",
				user: user?.name,
				roles: user?.roles?.map( r => r.name )
			} ) ) );
		} );

		it( "returns false when invoked on a private model no matter the user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = true;

			someUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBeExposed( req, ctx.hitchy.api.model.Bam ).should.be.false() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );
	} );

	describe( "has method mayBePromoted() which ", () => {
		it( "requires an optionally authenticated request and a model on invocation", () => {
			( () => ctx.hitchy.api.service.OdemSchema.mayBePromoted() ).should.throw();
			( () => ctx.hitchy.api.service.OdemSchema.mayBePromoted( {} ) ).should.throw();

			( () => ctx.hitchy.api.service.OdemSchema.mayBePromoted( {}, ctx.hitchy.api.model.Foo ) ).should.not.throw();
			( () => ctx.hitchy.api.service.OdemSchema.mayBePromoted( regularJohnRequest, ctx.hitchy.api.model.Foo ) ).should.not.throw();
		} );

		it( "returns true when invoked on an implicitly public model no matter the user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			someUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBePromoted( req, ctx.hitchy.api.model.Foo ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true when invoked on an explicitly public model no matter the user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			someUserRequest.forEach( () => ctx.hitchy.api.service.OdemSchema.mayBePromoted( {}, ctx.hitchy.api.model.Bar ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false based on plugin-auth authorization when invoked on a protected model with or without an authenticated regular user in given request", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			const tests = [ {}, ...someRegularUserRequest ];

			tests.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBePromoted( req, ctx.hitchy.api.model.Baz ).should.be.false() );

			ctx.hitchy.api.data.testedAuthorizations.should.deepEqual( tests.map( ( { user } ) => ( {
				resource: "@hitchy.odem.model.Baz.promote",
				user: user?.name,
				roles: user?.roles?.map( r => r.name )
			} ) ) );
		} );

		it( "returns true when invoked on a protected model with an authenticated privileged user in given request even though authorizations would revoke", () => {
			ctx.hitchy.api.data.mockedAuthorization = false;

			somePrivilegedUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBePromoted( req, ctx.hitchy.api.model.Baz ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true based on plugin-auth authorization when invoked on a protected model with or without an authenticated regular user in given request", () => {
			ctx.hitchy.api.data.mockedAuthorization = true;

			const tests = [ {}, ...someRegularUserRequest ];

			tests.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBePromoted( req, ctx.hitchy.api.model.Baz ).should.be.true() );

			ctx.hitchy.api.data.testedAuthorizations.should.deepEqual( tests.map( ( { user } ) => ( {
				resource: "@hitchy.odem.model.Baz.promote",
				user: user?.name,
				roles: user?.roles?.map( r => r.name )
			} ) ) );
		} );

		it( "returns false when invoked on a private model no matter the user given", () => {
			ctx.hitchy.api.data.mockedAuthorization = true;

			anyUserRequest.forEach( req => ctx.hitchy.api.service.OdemSchema.mayBePromoted( req, ctx.hitchy.api.model.Bam ).should.be.false() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );
	} );

	describe( "has method mayAccessScope() which", () => {
		it( "can be invoked", () => {
			( () => ctx.hitchy.api.service.OdemSchema.mayAccessScope() ).should.not.throw();
		} );

		it( "returns false when invoked without any arguments", () => {
			ctx.hitchy.api.service.OdemSchema.mayAccessScope().should.be.false();
		} );

		it( "returns true for public scope no matter the resource or the given user", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			OdemSchema.mayAccessScope( "public" ).should.be.true();
			OdemSchema.mayAccessScope( "public", "some.resource" ).should.be.true();

			anyUser.forEach( user => OdemSchema.mayAccessScope( "public", "some.resource", user ).should.be.true() );
		} );

		it( "does not check authorization for public scope no matter the resource or the given user", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			OdemSchema.mayAccessScope( "public" );
			OdemSchema.mayAccessScope( "public", "some.resource" );

			anyUser.forEach( user => OdemSchema.mayAccessScope( "public", "some.resource", user ) );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false/true solely based on whether regular user is given when invoked without resource for the latter is required to check with plugin-auth", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			ctx.hitchy.api.data.mockedAuthorization = false;

			OdemSchema.mayAccessScope( "protected" ).should.be.false();
			someUser.forEach( user => OdemSchema.mayAccessScope( "protected", undefined, user ).should.be.true() );

			ctx.hitchy.api.data.mockedAuthorization = true;

			OdemSchema.mayAccessScope( "protected" ).should.be.false();
			someUser.forEach( user => OdemSchema.mayAccessScope( "protected", undefined, user ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false for lacking or regular user on protected scope if plugin-auth is revoking authorization", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			ctx.hitchy.api.data.mockedAuthorization = false;

			OdemSchema.mayAccessScope( "protected", "some.resource" ).should.be.false();

			[ undefined, ...someRegularUser ].forEach( user => OdemSchema.mayAccessScope( "protected", "some.resource", user ).should.be.false() );
		} );

		it( "returns true for protected scope if plugin-auth is granting authorization", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			ctx.hitchy.api.data.mockedAuthorization = true;

			OdemSchema.mayAccessScope( "protected", "some.resource" ).should.be.true();

			anyUser.forEach( user => OdemSchema.mayAccessScope( "protected", "some.resource", user ).should.be.true() );
		} );

		it( "returns true for protected scope and privileged user even though plugin-auth would revoke authorization which is not checked", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			ctx.hitchy.api.data.mockedAuthorization = false;

			somePrivilegedUser.forEach( user => OdemSchema.mayAccessScope( "protected", "some.resource", user ).should.be.true() );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "checks authorization for protected scope with plugin-auth as soon as a resource is given", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			OdemSchema.mayAccessScope( "protected", "some.resource" );
			someRegularUser.forEach( user => OdemSchema.mayAccessScope( "protected", "some.resource", user ) );

			ctx.hitchy.api.data.testedAuthorizations.should.deepEqual( [ undefined, ...someRegularUser ].map( user => ( {
				resource: "some.resource",
				user: user?.name,
				roles: user?.roles?.map( r => r.name )
			} ) ) );
		} );

		it( "does not check authorization for protected scope with plugin-auth as soon as a resource and a privileged user is given", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			somePrivilegedUser.forEach( user => OdemSchema.mayAccessScope( "protected", "some.resource", user ) );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false for private scope no matter the resource or the given user", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			OdemSchema.mayAccessScope( "private" ).should.be.false();
			OdemSchema.mayAccessScope( "private", "some.resource" ).should.be.false();
			anyUser.forEach( user => OdemSchema.mayAccessScope( "private", "some.resource", user ).should.be.false() );
		} );

		it( "does not check authorization for private scope no matter the resource or the given user", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			OdemSchema.mayAccessScope( "private" );
			OdemSchema.mayAccessScope( "private", "some.resource" );
			anyUser.forEach( user => OdemSchema.mayAccessScope( "private", "some.resource", user ) );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );
	} );

	describe( "has method mayAccessProperty() which", () => {
		it( "requires a model on invocation", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo } = ctx.hitchy.api.model;

			( () => OdemSchema.mayAccessProperty() ).should.throw();
			( () => OdemSchema.mayAccessProperty( undefined, undefined ) ).should.throw();

			combine3( [ "read", "write" ], [ undefined, "propName" ], anyUser ).forEach( ( [ mode, prop, user ] ) => {
				( () => OdemSchema.mayAccessProperty( prop, user, undefined ) ).should.throw();
				( () => OdemSchema.mayAccessProperty( prop, user, undefined, mode ) ).should.throw();

				( () => OdemSchema.mayAccessProperty( prop, user, Foo ) ).should.not.throw();
				( () => OdemSchema.mayAccessProperty( prop, user, Foo, mode ) ).should.not.throw();
			} );
		} );

		it( "returns false for testing nullishly named property of any model no matter the given user or access mode or the model's exposure", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = true;

			combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ "read", "write" ], anyUser ).forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( undefined, user, model ).should.be.false( `${user?.name} ${undefined}ing ${model.name}.undefined` );
				OdemSchema.mayAccessProperty( undefined, user, model, mode ).should.be.false( `${user?.name} ${mode}ing ${model.name}.undefined` );

				OdemSchema.mayAccessProperty( null, user, model ).should.be.false( `${user?.name} ${undefined}ing ${model.name}.null` );
				OdemSchema.mayAccessProperty( null, user, model, mode ).should.be.false( `${user?.name} ${mode}ing ${model.name}.null` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true for properties not defined in any of the tested models no matter the given user or access mode or the model's exposure", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = false;

			combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ "read", "write" ], anyUser ).forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "propName", user, model ).should.be.true( `${user?.name} ${undefined}ing ${model.name}.propName` );
				OdemSchema.mayAccessProperty( "propName", user, model, mode ).should.be.true( `${user?.name} ${mode}ing ${model.name}.propName` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true for implicitly public properties of any model no matter the given user or access mode or the model's exposure", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = false;

			combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ "read", "write" ], anyUser ).forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "a", user, model ).should.be.true( `${user?.name} ${undefined}ing ${model.name}.a` );
				OdemSchema.mayAccessProperty( "a", user, model, mode ).should.be.true( `${user?.name} ${mode}ing ${model.name}.a` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true for explicitly public properties of any model no matter the given user or access mode or the model's exposure", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = false;

			combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ "read", "write" ], anyUser ).forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "b", user, model ).should.be.true( `${user?.name} ${undefined}ing ${model.name}.b` );
				OdemSchema.mayAccessProperty( "b", user, model, mode ).should.be.true( `${user?.name} ${mode}ing ${model.name}.b` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false for protected properties of any model for guest users no matter the model's exposure if plugin-auth is revoking authorization", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;
			const allModels = [ models.Foo, models.Bar, models.Baz, models.Bam ];

			ctx.hitchy.api.data.mockedAuthorization = false;

			const tests = combine2( allModels, [ undefined, "read", "write" ] );

			tests.forEach( ( [ model, mode ] ) => {
				OdemSchema.mayAccessProperty( "c", undefined, model, mode ).should.be.false( `${undefined} ${mode}ing ${model.name}.c` );
			} );

			ctx.hitchy.api.data.testedAuthorizations.should.deepEqual( tests.map( ( [ model, mode ] ) => ( {
				resource: `@hitchy.odem.model.${model.name}.property.c.${mode || "read"}`,
				user: undefined,
				roles: undefined,
			} ) ) );
		} );

		it( "returns true for protected properties of any model for guest users no matter the model's exposure if plugin-auth is granting authorization", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;
			const allModels = [ models.Foo, models.Bar, models.Baz, models.Bam ];

			ctx.hitchy.api.data.mockedAuthorization = true;

			const tests = combine2( allModels, [ undefined, "read", "write" ] );

			tests.forEach( ( [ model, mode ] ) => {
				OdemSchema.mayAccessProperty( "c", undefined, model, mode ).should.be.true( `${undefined} ${mode}ing ${model.name}.c` );
			} );

			ctx.hitchy.api.data.testedAuthorizations.should.deepEqual( tests.map( ( [ model, mode ] ) => ( {
				resource: `@hitchy.odem.model.${model.name}.property.c.${mode || "read"}`,
				user: undefined,
				roles: undefined,
			} ) ) );
		} );

		it( "returns false for protected properties of any model for authenticated regular users no matter the given access mode or the model's exposure if plugin-auth is revoking authorization", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = false;

			const tests = combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ undefined, "read", "write" ], someRegularUser );

			tests.forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "c", user, model, mode ).should.be.false( `${user?.name} ${mode}ing ${model.name}.c` );
			} );

			ctx.hitchy.api.data.testedAuthorizations.should.deepEqual( tests.map( ( [ model, mode, user ] ) => ( {
				resource: `@hitchy.odem.model.${model.name}.property.c.${mode || "read"}`,
				user: user?.name,
				roles: user?.roles?.map( r => r.name ),
			} ) ) );
		} );

		it( "returns true for protected properties of any model for authenticated regular users no matter the given access mode or the model's exposure if plugin-auth is granting authorization", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = true;

			const tests = combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ undefined, "read", "write" ], someRegularUser );

			tests.forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "c", user, model, mode ).should.be.true( `${user?.name} ${mode}ing ${model.name}.c` );
			} );

			ctx.hitchy.api.data.testedAuthorizations.should.deepEqual( tests.map( ( [ model, mode, user ] ) => ( {
				resource: `@hitchy.odem.model.${model.name}.property.c.${mode || "read"}`,
				user: user?.name,
				roles: user?.roles?.map( r => r.name ),
			} ) ) );
		} );

		it( "considers custom authorization rule given as value of a property's `protected` declaration for guests and regular users", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			const tests = combine3( [ models.Boo, models.Far, models.Faz, models.Fam ], [ undefined, "read", "write" ], someRegularUser );

			tests.forEach( ( [ model, mode, user ] ) => OdemSchema.mayAccessProperty( "c", user, model, mode ) );

			ctx.hitchy.api.data.testedAuthorizations.should.deepEqual( tests.map( ( [ , , user ] ) => ( {
				resource: "custom.auth.rule",
				user: user?.name,
				roles: user?.roles?.map( r => r.name ),
			} ) ) );
		} );

		it( "returns true for protected properties of any model for authenticated privileged users no matter the given access mode, the model's exposure or if plugin-auth is revoking authorization", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = false;

			const tests = combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ undefined, "read", "write" ], somePrivilegedUser );

			tests.forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "c", user, model, mode ).should.be.true( `${user?.name} ${mode}ing ${model.name}.c` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns true for protected properties of any model for authenticated privileged users no matter the given access mode, the model's exposure or if plugin-auth is granting authorization", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = true;

			const tests = combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ undefined, "read", "write" ], somePrivilegedUser );

			tests.forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "c", user, model, mode ).should.be.true( `${user?.name} ${mode}ing ${model.name}.c` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "returns false for private properties of any model no matter the given user, access mode, the model's exposure or if plugin-auth is granting authorization", () => {
			const { models, service } = ctx.hitchy.api;
			const { OdemSchema } = service;

			ctx.hitchy.api.data.mockedAuthorization = true;

			combine3( [ models.Foo, models.Bar, models.Baz, models.Bam ], [ "read", "write" ], anyUser ).forEach( ( [ model, mode, user ] ) => {
				OdemSchema.mayAccessProperty( "d", user, model ).should.be.false( `${user?.name} ${undefined}ing ${model.name}.d` );
				OdemSchema.mayAccessProperty( "d", user, model, mode ).should.be.false( `${user?.name} ${mode}ing ${model.name}.d` );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );
	} );

	describe( "has method filterItem() which", () => {
		it( "requires an item to process and a request descriptor on invocation", () => {
			const { OdemSchema } = ctx.hitchy.api.service;

			( () => OdemSchema.filterItem() ).should.throw();
			( () => OdemSchema.filterItem( {} ) ).should.throw();
			( () => OdemSchema.filterItem( {}, {} ) ).should.not.throw();
		} );

		it( "requires a model on processing a non-empty item", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				( () => OdemSchema.filterItem( { foo: null }, request ) ).should.throw();
				( () => OdemSchema.filterItem( { foo: null }, request, undefined, "read" ) ).should.throw();
				( () => OdemSchema.filterItem( { foo: null }, request, undefined, "write" ) ).should.throw();

				( () => OdemSchema.filterItem( { foo: null }, request, model ) ).should.not.throw();
				( () => OdemSchema.filterItem( { foo: null }, request, model, "read" ) ).should.not.throw();
				( () => OdemSchema.filterItem( { foo: null }, request, model, "write" ) ).should.not.throw();
			} );
		} );

		it( "does not require a model on processing an empty item", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				( () => OdemSchema.filterItem( {}, request ) ).should.not.throw();
				( () => OdemSchema.filterItem( {}, request, undefined, "read" ) ).should.not.throw();
				( () => OdemSchema.filterItem( {}, request, undefined, "write" ) ).should.not.throw();

				( () => OdemSchema.filterItem( {}, request, model ) ).should.not.throw();
				( () => OdemSchema.filterItem( {}, request, model, "read" ) ).should.not.throw();
				( () => OdemSchema.filterItem( {}, request, model, "write" ) ).should.not.throw();
			} );
		} );

		it( "keeps properties not declared by given model in provided item no matter the model's own visibility", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { foo: null }, request, model ).should.have.property( "foo" );
				OdemSchema.filterItem( { z: null }, request, model ).should.have.property( "z" );
			} );
		} );

		it( "keeps implicitly public properties of given model in provided item no matter the model's own visibility", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { a: null }, request, model ).should.have.property( "a" );
			} );
		} );

		it( "keeps explicitly public properties of given model in provided item no matter the model's own visibility", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { b: null }, request, model ).should.have.property( "b" );
			} );
		} );

		it( "keeps or removes protected properties of given model in/from provided item for guests or regular users based on plugin-auth granting or revoking authorization", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			const tests = combine2( [ {}, ...someRegularUserRequest ], [ Foo, Bar, Baz, Bam ] );

			tests.forEach( ( [ request, model ] ) => {
				ctx.hitchy.api.data.mockedAuthorization = true;

				OdemSchema.filterItem( { c: null }, request, model ).should.have.property( "c" );

				ctx.hitchy.api.data.mockedAuthorization = false;

				OdemSchema.filterItem( { c: null }, request, model ).should.not.have.property( "c" );
			} );

			const expectedResult = [];

			tests.forEach( ( [ request, model ] ) => {
				expectedResult.push( {
					resource: `@hitchy.odem.model.${model.name}.property.c.read`,
					user: request.user?.name,
					roles: request.user?.roles?.map( r => r.name ),
				}, {
					resource: `@hitchy.odem.model.${model.name}.property.c.read`,
					user: request.user?.name,
					roles: request.user?.roles?.map( r => r.name ),
				} );
			} );

			ctx.hitchy.api.data.testedAuthorizations.should.deepEqual( expectedResult );
		} );

		it( "considers custom authorization rule given as value of property's `protected` declaration for guests or regular users", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Boo, Far, Faz, Fam } = ctx.hitchy.api.model;

			const tests = combine2( [ {}, ...someRegularUserRequest ], [ Boo, Far, Faz, Fam ] );

			tests.forEach( ( [ request, model ] ) => {
				ctx.hitchy.api.data.mockedAuthorization = true;

				OdemSchema.filterItem( { c: null }, request, model ).should.have.property( "c" );

				ctx.hitchy.api.data.mockedAuthorization = false;

				OdemSchema.filterItem( { c: null }, request, model ).should.not.have.property( "c" );
			} );

			const expectedResult = [];

			tests.forEach( ( [request,] ) => {
				expectedResult.push( {
					resource: "custom.auth.rule",
					user: request.user?.name,
					roles: request.user?.roles?.map( r => r.name ),
				}, {
					resource: "custom.auth.rule",
					user: request.user?.name,
					roles: request.user?.roles?.map( r => r.name ),
				} );
			} );

			ctx.hitchy.api.data.testedAuthorizations.should.deepEqual( expectedResult );
		} );

		it( "keeps protected properties of given model in/from provided item for privileged users no matter plugin-auth granting or revoking authorization", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			const tests = combine2( somePrivilegedUserRequest, [ Foo, Bar, Baz, Bam ] );

			tests.forEach( ( [ request, model ] ) => {
				ctx.hitchy.api.data.mockedAuthorization = true;

				OdemSchema.filterItem( { c: null }, request, model ).should.have.property( "c" );

				ctx.hitchy.api.data.mockedAuthorization = false;

				OdemSchema.filterItem( { c: null }, request, model ).should.have.property( "c" );
			} );

			( ctx.hitchy.api.data.testedAuthorizations == null ).should.be.true();
		} );

		it( "removes private properties of given model in provided item no matter the model's own visibility", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { d: null }, request, model ).should.not.have.property( "d" );
			} );
		} );

		it( "removes properties declared as `readonly` in `write` mode without regard to the user, authorization rules or the visibility of the property or the model", () => {
			const { OdemSchema } = ctx.hitchy.api.service;
			const { Foo, Bar, Baz, Bam } = ctx.hitchy.api.model;

			combine2( anyUserRequest, [ Foo, Bar, Baz, Bam ] ).forEach( ( [ request, model ] ) => {
				OdemSchema.filterItem( { e: null }, request, model ).should.have.property( "e" );
				OdemSchema.filterItem( { e: null }, request, model, "read" ).should.have.property( "e" );
				OdemSchema.filterItem( { e: null }, request, model, "write" ).should.not.have.property( "e" );
				OdemSchema.filterItem( { f: null }, request, model, "write" ).should.not.have.property( "f" );
				OdemSchema.filterItem( { g: null }, request, model, "write" ).should.not.have.property( "g" );
				OdemSchema.filterItem( { h: null }, request, model, "write" ).should.not.have.property( "h" );
			} );
		} );
	} );
} );
