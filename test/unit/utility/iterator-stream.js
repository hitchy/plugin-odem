import { Readable } from "node:stream";

import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";
import Should from "should";

const Test = await SDT( Core );

describe( "IteratorStream", function() {
	const ctx = {};
	let OdemUtilityIteratorStream;

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
	} ) );

	before( () => {
		( { OdemUtilityIteratorStream } = ctx.hitchy.api.service );
	} );

	it( "is exposed", function() {
		Should.exist( OdemUtilityIteratorStream );
	} );

	it( "requires iterator on construction", function() {
		( () => new OdemUtilityIteratorStream() ).should.throw();

		( () => new OdemUtilityIteratorStream( ""[Symbol.iterator]() ) ).should.not.throw();
		( () => new OdemUtilityIteratorStream( new Map()[Symbol.iterator]() ) ).should.not.throw();
	} );

	it( "inherits from `Readable`", function() {
		new OdemUtilityIteratorStream( ""[Symbol.iterator]() ).should.be.instanceOf( Readable );
	} );

	it( "provides iterated entries one by one", function() {
		return new Promise( resolve => {
			const stream = new OdemUtilityIteratorStream( "Hello World!"[Symbol.iterator]() );

			const exposed = [];

			stream.on( "end", () => {
				exposed.should.have.length( 12 );
				exposed.join( "" ).should.be.equal( "Hello World!" );
				resolve();
			} );

			stream.on( "data", chunk => {
				exposed.push( chunk );
			} );
		} );
	} );

	it( "accepts custom feeder for processing iterated values before pushing into stream", function() {
		return new Promise( resolve => {
			const stream = new OdemUtilityIteratorStream( new Map( [
				[ "name", "John" ],
				[ "surname", "Doe" ],
				[ "age", 42 ],
			] )[Symbol.iterator](), {
				feeder: function() {
					const item = this.iterator.next();
					if ( item.done ) {
						this.push( null );
					} else {
						this.push( `${item.value[0]}: ${item.value[1]}` );
					}
				},
			} );

			const exposed = [];

			stream.on( "end", () => {
				exposed.should.have.length( 3 );
				exposed[0].should.be.equal( "name: John" );
				exposed[1].should.be.equal( "surname: Doe" );
				exposed[2].should.be.equal( "age: 42" );
				resolve();
			} );

			stream.on( "data", chunk => {
				exposed.push( chunk );
			} );
		} );
	} );
} );
