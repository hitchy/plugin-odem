import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";
import Should from "should";

const Test = await SDT( Core );

describe( "Utility API for processing functions", function() {
	const ctx = {};
	let OdemUtilityFunction;

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
	} ) );

	before( () => {
		( { OdemUtilityFunction } = ctx.hitchy.api.service );
	} );

	it( "is available", function() {
		Should.exist( OdemUtilityFunction );

		OdemUtilityFunction.should.have.property( "extractBody" ).which.is.a.Function().and.has.length( 1 );
	} );

	describe( "exports extractBody() which", function() {
		it( "requires provision of function", function() {
			( () => OdemUtilityFunction.extractBody() ).should.throw();
			( () => OdemUtilityFunction.extractBody( null ) ).should.throw();
			( () => OdemUtilityFunction.extractBody( undefined ) ).should.throw();
			( () => OdemUtilityFunction.extractBody( false ) ).should.throw();
			( () => OdemUtilityFunction.extractBody( true ) ).should.throw();
			( () => OdemUtilityFunction.extractBody( 0 ) ).should.throw();
			( () => OdemUtilityFunction.extractBody( 1.5 ) ).should.throw();
			( () => OdemUtilityFunction.extractBody( -3000000 ) ).should.throw();
			( () => OdemUtilityFunction.extractBody( "" ) ).should.throw();
			( () => OdemUtilityFunction.extractBody( "test" ) ).should.throw();
			( () => OdemUtilityFunction.extractBody( [] ) ).should.throw();
			( () => OdemUtilityFunction.extractBody( [() => {}] ) ).should.throw();
			( () => OdemUtilityFunction.extractBody( {} ) ).should.throw();
			( () => OdemUtilityFunction.extractBody( { function: () => {} } ) ).should.throw();

			( () => OdemUtilityFunction.extractBody( function() {} ) ).should.not.throw();

			( () => OdemUtilityFunction.extractBody( () => {} ) ).should.not.throw();
		} );

		it( "returns information on provided function", function() {
			OdemUtilityFunction.extractBody( function() {} ).should.be.Object().which.has.ownProperty( "args" );
			OdemUtilityFunction.extractBody( function() {} ).should.be.Object().which.has.ownProperty( "body" );

			OdemUtilityFunction.extractBody( () => {} ).should.be.Object().which.has.ownProperty( "body" );
			OdemUtilityFunction.extractBody( () => {} ).should.be.Object().which.has.ownProperty( "args" );
		} );

		it( "returns sorted list of names of all arguments of provided function", function() {
			OdemUtilityFunction.extractBody( function() {} ).args.should.be.an.Array().which.has.length( 0 );
			OdemUtilityFunction.extractBody( function( a ) {} ).args // eslint-disable-line no-unused-vars
				.should.be.an.Array().which.is.containEql( "a" ).and.has.length( 1 );
			OdemUtilityFunction.extractBody( function( first, second ) {} ).args // eslint-disable-line no-unused-vars
				.should.be.an.Array().which.is.eql( [ "first", "second" ] ).and.has.length( 2 );

			OdemUtilityFunction.extractBody( () => {} ).args.should.be.an.Array().which.has.length( 0 );
			OdemUtilityFunction.extractBody( a => {} ).args // eslint-disable-line no-unused-vars
				.should.be.an.Array().which.is.containEql( "a" ).and.has.length( 1 );
			OdemUtilityFunction.extractBody( ( first, second ) => {} ).args // eslint-disable-line no-unused-vars
				.should.be.an.Array().which.is.eql( [ "first", "second" ] ).and.has.length( 2 );

			OdemUtilityFunction.extractBody( () => false ).args.should.be.an.Array().which.has.length( 0 );
			OdemUtilityFunction.extractBody( a => a ).args
				.should.be.an.Array().which.is.containEql( "a" ).and.has.length( 1 );
			OdemUtilityFunction.extractBody( ( first, second ) => 1 + 2 ).args // eslint-disable-line no-unused-vars
				.should.be.an.Array().which.is.eql( [ "first", "second" ] ).and.has.length( 2 );
		} );

		it( "returns body of provided function", function() {
			OdemUtilityFunction.extractBody( function() {} ).body.should.be.a.String().which.is.equal( "" );
			OdemUtilityFunction.extractBody( function( a ) { return a + a; } ).body.should.be.a.String().which.is.equal( "return a + a;" );
			OdemUtilityFunction.extractBody( function( a ) {
				return a + a;
			} ).body.should.be.a.String().which.is.equal( "return a + a;" );
			OdemUtilityFunction.extractBody( function( a ) {
				const b = a * 2;
				return b + a;
			} ).body.should.be.a.String().which.is.equal( `const b = a * 2;
				return b + a;` );

			OdemUtilityFunction.extractBody( () => {} ).body.should.be.a.String().which.is.equal( "" );
			OdemUtilityFunction.extractBody( a => { return a + a; } ).body.should.be.a.String().which.is.equal( "return a + a;" );
			OdemUtilityFunction.extractBody( a => {
				return a + a;
			} ).body.should.be.a.String().which.is.equal( "return a + a;" );

			OdemUtilityFunction.extractBody( () => false ).body.should.be.a.String().which.is.equal( "return false;" );
			OdemUtilityFunction.extractBody( a => a ).body.should.be.a.String().which.is.equal( "return a;" );
			OdemUtilityFunction.extractBody( a => a + a ).body.should.be.a.String().which.is.equal( "return a + a;" );
		} );
	} );
} );
