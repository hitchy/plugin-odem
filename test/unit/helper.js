/**
 * Retrieves list of values covering all basic kinds of data supported in
 * Javascript.
 *
 * @returns {*[]} list of values
 */
function allTypesOfData() {
	return [
		undefined,
		null,
		false,
		true,
		{},
		{ property: "value" },
		[ "item", "item" ],
		"",
		"hello world",
		0,
		NaN,
		-Infinity,
		Number( Infinity ),
		-10.5,
		+10.5,
		() => {},
		() => "",
		function() { return ""; },
	];
}

export default Object.seal( {
	allTypesOfData,
	allTypesOfDataButNullLike() {
		return allTypesOfData().filter( i => i != null );
	},
	allComparisonOperations() {
		return [
			"eq",
			"neq",
			"noteq",
			"not",
			"null",
			"notnull",
			"between",
			"lt",
			"lte",
			"gt",
			"gte",
		];
	},
} );
