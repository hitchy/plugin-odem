import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";
import "should";

const Test = await SDT( Core );

describe( "Regression: Model#find() coerces values in a query for matching indexed properties", () => {
	const ctx = {};
	let Model;
	let Base;
	let Inherited;
	let item1;

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
	} ) );

	before( () => {
		( { Model } = ctx.hitchy.api.service );
	} );

	before( async() => {
		Base = Model.define( "Base", {
			props: {
				aString: { index: "eq" },
				aNumber: { type: "number", index: "eq" },
				anInteger: { type: "integer", index: "eq" },
				aBoolean: { type: "boolean", index: "eq" },
				aDate: { type: "date", index: "eq" },
				aUUID: { type: "uuid", index: "eq" },
			},
		} );

		Inherited = Model.define( "Inherited", {
			props: {
				anotherString: { index: "eq" },
				anotherNumber: { type: "number", index: "eq" },
				anotherInteger: { type: "integer", index: "eq" },
				anotherBoolean: { type: "boolean", index: "eq" },
				anotherDate: { type: "date", index: "eq" },
				anotherUUID: { type: "uuid", index: "eq" },
			},
		}, Base );

		item1 = new Inherited();
		item1.aString = "a 1";
		item1.aNumber = 1.23;
		item1.anInteger = 1;
		item1.aBoolean = true;
		item1.aDate = new Date( "2022-08-24T13:24:56" );
		item1.aUUID = "12345678-1234-1234-1234-1234567890ab";

		item1.anotherString = "another 1";
		item1.anotherNumber = 9.87;
		item1.anotherInteger = 9;
		item1.anotherBoolean = false;
		item1.anotherDate = new Date( "2020-01-04T12:13:14" );
		item1.anotherUUID = "09876543-0987-0987-0987-098765432109";
		await item1.save();
	} );

	it( "matches no matter type of value in an equality-testing query", async() => {
		let matches = await Inherited.find( { eq: { aString: "a 1" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { aString: { toString: () => "a 1" } } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { aNumber: 1.23 } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { aNumber: "1.23" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anInteger: 1 } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anInteger: "1" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { aBoolean: true } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { aBoolean: "true" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { aBoolean: "Y" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { aBoolean: "on" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { aDate: new Date( "2022-08-24T13:24:56" ) } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { aDate: "2022-08-24T13:24:56" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { aDate: "2022-08-24 13:24:56" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { aUUID: "12345678-1234-1234-1234-1234567890ab" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { aUUID: { toString: () => "12345678-1234-1234-1234-1234567890ab" } } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherString: "another 1" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherString: { toString: () => "another 1" } } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherNumber: 9.87 } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherNumber: "9.87" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherInteger: 9 } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherInteger: "9" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherBoolean: false } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherBoolean: "false" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherBoolean: "N" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherBoolean: "" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherBoolean: "off" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherDate: new Date( "2020-01-04T12:13:14" ) } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherDate: "2020-01-04T12:13:14" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherDate: "2020-01-04 12:13:14" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherUUID: "09876543-0987-0987-0987-098765432109" } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { eq: { anotherUUID: { toString: () => "09876543-0987-0987-0987-098765432109" } } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );
	} );

	it( "matches no matter type of value in a range-testing query", async() => {
		let matches = await Inherited.find( { between: { aString: [ "a 0", "a 2" ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { aString: [ { toString: () => "a 1" }, { toString: () => "a 2" } ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { aNumber: [ 1.22, 1.24 ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { aNumber: [ "1.22", "1.24" ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anInteger: [ 0, 1 ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anInteger: [ "0", "1" ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { aDate: [ new Date( "2022-08-24T13:24:55" ), new Date( "2022-08-24T13:24:57" ) ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { aDate: [ "2022-08-24T13:24:55", "2022-08-24T13:24:57" ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { aDate: [ "2022-08-24 13:24:55", "2022-08-24 13:24:57" ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { aUUID: [ "12345678-1234-1234-1234-1234567890aa", "12345678-1234-1234-1234-1234567890ac" ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { aUUID: [ { toString: () => "12345678-1234-1234-1234-1234567890aa" }, { toString: () => "12345678-1234-1234-1234-1234567890ac" } ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anotherString: [ "another 0", "another 2" ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anotherString: [ { toString: () => "another 0" }, { toString: () => "another 2" } ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anotherNumber: [ 9.86, 9.88 ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anotherNumber: [ "9.86", "9.88" ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anotherInteger: [ 8, 10 ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anotherInteger: [ "8", "10" ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anotherDate: [ new Date( "2020-01-04T12:13:13" ), new Date( "2020-01-04T12:13:15" ) ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anotherDate: [ "2020-01-04T12:13:13", "2020-01-04T12:13:15" ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anotherDate: [ "2020-01-04 12:13:13", "2020-01-04 12:13:15" ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anotherUUID: [ "09876543-0987-0987-0987-098765432108", "09876543-0987-0987-0987-09876543210a" ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );

		matches = await Inherited.find( { between: { anotherUUID: [ { toString: () => "09876543-0987-0987-0987-098765432108" }, { toString: () => "09876543-0987-0987-0987-09876543210a" } ] } } );
		matches.should.be.Array().which.has.length( 1 );
		matches[0].uuid.should.be.equal( item1.uuid );
	} );
} );
