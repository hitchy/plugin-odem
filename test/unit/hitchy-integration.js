import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";
import "should";

const Test = await SDT( Core );

describe( "Integration with hitchy", () => {
	const ctx = {};
	let OdemConverter, OdemAdapterMemory, Model;

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
	} ) );

	before( () => {
		( { Model, OdemConverter, OdemAdapterMemory } = ctx.hitchy.api.service );
	} );

	describe( "relies on exposed function OdemConverter.processModelDefinitions() which", () => {
		it( "is a function", () => {
			OdemConverter.processModelDefinitions.should.be.Function();
		} );

		it( "takes three arguments", () => {
			OdemConverter.processModelDefinitions.should.have.length( 2 );
		} );

		it( "requires three arguments", () => {
			( () => OdemConverter.processModelDefinitions() ).should.throw();
			( () => OdemConverter.processModelDefinitions( {} ) ).should.throw();
			( () => OdemConverter.processModelDefinitions( {}, new OdemAdapterMemory() ) ).should.not.throw();
		} );

		it( "returns provided set of models", () => {
			const models = {};

			OdemConverter.processModelDefinitions( models, new OdemAdapterMemory() ).should.be.equal( models );
		} );

		it( "replaces existing models in provided set of model definitions with either model's implementation", () => {
			const models = {
				SomeModel: { props: { a: {} } },
			};

			const defined = OdemConverter.processModelDefinitions( Object.assign( {}, models ), new OdemAdapterMemory() );

			defined.should.be.Object().which.has.property( "SomeModel" ).which.has.property( "prototype" ).which.is.instanceof( Model );
		} );
	} );

	describe( "processes set of model definitions that", () => {
		it( "is empty", () => {
			( () => OdemConverter.processModelDefinitions( {}, new OdemAdapterMemory() ) ).should.not.throw();
		} );

		it( "defines single model `sole` with single string property named `sole`", () => {
			const models = OdemConverter.processModelDefinitions( {
				sole: { props: { sole: {} } },
			}, new OdemAdapterMemory() );

			models.should.have.property( "sole" );
			models.sole.should.have.property( "prototype" ).which.is.instanceof( Model );

			const sole = new models.sole(); // eslint-disable-line new-cap

			sole.sole = "test";
			sole.$properties.should.have.property( "sole" ).which.is.equal( "test" );
		} );
	} );
} );
