import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

const Test = await SDT( Core );

describe( "When integrating with Hitchy a controller", () => {
	const ctx = {};

	before( Test.before( ctx, {
		pluginsFolder: "../../..",
		projectFolder: "../../project",
		options: {
			// debug: true,
		},
	} ) );

	after( Test.after( ctx ) );


	it( "can access implicitly discovered models", () => {
		return ctx.get( "/models" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.an.Array().which.is.deepEqual( [ "BasicData", "CmfpRegular" ] );
			} );
	} );

	it( "can access abstract model as a service component", () => {
		return ctx.get( "/modelClass" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.true();
			} );
	} );

	it( "can access instance of implicitly discovered model which is having access on Hitchy's API", () => {
		return ctx.get( "/modelImplicitInstance" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.Object().which.has.property( "someString" ).which.is.equal( "BasicData,CmfpRegular" );
			} );
	} );

	it( "can access instance of implicitly discovered model provided in compliance with CMFP which is having access on Hitchy's API", () => {
		return ctx.get( "/modelImplicitCmfpInstance" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.Object().which.has.property( "someString" ).which.is.equal( "BasicData,CmfpRegular" );
			} );
	} );

	it( "can define new model using Model service exposed by @hitchy/plugin-odem and access instance of it which is having access on Hitchy's API", () => {
		return ctx.get( "/modelExplicitInstance" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.Object().which.has.property( "someString" ).which.is.equal( "BasicData,CmfpRegular" );
			} );
	} );

	it( "can define new model using Model exported by @hitchy/plugin-odem and access instance of it which is having access on Hitchy's API", () => {
		return ctx.get( "/modelExplicitInstance" )
			.then( res => {
				res.should.have.status( 200 ).and.be.json();
				res.data.should.be.Object().which.has.property( "someString" ).which.is.equal( "BasicData,CmfpRegular" );
			} );
	} );
} );
