/** */
export default function() {
	const api = this;
	const logDebug = api.log( "hitchy:odem:debug" );
	const logError = api.log( "hitchy:odem:error" );

	return {
		initialize() {
			const { config, models, services } = api;

			// choose configured default adapter for storing model instances
			let adapter = ( config.database || {} ).default;
			if ( adapter ) {
				if ( !( adapter instanceof services.OdemAdapter ) ) {
					logError( "invalid adapter:", adapter );

					return Promise.reject( new Error( "invalid adapter rejected" ) );
				}

				logDebug( "discovering model definitions to be managed via %s with %j", adapter.constructor.name, adapter.options || adapter.config || adapter.id );
			} else {
				adapter = new services.OdemAdapterMemory();

				logDebug( "discovering model definitions to be managed via memory adapter by default" );
			}

			return ( adapter.onPrepared || Promise.resolve() )
				.then( () => services.OdemConverter.processModelDefinitions( models, adapter ) );
		}
	};
}
