/* eslint no-unused-vars: ["error", { "args": "none" }] */

import { Readable } from "node:stream";
import EventEmitter from "node:events";

/**
 * Provides ID of next transaction to be created.
 *
 * @type {number}
 */
let nextTransactionId = 1;

/**
 * Lists pending transactions with their ID and some promise resolved once
 * either transaction is done.
 *
 * @type {{id: number, done: Promise}[]}
 */
const transactionsQueue = [];

/** */
export default function() {
	const api = this;

	/**
	 * @name OdemAdapter
	 * @alias this.services.OdemAdapter
	 */
	class OdemAdapter extends EventEmitter {
		/** @ignore */
		constructor() {
			super();

			this.isInATransaction = false;

			const numModels = Object.keys( api.models ).length;

			this.setMaxListeners( Math.max( 10, numModels * 2 ) );

			/**
			 * Exposes capabilities of this adapter instance. This is used by
			 * Odem to decide when to use available features and when to use
			 * polyfills for a missing feature.
			 *
			 * @type {{read: boolean, remove: boolean, stream: boolean, test: boolean, transact: boolean, watch: boolean, write: boolean}}
			 */
			this.can = {
				test: false,
				read: false,
				write: false,
				remove: false,
				stream: false,
				watch: false,
				transact: false,
			};
		}

		/**
		 * Drops all data available via current adapter.
		 *
		 * @note This method is primarily meant for use while testing. It might be
		 *       useful in similar situations as well, like uninstalling some app.
		 *
		 * @returns {Promise} promises purging all data available via current adapter
		 * @abstract
		 */
		purge() {
			return Promise.reject( new Error( "invalid use of abstract base adapter" ) );
		}

		/**
		 * Puts provided data in storage assigning new unique key.
		 *
		 * @param {string} keyTemplate template of key containing %u to be replaced with assigned UUID
		 * @param {object} data record to be written
		 * @returns {Promise.<string>} promises unique key of new record
		 * @abstract
		 */
		create( keyTemplate, data ) {
			return Promise.reject( new Error( "invalid use of abstract base adapter" ) );
		}

		/**
		 * Checks if provided key exists.
		 *
		 * @param {string} key unique key of record to test
		 * @returns {Promise.<boolean>} promises information if key exists or not
		 * @abstract
		 */
		has( key ) {
			return Promise.reject( new Error( "invalid use of abstract base adapter" ) );
		}

		/**
		 * Reads data selected by provided key.
		 *
		 * @param {string} key unique key of record to read
		 * @param {object} ifMissing data object to return if selected record is missing
		 * @returns {Promise.<object>} promises read data
		 * @abstract
		 */
		read( key, { ifMissing = null } = {} ) {
			return Promise.reject( new Error( "invalid use of abstract base adapter" ) );
		}

		/**
		 * Writes provided data to given key.
		 *
		 * @note To support REST API in @hitchy/plugin-odem-rest this method must be
		 *       capable of writing to record that did not exist before, thus
		 *       creating new record with provided key.
		 *
		 * @param {string} key unique key of record to be written
		 * @param {object} data record to be written
		 * @returns {Promise.<object>} promises provided data
		 * @abstract
		 */
		write( key, data ) {
			return Promise.reject( new Error( "invalid use of abstract base adapter" ) );
		}

		/**
		 * Removes data addressed by given key.
		 *
		 * @note Removing some parent key includes removing all subordinated keys.
		 *
		 * @param {string} key unique key of record to be removed
		 * @returns {Promise.<key>} promises key of removed data
		 * @abstract
		 */
		remove( key ) {
			return Promise.reject( new Error( "invalid use of abstract base adapter" ) );
		}

		/**
		 * Waits for selected transaction to be actively running one.
		 *
		 * - If provided transaction ID is falsy, processing is postponed until
		 *   all transactions in queue have been executed.
		 * - If provided transaction ID is truthy and is matching current
		 *   transaction, returned promise is resolved instantly.
		 * - If provided transaction ID is truthy and is not matching current
		 *   transaction, but some outstanding transaction, returned promise is
		 *   resolved as soon as that transaction has become active one.
		 * - If provided transaction ID is truthy and does not match any of the
		 *   active or pending transactions, returned promise is rejected instantly.
		 *
		 * @param {number|null|undefined} id ID of transaction to wait for
		 * @returns {Promise<void>} promise resolved when selected transaction is active
		 */
		async waitForTransaction( id ) { // eslint-disable-line consistent-return
			const queueSize = transactionsQueue.length;

			if ( id ) {
				// caller is bound to some pending transaction
				// -> look it up in queue and deliver promise for its
				//    predecessor being done
				for ( let i = 0; i < queueSize; i++ ) {
					if ( id === transactionsQueue[i].id ) {
						return i > 0 ? transactionsQueue[i - 1].done : undefined;
					}
				}

				throw new Error( "no such transaction to wait for" );
			}

			// caller is not bound to some transaction, but has to wait
			// until all pending transactions have been done
			if ( queueSize ) {
				await new Promise( resolve => {
					const waitForDrain = () => {
						if ( transactionsQueue.length > 0 ) {
							// keep waiting if more transactions have been enqueued
							transactionsQueue[transactionsQueue.length - 1].done
								.finally( waitForDrain );
						} else {
							resolve();
						}
					};

					waitForDrain();
				} );
			}
		}

		/**
		 * Wraps provided handler in a transaction ensuring that either all
		 * queries and updates of that handler get applied or none of them.
		 *
		 * @param {(adapter:OdemAdapter)=>(void|Promise<void>)} handler callback invoked with transaction-specific adapter to use instead of current one
		 * @param {Object} options transaction customizations
		 * @returns {Promise} promises success of transaction
		 */
		async transaction( handler, options = {} ) {
			if ( typeof handler !== "function" ) {
				throw new TypeError( "callback must be provided to actually perform the transaction" );
			}

			if ( this.isInATransaction ) {
				throw new Error( "must not start a nested transaction" );
			}

			const predecessor = transactionsQueue.length > 0 ? transactionsQueue[transactionsQueue.length - 1].done : Promise.resolve();
			const me = {
				id: nextTransactionId++,
			};

			transactionsQueue.push( me );

			me.done = predecessor
				.catch( () => {} ) // eslint-disable-line no-empty-function
				.then( async() => {
					if ( transactionsQueue[0].id !== me.id ) {
						throw new Error( "unexpected mismatch of transaction IDs" );
					}

					await this._handleTransaction( me.id, handler, options );
				} )
				.finally( () => {
					transactionsQueue.shift();
				} );

			await me.done;
		}

		/**
		 * Invokes provided transaction handler as part of local transaction
		 * support commonly implemented by abstract OdemAdapter class.
		 *
		 * @param {number} transactionId unique ID of transaction
		 * @param {(adapter:OdemAdapter)=>(void|Promise<void>)} handler callback invoked to perform the transaction, "adapter" must be bound to transaction
		 * @param {Object<string,any>} options transaction customizations
		 * @protected
		 */
		_handleTransaction( transactionId, handler, options ) {
			throw new Error( "transactions are not supported by current adapter" );
		}

		/**
		 * Retrieves stream of available keys.
		 *
		 * @param {string} prefix stream keys with given prefix, only
		 * @param {int} maxDepth skip keys beyond this depth (relative to `prefix`)
		 * @param {string} separator consider this character separating segments of key selecting different depth, set null to disable depth processing
		 * @param {"skip"|"fail"} invalidPolicy selects policy for handling invalid records, e.g. failing to contain valid JSON
		 * @returns {Readable} stream of keys
		 * @deprecated use stream() with target=="key" instead
		 */
		keyStream( {
			prefix = "",
			maxDepth = Infinity,
			separator = "/",
			invalidPolicy = "skip",
		} = {} ) {
			return this.stream( { prefix, maxDepth, separator, invalidPolicy, target: "key" } );
		}

		/**
		 * Retrieves stream of available keys, values or key-value pairs.
		 *
		 * @param {string} prefix stream keys with given prefix, only
		 * @param {int} maxDepth skip keys beyond this depth (relative to `prefix`)
		 * @param {string} separator consider this character separating segments of key selecting different depth, set null to disable depth processing
		 * @param {"entry"|"key"|"value"} target selects data to be provided per key-value pair
		 * @param {"skip"|"fail"} invalidPolicy selects policy for handling invalid records, e.g. failing to contain valid JSON
		 * @returns {Readable} stream of keys, values or key-value pairs
		 * @abstract
		 */
		stream( {
			prefix = "",
			maxDepth = Infinity,
			separator = "/",
			target = "entry",
			invalidPolicy = "skip",
		} = {} ) {
			return new Readable( {
				read() {
					this.emit( "error", new Error( "invalid use of abstract base adapter" ) );
				}
			} );
		}

		/**
		 * Maps some key to relative pathname to use on addressing related record in
		 * backend.
		 *
		 * @note This is available e.g. for splitting longer IDs into several path
		 *       segments e.g. for limiting number of possible files per folder in a
		 *       file-based backend.
		 *
		 * @param {string} key key to be mapped
		 * @returns {string} related path name to use for actually addressing entity in backend
		 */
		static keyToPath( key ) {
			return key;
		}

		/**
		 * Maps some relative pathname to use on addressing related record in
		 * backend into related key.
		 *
		 * @note This is available to reverse process of OdemAdapter.keyToPath().
		 *
		 * @param {string} path path name addressing some entity in backend
		 * @returns {string} unique key for addressing selected entity
		 */
		static pathToKey( path ) {
			return path;
		}

		/**
		 * Indicates if adapter is capable of storing Buffer as property value.
		 *
		 * This information can be used by property type handlers on serializing
		 * data for storing via this adapter.
		 *
		 * @returns {boolean} true if adapter can save binary buffers as property value
		 */
		static get supportsBinary() { return false; }

		/**
		 * Indicates if adapter benefits from consuming models caching fetched
		 * data or not.
		 *
		 * This information is used by the model code to decide whether caching
		 * should be enabled by default or not.
		 *
		 * @returns {boolean} true if adapter is slow enough so that caching in model would be beneficial
		 */
		static get benefitsFromCaching() { return true; }
	}

	return OdemAdapter;
};
