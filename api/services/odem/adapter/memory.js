import { randomUUID } from "node:crypto";
import { Readable } from "node:stream";

const DefaultConfig = {
	dataSource: "memory:///",
};

/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	const logDebug = api.log( "hitchy:odem:memory:debug" );
	const logError = api.log( "hitchy:odem:memory:error" );

	/**
	 * Implements backend managing all data in files of local file system.
	 *
	 * @name OdemAdapterMemory
	 * @alias this.services.OdemAdapterMemory
	 * @extends OdemAdapter
	 * @property {object} config options customizing current adapter
	 * @property {string} dataSource path name of folder containing all data files
	 */
	class OdemAdapterMemory extends Services.OdemAdapter {
		/**
		 * @param {object} config configuration of adapter
		 */
		constructor( config = null ) {
			super();

			const _config = Object.assign( {}, DefaultConfig, config );

			Object.defineProperties( this, {
				/**
				 * Exposes configuration of current adapter.
				 *
				 * @name OdemAdapterMemory#config
				 * @property {object}
				 * @readonly
				 */
				config: { value: _config },

				/**
				 * Exposes data source managed by current adapter.
				 *
				 * @name OdemAdapterMemory#dataSource
				 * @property {Map}
				 * @readonly
				 */
				dataSource: { value: _config.dataSource instanceof Map ? _config.dataSource : new Map() },

				/**
				 * Indicates if current adapter is basically a copy of some
				 * original adapter which has been bound to a transaction.
				 *
				 * @prop isInATransaction
				 * @name OdemAdapterMemory#isInATransaction
				 * @type {boolean}
				 */
				isInATransaction: { value: _config._transactionId > 0 },

				/**
				 * Exposes ID of transaction this copy of original memory
				 * adapter has been bound to.
				 *
				 * @private
				 */
				_transactionId: { value: _config._transactionId || 0 },

				/**
				 * Tracks changes to be persisted once the transaction this
				 * adapter is bound to has completed.
				 */
				_journal: { value: _config._transactionId ? new Map() : undefined },

				/**
				 * Exposes capabilities of this adapter instance. This is used by
				 * Odem to decide when to use available features and when to use
				 * polyfills for a missing feature.
				 *
				 * @type {{read: boolean, remove: boolean, stream: boolean, test: boolean, transact: boolean, watch: boolean, write: boolean}}
				 */
				can: {
					value: {
						test: true,
						read: true,
						write: true,
						remove: true,
						stream: true,
						watch: true,
						transact: !( _config._transactionId > 0 ),
					},
					enumerable: true
				},
			} );
		}

		/** @inheritDoc */
		async purge() {
			if ( this._transactionId ) {
				throw new Error( "model may not be purged while in a transaction" );
			}

			await this.waitForTransaction();

			for ( const key of this.dataSource.keys() ) {
				this.emit( "delete", key );
			}

			this.dataSource.clear();
		}

		/** @inheritDoc */
		async create( keyTemplate, data ) {
			await this.waitForTransaction( this._transactionId );

			const { _journal, dataSource } = this;
			let uuid, key;

			while ( true ) {
				uuid = randomUUID();
				key = keyTemplate.replace( /%u/g, uuid );

				if ( !_journal?.has( key ) && !dataSource.has( key ) ) {
					// UUID has not been assigned to some record before
					break;
				}
			}

			if ( _journal ) {
				_journal.set( key, data );
			} else {
				dataSource.set( key, data );

				this.emit( "change", key, data, undefined );
			}

			return key;
		}

		/** @inheritDoc */
		async has( key ) {
			if ( this._journal?.has( key ) ) {
				return Boolean( this._journal.get( key ) );
			}

			return await this.dataSource.has( key );
		}

		/** @inheritDoc */
		async read( key, { ifMissing = null } = {} ) {
			if ( this._transactionId ) {
				await this.waitForTransaction( this._transactionId );
			}

			const { dataSource, _journal } = this;

			if ( _journal && _journal.has( key ) ) {
				const data = _journal.get( key );

				if ( data ) {
					return data;
				}
			} else if ( dataSource.has( key ) ) {
				return dataSource.get( key );
			}

			if ( ifMissing ) {
				return ifMissing;
			}

			throw Object.assign( new Error( `no such record @${key}` ), { code: "ENOENT" } );
		}

		/** @inheritDoc */
		async write( key, data ) {
			await this.waitForTransaction( this._transactionId );

			if ( this._journal ) {
				this._journal.set( key, data );
			} else {
				this._write( key, data );
			}

			return data;
		}

		/**
		 * Internally writes entry with given key to data source without any
		 * safeguards.
		 *
		 * @param {string} key key of record to remove
		 * @param {Object} data record to write
		 * @private
		 */
		_write( key, data ) {
			const hasOld = this.dataSource.has( key );
			const old = this.dataSource.get( key );

			this.dataSource.set( key, data );

			this.emit( "change", key, data, hasOld ? { key, value: old } : undefined );
		}

		/** @inheritDoc */
		async remove( key ) {
			await this.waitForTransaction( this._transactionId );

			if ( this._journal ) {
				this._journal.set( key, false );
			} else {
				this._remove( key );
			}

			return key;
		}

		/**
		 * Internally removes entry with given key from data source without any
		 * safeguards.
		 *
		 * @param {string} key key of record to remove
		 * @private
		 */
		_remove( key ) {
			this.dataSource.delete( key );

			this.emit( "delete", key );
		}

		/** @inheritDoc */
		async _handleTransaction( id, handler, options = {} ) { // eslint-disable-line no-unused-vars
			const boundAdapter = new this.constructor( {
				...this.config,
				dataSource: this.dataSource,
				_transactionId: id,
			} );

			await handler( boundAdapter );

			for ( const [ key, data ] of boundAdapter._journal.entries() ) {
				if ( data ) {
					this._write( key, data );
				} else {
					this._remove( key );
				}
			}
		}

		/** @inheritDoc */
		stream( {
			prefix = "",
			maxDepth = Infinity,
			separator = "/",
			target = "entry",
			invalidPolicy = "skip",
		} = {} ) {
			const sep = separator || "/";

			while ( prefix.endsWith( sep ) ) {
				prefix = prefix.slice( 0, prefix.length - 1 );
			}

			const { _journal, dataSource } = this;
			const sources = [dataSource];

			if ( _journal ) {
				sources.push( _journal );
			}

			const whenReady = this.waitForTransaction( this._transactionId );
			let iterator;

			return new Readable( {
				objectMode: true,
				read() {
					whenReady.then( () => {
						let item;
						let writable = true;

						while ( writable ) {
							if ( !iterator ) {
								// enumerate next available source
								const map = sources.shift();

								if ( map ) {
									iterator = map[Symbol.iterator]();
								} else {
									this.push( null );
									break;
								}
							}

							while ( writable ) {
								item = iterator.next();

								if ( item.done ) {
									iterator = null;
									break;
								}

								const [ key, value ] = item.value;

								if ( typeof key !== "string" ) {
									continue;
								}

								if ( _journal?.has( key ) ) {
									// journal tracks an update of this record
									// -> ignore now and consider it later when enumerating journal
									continue;
								}

								if ( !value ) {
									// record is missing (e.g. when journal tracks item to be removed)
									continue;
								}

								const prefixLength = prefix.length;

								if ( key.slice( 0, prefixLength ) !== prefix ) {
									continue;
								}

								const sub = key.slice( prefixLength );

								if ( separator != null ) {
									const lead = key[prefixLength];

									if ( prefix.length > 0 && lead !== sep && lead != null ) {
										continue;
									}

									const segments = sub.split( sep );
									let depth = segments.length;
									let cursor = 0;

									while ( cursor < depth && !segments[cursor].length ) {
										cursor++;
									}

									depth -= cursor;
									if ( depth > maxDepth ) {
										continue;
									}
								}

								if ( typeof value !== "object" || !value ) {
									switch ( invalidPolicy ) {
										case "fail" :
											logError( "invalid memory record at %s of type %s: %s", key, typeof value, String( value ) );
											throw new Error( "invalid memory record at %s" );

										case "skip" :
											logDebug( "skipping invalid/malformed memory record at %s of type %s: %s", key, typeof value, String( value ) );
											continue;

										default :
											throw new TypeError( "unsupported invalidPolicy parameter" );
									}
								}

								switch ( target ) {
									case "key" :
										writable = this.push( key );
										break;

									case "value" :
										writable = this.push( value );
										break;

									case "entry" :
										writable = this.push( { key, value } );
										break;

									default :
										throw new TypeError( "invalid target parameter" );
								}
							}
						}
					} )
						.catch( cause => this.destroy( cause ) );
				},
			} );
		}

		/** @inheritDoc */
		static get benefitsFromCaching() { return false; }
	}

	return OdemAdapterMemory;
};
