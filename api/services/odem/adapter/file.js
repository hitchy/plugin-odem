import { sep, posix, resolve as PathResolve, dirname } from "node:path";
import FS from "node:fs";
import { PassThrough, Readable } from "node:stream";

import FileEssentials from "file-essentials";

const DefaultConfig = {
	dataSource: "./data",
};

const FolderCreator = new Map();


/**
 * Implements map of queues each for synchronizing access on a single file of
 * backend.
 */
class FileQueueMap extends Map {
	/**
	 * Aligns provided asynchronous handler accessing selected file with other
	 * handlers intending to access the same file.
	 *
	 * @param {string} filename name of file to be processed by handler
	 * @param {function():Promise<any>} asyncHandler handler processing named file promising some result
	 * @return {Promise<any>} promises whatever provided handler is promising on invocation
	 */
	enqueue( filename, asyncHandler ) {
		let queue = this.get( filename );
		let started = false;

		if ( !Array.isArray( queue ) ) {
			queue = [];
			started = true;

			this.set( filename, queue );
		}

		let proxyResolve, proxyReject;
		const proxy = new Promise( ( resolve, reject ) => {
			proxyResolve = resolve;
			proxyReject = reject;
		} );

		const wrapper = () => {
			const done = asyncHandler();

			done.then( proxyResolve ).catch( proxyReject );

			done
				.catch( () => undefined )
				.then( () => {
					if ( queue.length > 0 ) {
						queue.shift()();
					} else {
						this.delete( filename );
					}
				} );
		};

		if ( started ) {
			wrapper();
		} else {
			queue.push( wrapper );
		}

		return proxy;
	}
}

const FileQueues = new FileQueueMap();

/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	const logDebug = api.log( "hitchy:odem:file:debug" );
	const logError = api.log( "hitchy:odem:file:error" );

	/**
	 * Implements backend managing all data in files of local file system.
	 *
	 * @name OdemAdapterFile
	 * @alias this.services.OdemAdapterFile
	 * @extends OdemAdapter
	 * @property {object} config options customizing current adapter
	 * @property {string} dataSource path name of folder containing all data files
	 */
	class OdemAdapterFile extends Services.OdemAdapter {
		/**
		 * @param {object} config configuration of adapter
		 */
		constructor( config ) {
			super();

			const _config = Object.assign( {}, DefaultConfig, config );

			Object.defineProperties( this, {
				/**
				 * Exposes configuration customizing behaviour of current adapter.
				 *
				 * @name OdemAdapterFile#config
				 * @type {Object}
				 * @readonly
				 */
				config: { value: _config },

				/**
				 * Exposes data source managed by current adapter.
				 *
				 * @note In context of OdemAdapterFile this is a promise for the path
				 *       name of folder containing all files managed by this adapter
				 *       actually existing in filesystem.
				 *
				 * @name OdemAdapterFile#dataSource
				 * @type {Promise<string>}
				 * @readonly
				 */
				dataSource: { value: this.constructor.makeDirectory( api.folder( _config.dataSource ) ) },

				/**
				 * Indicates if current adapter is basically a copy of some
				 * original adapter which has been bound to a transaction.
				 *
				 * @prop isInATransaction
				 * @name OdemAdapterMemory#isInATransaction
				 * @type {boolean}
				 */
				isInATransaction: { value: ( _config._transactionId || 0 ) > 0 },

				/**
				 * Exposes ID of transaction this copy of original memory
				 * adapter has been bound to.
				 *
				 * @private
				 */
				_transactionId: { value: _config._transactionId || 0 },

				/**
				 * Tracks changes to be persisted once the transaction this
				 * adapter is bound to has completed.
				 */
				_journal: { value: _config._transactionId ? new Map() : undefined },

				/**
				 * Exposes capabilities of this adapter instance. This is used by
				 * Odem to decide when to use available features and when to use
				 * polyfills for a missing feature.
				 *
				 * @type {{read: boolean, remove: boolean, stream: boolean, test: boolean, transact: boolean, watch: boolean, write: boolean}}
				 */
				can: {
					value: {
						test: true,
						read: true,
						write: true,
						remove: true,
						stream: true,
						watch: true,
						transact: !( _config._transactionId > 0 ),
					},
					enumerable: true
				},
			} );
		}

		/**
		 * Safely creates folders using map of currently running folder creations to
		 * prevent multiple actions trying to create same folder resulting in race
		 * conditions.
		 *
		 * @param {string} baseFolder base folder
		 * @param {string|string[]} folder sub-folder to create in context of given base folder
		 * @returns {Promise<string>} promises path name of created folder
		 */
		static makeDirectory( baseFolder, folder = null ) {
			const _folder = folder == null ? baseFolder : Array.isArray( folder ) ? PathResolve( baseFolder, ...folder ) : PathResolve( baseFolder, folder );

			if ( FolderCreator.has( _folder ) ) {
				return FolderCreator.get( _folder );
			}

			const promise = FileEssentials.MkDir( baseFolder, folder );

			FolderCreator.set( _folder, promise );

			promise
				.then( () => FolderCreator.delete( _folder ) )
				.catch( () => FolderCreator.delete( _folder ) );

			return promise;
		}

		/** @inheritDoc */
		async purge() {
			let allKeys;

			await this.transaction( async tx => {
				allKeys = await new Promise( ( resolve, reject ) => {
					const keys = [];
					const stream = tx.stream( { target: "key" } );

					stream.on( "data", key => keys.push( key ) );
					stream.once( "error", reject );
					stream.once( "end", () => resolve( keys ) );
				} );

				const path = await tx.dataSource;

				await FileEssentials.RmDir( path, {
					subsOnly: true
				} );
			} );

			for ( const key of allKeys ) {
				this.emit( "delete", key );
			}
		}

		/** @inheritDoc */
		async create( keyTemplate, data ) {
			await this.waitForTransaction( this._transactionId );

			const path = await this.dataSource;
			let key = null;
			let filename;
			let fd;

			while ( true ) {
				( { fd } = await FileEssentials.MkFile( path, { // eslint-disable-line no-await-in-loop
					uuidToPath: uuid => {
						key = keyTemplate.replace( /%u/g, uuid );
						filename = this.constructor.keyToPath( key );

						return filename.split( /[\\/]/ );
					},
				} ) );

				if ( this._journal ) {
					if ( this._journal.has( key ) ) {
						FS.close( fd );
						continue;
					}

					this._journal.set( key, { data, created: true } );
				}

				const promise = new Promise( ( resolve, reject ) => {
					FS.write( fd, JSON.stringify( data ), 0, "utf8", writeError => {
						if ( writeError ) {
							FS.close( fd, () => {
								if ( this._journal ) {
									this._journal.delete( key );
								}

								reject( writeError );
							} );
						} else {
							FS.close( fd, closeError => {
								if ( closeError ) {
									if ( this._journal ) {
										this._journal.delete( key );
									}

									reject( closeError );
								} else {
									if ( !this._journal ) {
										this.emit( "change", key, data, undefined );
									}

									resolve( key );
								}
							} );
						}
					} );
				} );

				FileQueues.enqueue( PathResolve( path, filename ), () => promise );

				return promise;
			}
		}

		/** @inheritDoc */
		async has( key ) {
			if ( this._journal?.has( key ) ) {
				return Boolean( this._journal.get( key ).record );
			}

			const path = await this.dataSource;
			const stat = await FileEssentials.Stat( PathResolve( path, this.constructor.keyToPath( key ) ) );

			return Boolean( stat );
		}

		/** @inheritDoc */
		async read( key, { ifMissing = null } = {} ) {
			if ( this._transactionId ) {
				await this.waitForTransaction( this._transactionId );
			}

			const { _journal } = this;

			if ( _journal && _journal.has( key ) ) {
				const record = _journal.get( key );

				if ( record?.data ) {
					return record.data;
				}

				if ( ifMissing ) {
					return ifMissing;
				}

				throw Object.assign( new Error( `no such record @${key}` ), { code: "ENOENT" } );
			}

			const subPath = this.constructor.keyToPath( key );

			if ( subPath.match( /[\\/]\.\.?[\\/]/ ) ) {
				throw Object.assign( new Error( "invalid key: " + key ), { code: "EBADKEY" } );
			}

			const basePath = await this.dataSource;
			const filename = PathResolve( basePath, subPath );

			return FileQueues.enqueue( filename, () => {
				return FS.promises.readFile( filename, { encoding: "utf8" } )
					.then( content => JSON.parse( content ) )
					.catch( error => {
						if ( error.code === "ENOENT" ) {
							if ( ifMissing ) {
								return ifMissing;
							}

							throw Object.assign( new Error( `no such record @${key}` ), { code: "ENOENT" } );
						}

						throw error;
					} );
			} );
		}

		/** @inheritDoc */
		async write( key, data ) {
			await this.waitForTransaction( this._transactionId );

			if ( this._journal ) {
				this._journal.set( key, this._journal.get( key )?.created ? { data, created: true } : { data } );
			} else {
				await this._write( key, data );
			}

			return data;
		}

		/**
		 * Internally writes entry with given key to data source without any
		 * safeguards.
		 *
		 * @param {string} key key of record to remove
		 * @param {Object} data record to write
		 * @private
		 */
		async _write( key, data ) {
			const path = this.constructor.keyToPath( key );

			if ( path.match( /[\\/]\.\.?[\\/]/ ) ) {
				throw new Error( "invalid key: " + key );
			}

			const basePath = await this.dataSource;
			const parent = dirname( path );

			switch ( parent ) {
				case "" :
				case "." :
				case "/" :
					break;

				default :
					await this.constructor.makeDirectory( basePath, parent );
			}

			const filename = PathResolve( basePath, path );

			const previousData = await FileQueues.enqueue( filename, async() => {
				// try reading existing record from filesystem (to include it in emitted change notification)
				const record = await FS.promises.readFile( filename, "utf-8" ).catch( cause => {
					if ( cause.code === "ENOENT" ) {
						return "{}";
					}

					throw cause;
				} );

				// always write new record to the file no matter its previous content
				// (using createWriteStream() for better performance)
				await new Promise( ( resolve, reject ) => {
					const writer = FS.createWriteStream( filename );

					writer.once( "error", reject );
					writer.end( JSON.stringify( data ), "utf-8", resolve );
				} );

				try {
					return JSON.parse( record );
				} catch ( cause ) {
					logError( `found malformed record ${key} in filesystem: ${cause.message}` );

					throw cause;
				}
			} );

			this.emit( "change", key, data, { key, value: previousData } );

			return data;
		}

		/** @inheritDoc */
		async remove( key ) {
			await this.waitForTransaction( this._transactionId );

			if ( this._journal ) {
				this._journal.set( key, { data: false } );
			} else {
				await this._remove( key );
			}

			return key;
		}

		/**
		 * Internally removes entry with given key from data source without any
		 * safeguards.
		 *
		 * @param {string} key key of record to remove
		 * @private
		 */
		async _remove( key ) {
			const path = this.constructor.keyToPath( key );

			if ( path.match( /[\\/]\.\.?[\\/]/ ) ) {
				throw new Error( "invalid key: " + key );
			}

			const basePath = await this.dataSource;

			await FileEssentials.RmDir( PathResolve( basePath, path ) );

			this.emit( "delete", key );

			return key;
		}

		/** @inheritDoc */
		async _handleTransaction( id, handler, options = {} ) { // eslint-disable-line no-unused-vars
			const boundAdapter = new this.constructor( {
				...this.config,
				_transactionId: id,
			} );

			let promises;

			try {
				await handler( boundAdapter );
			} catch ( cause ) {
				promises = [];

				for ( const [ key, { created } ] of boundAdapter._journal.entries() ) {
					if ( created ) {
						promises.push( this._remove( key ) );
					}
				}

				await Promise.all( promises );

				throw cause;
			}

			promises = new Array( boundAdapter._journal.size );
			let write = 0;

			for ( const [ key, { data } ] of boundAdapter._journal.entries() ) {
				if ( data ) {
					promises[write++] = this._write( key, data );
				} else {
					promises[write++] = this._remove( key );
				}
			}

			await Promise.all( promises );
		}

		/** @inheritDoc */
		stream( {
			prefix = "",
			maxDepth = Infinity,
			separator = "/",
			target = "entry",
			invalidPolicy = "skip"
		} = {} ) {
			const { _journal, constructor: { pathToKey, keyToPath } } = this;

			const ptnFinal = /(^|[\\/])[ps][^\\/]+$/i;
			const prefixPath = keyToPath( prefix );
			const stream = new PassThrough( { objectMode: true } );

			this.waitForTransaction( this._transactionId )
				.then( () => this.dataSource )
				.then( async basePath => {
					const prefixBasePath = PathResolve( basePath, prefixPath );
					const stats = await FileEssentials.Stat( prefixBasePath );

					if ( !stats ) {
						streamFromJournal();
						return;
					}

					if ( !stats.isDirectory() ) {
						// is prefix set to match a single record's file exclusively?
						let push;

						try {
							push = !_journal?.has( pathToKey( prefixPath ) );
						} catch {
							push = false;
						}

						if ( push ) {
							// yes -> deliver that exclusively matching file
							stream.push( await converter.call( {}, ".", prefixBasePath, stats ) );
						}

						streamFromJournal();
						return;
					}

					let files = FileEssentials.Find( prefixBasePath, {
						stream: true,
						filter( local, full, state ) {
							const path = posix.join( prefixPath, local );

							if ( state.isDirectory() ) {
								// decide whether to descend into the folder or not
								if ( separator ) {
									try {
										if ( pathToKey( local ).split( separator ).length >= maxDepth ) {
											return false;
										}
									} catch ( cause ) {
										// provided path fails to yield a proper key
										// -> need to descend if failure is due to insufficient information
										return Boolean( cause.partialPath );
									}
								}

								// do not descend if last segment of current path
								// is no apparent part of properly encoded key
								return ptnFinal.test( path );
							}

							if ( !ptnFinal.test( path ) ) {
								return false;
							}

							try {
								// consider this file
								// - if its pathname can be converted into valid key of Odem adapter
								// - unless it has been tracked transaction journal, too
								//   -> going to consider those later
								return !_journal?.has( pathToKey( path ) );
							} catch {
								return false;
							}
						},
						waitForConverter: true,
						converter,
					} );

					stream.once( "close", () => {
						if ( files ) {
							files.unpipe( stream );
							files.pause();
							files.destroy();
						}
					} );

					files.once( "end", () => {
						files = null;

						streamFromJournal();
					} );

					files.pipe( stream, { end: false } );
				} )
				.catch( cause => {
					stream.emit( "error", cause );
				} );

			return stream;

			/** */
			async function converter( local, full, state ) {
				if ( !state.isDirectory() ) {
					const path = posix.join( prefixPath, local );
					const key = pathToKey( path );
					let value;

					if ( target === "key" ) {
						return key;
					}

					try {
						value = JSON.parse( await FS.promises.readFile( full, "utf-8" ) );
					} catch ( cause ) {
						switch ( invalidPolicy ) {
							case "fail" :
								logError( "reading record at %s from file %s and parsing it as JSON has failed: %s", key, full, cause.message );
								throw cause;

							case "skip" :
								logDebug( "skipping malformed record %s in file %s: %s", key, full, cause.message );
								return null;

							default :
								throw new TypeError( "unsupported invalidPolicy parameter" );
						}
					}

					switch ( target ) {
						case "value" :
							return value;

						case "entry" :
							return { key, value };

						default :
							throw new TypeError( "invalid target parameter" );
					}
				}

				return null;
			}

			/** */
			function streamFromJournal() {
				if ( _journal ) {
					const iterator = _journal[Symbol.iterator]();

					const journalStream = new Readable( {
						read() {
							let writable = true;

							while ( writable ) {
								const item = iterator.next();

								if ( item.done ) {
									this.push( null );
									return;
								}

								const [ key, value ] = item.value;

								const prefixLength = prefix.length;

								if ( key.slice( 0, prefixLength ) !== prefix ) {
									continue;
								}

								if ( typeof value !== "object" || !value ) {
									switch ( invalidPolicy ) {
										case "fail" :
											logError( "invalid memory record at %s of type %s: %s", key, typeof value, String( value ) );
											throw new Error( "invalid memory record at %s" );

										case "skip" :
											logDebug( "skipping invalid/malformed memory record at %s of type %s: %s", key, typeof value, String( value ) );
											continue;

										default :
											throw new TypeError( "unsupported invalidPolicy parameter" );
									}
								}

								switch ( target ) {
									case "key" :
										writable = this.push( key );
										break;

									case "value" :
										writable = this.push( value );
										break;

									case "entry" :
										writable = this.push( { key, value } );
										break;

									default :
										throw new TypeError( "invalid target parameter" );
								}
							}
						}
					} );

					stream.once( "close", () => {
						journalStream.destroy();
					} );

					journalStream.pipe( stream );
				} else {
					stream.end();
				}
			}
		}

		/** @inheritDoc */
		static keyToPath( key ) {
			if ( key === "" ) {
				return "";
			}

			const segments = key.replace( /\/$/, "" ).split( /\//g );
			const length = segments.length;
			const copy = new Array( length * 3 );
			let write = 0;

			for ( let i = 0; i < length; i++ ) {
				const segment = segments[i];

				if ( Services.OdemUtilityUuid.ptnUuid.test( segment ) ) {
					copy[write++] = "p" + segment[0];
					copy[write++] = "p" + segment.slice( 1, 3 );
					copy[write++] = "p" + segment.slice( 3 );
				} else {
					copy[write++] = "s" + segment;
				}
			}

			copy.splice( write );

			return copy.join( sep );
		}

		/** @inheritDoc */
		static pathToKey( path ) {
			if ( path === "" ) {
				return "";
			}

			const segments = path.replace( /\/$/, "" ).split( /[\\/]/g );
			const numSegments = segments.length;
			const decoded = new Array( numSegments );
			let write = 0;

			for ( let i = 0; i < numSegments; i++ ) {
				const segment = segments[i];

				switch ( segment[0] ) {
					case "P" :
					case "p" : {
						// found first part of UUID distributed over three
						// consecutive segments of path name
						// --> require two more according segments
						const next = segments[i + 1];
						const second = segments[i + 2];

						if ( next == null || second == null ) {
							throw Object.assign( new Error( "insufficient UUID partials in path" ), { partialPath: true } );
						}

						// follow-up segments do not provide further parts of UUID
						// -> invalid path name
						if ( ( next[0] !== "p" && next[0] !== "P" ) || ( second[0] !== "p" && second[0] !== "P" ) ) {
							throw new Error( "invalid UUID partials in path" );
						}

						// re-compile UUID from separate parts
						decoded[write++] = segment.slice( 1 ) + next.slice( 1 ) + second.slice( 1 );
						i += 2;
						break;
					}

					case "S" :
					case "s" :
						// found a non-UUID-related segment -> decode
						decoded[write++] = segment.slice( 1 );
						break;

					default :
						throw new Error( "malformed segment in path name" );
				}
			}

			decoded.splice( write );

			return decoded.join( "/" );
		}
	}

	return OdemAdapterFile;
};
