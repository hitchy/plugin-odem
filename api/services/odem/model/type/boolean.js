/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Implements scalar type `boolean` for use with attributes of defined models.
	 *
	 * @name OdemModelTypeBoolean
	 * @extends ModelType
	 */
	class OdemModelTypeBoolean extends Services.OdemModelType {
		/** @inheritDoc */
		static get typeName() {
			return "boolean";
		}

		/** @inheritDoc */
		static get aliases() {
			return ["bool"];
		}


		/** @inheritDoc */
		static coerce( value, requirements, defaultMarker ) {
			if ( value === defaultMarker && requirements ) {
				value = requirements.default;
			}

			if ( value == null ) {
				value = null;
			} else if ( /^\s*(t(?:rue)?|y(?:es)?|set|on)\s*$/i.test( value ) ) {
				value = true;
			} else if ( /^\s*(0|f(?:alse)?|no?|unset|off)\s*$/i.test( value ) ) {
				value = false;
			} else {
				value = Boolean( value );
			}

			return value;
		}


		/** @inheritDoc */
		static isValid( name, value, requirements, errors ) {
			if ( value == null ) {
				if ( requirements.required ) {
					errors.push( new Error( `${name} must be boolean value` ) );
				}
			} else {
				const { isSet } = requirements;

				if ( isSet && !value ) {
					errors.push( new Error( `${name} must be set` ) );
				}
			}
		}


		/** @inheritDoc */
		static serialize( value, adapter ) { // eslint-disable-line no-unused-vars
			if ( value === "null" || value == null ) {
				value = null;
			} else {
				value = value ? 1 : 0;
			}

			return value;
		}



		/** @inheritDoc */
		static deserialize( value ) {
			if ( value === "null" || value == null ) {
				value = null;
			} else if ( typeof value === "string" ) {
				value = value.trim();

				if ( global.hitchyPtnTrue.test( value ) ) {
					value = true;
				} else if ( global.hitchyPtnFalse.test( value ) ) {
					value = false;
				} else if ( global.hitchyPtnFloat.test( value ) ) {
					value = Boolean( parseFloat( value ) );
				} else {
					value = Boolean( value );
				}
			} else {
				value = Boolean( value );
			}

			return value;
		}


		/** @inheritDoc */
		static compare( value, reference, operation ) {
			let result;

			switch ( operation ) {
				case "eq" :
					result = value === reference;
					break;

				case "neq" :
				case "noteq" :
					result = value !== reference;
					break;

				case "null" :
					result = value == null;
					break;

				case "notnull" :
					result = value != null;
					break;

				case "not" :
					result = !value;
					break;

				case "gte" :
				case "lte" :
					result = ( value == null && reference == null ) || value === reference;
					break;

				default :
					result = false;
					break;
			}

			return result;
		}

		/** @inheritDoc */
		static sort( left, right ) {
			return left == null ? 1 : right == null ? -1 : left === right ? 0 : left ? -1 : 1;
		}
	}

	return OdemModelTypeBoolean;
};
