/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Implements scalar type `integer` for use with attributes of defined models.
	 *
	 * @name OdemModelTypeInteger
	 * @extends OdemModelType
	 */
	class OdemModelTypeInteger extends Services.OdemModelType {
		/** @inheritDoc */
		static get typeName() {
			return "integer";
		}

		/** @inheritDoc */
		static get aliases() {
			return ["int"];
		}

		/** @inheritDoc */
		static checkDefinition( definition ) {
			const errors = super.checkDefinition( definition );
			if ( !errors.length ) {
				if ( definition.min > definition.max ) {
					const min = definition.min;
					definition.min = definition.max;
					definition.max = min;
				}

				const { min, max, step } = definition;

				if ( min != null && ( typeof min === "object" || !Services.OdemUtilityNumber.ptnFloat.test( min ) ) ) {
					errors.push( new TypeError( "invalid requirement on minimum value" ) );
				}

				if ( max != null && ( typeof max === "object" || !Services.OdemUtilityNumber.ptnFloat.test( max ) ) ) {
					errors.push( new TypeError( "invalid requirement on maximum value" ) );
				}

				if ( step != null && ( !step || step < 0 || typeof step === "object" || !Services.OdemUtilityNumber.ptnFloat.test( step ) ) ) {
					errors.push( new TypeError( "invalid requirement on value stepping" ) );
				}
			}

			return errors;
		}


		/** @inheritDoc */
		static coerce( value, requirements, defaultMarker ) {
			if ( value === defaultMarker && requirements ) {
				value = requirements.default;
			}

			if ( value == null ) {
				value = null;
			} else {
				switch ( typeof value ) {
					case "string" :
						value = value.trim();

						if ( !value.length ) {
							value = null;
							break;
						}

						if ( !global.hitchyPtnFloat.test( value ) ) {
							value = NaN;
							break;
						}

					// falls through
					case "number" : {
						value = Math.round( parseFloat( value ) );

						const { step, min = 0 } = requirements;
						if ( step ) {
							value = Math.round( ( Math.round( ( value - min ) / step ) * step ) + min );
						}
						break;
					}

					default :
						value = NaN;
				}
			}

			return value;
		}



		/** @inheritDoc */
		static serialize( value, adapter ) { // eslint-disable-line no-unused-vars
			value = value == null ? null : parseInt( value );

			return value;
		}


		/** @inheritDoc */
		static isValid( name, value, requirements, errors ) {
			if ( requirements.required && ( value == null || isNaN( value ) ) ) {
				errors.push( new Error( `${name} is not a number` ) );
			}

			if ( value != null ) {
				const { min, max } = requirements;

				if ( !isNaN( min ) && ( isNaN( value ) || value < min ) ) {
					errors.push( new Error( `${name} is below required minimum` ) );
				}

				if ( !isNaN( max ) && ( isNaN( value ) || value > max ) ) {
					errors.push( new Error( `${name} is above required maximum` ) );
				}
			}
		}
	}

	return OdemModelTypeInteger;
};
