/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Implements scalar type `string` for use with attributes of defined models.
	 *
	 * @name OdemModelTypeString
	 * @extends OdemModelType
	 */
	class OdemModelTypeString extends Services.OdemModelType {
		/** @inheritDoc */
		static get typeName() {
			return "string";
		}

		/** @inheritDoc */
		static get aliases() {
			return ["text"];
		}

		/** @inheritDoc */
		static checkDefinition( definition ) {
			const errors = super.checkDefinition( definition );
			if ( !errors.length ) {
				if ( definition.minLength > definition.maxLength ) {
					const minLength = definition.minLength;
					definition.minLength = definition.maxLength;
					definition.maxLength = minLength;
				}

				if ( definition.maxLength < 1 ) {
					errors.push( new TypeError( "invalid requirement on maximum length" ) );
				}

				if ( definition.minLength < 0 ) {
					errors.push( new TypeError( "invalid requirement on minimum length" ) );
				}

				if ( definition.upperCase && definition.lowerCase ) {
					errors.push( new TypeError( "ambiguous request for converting case" ) );
				}
			}

			return errors;
		}


		/** @inheritDoc */
		static coerce( value, requirements, defaultMarker ) {
			if ( value === defaultMarker && requirements ) {
				value = requirements.default;
			}

			if ( value == null ) {
				value = null;
			} else {
				if ( typeof value !== "string" ) {
					value = String( value );
				}

				if ( requirements.trim ) {
					value = value.trim();
				}

				if ( requirements.reduceSpace ) {
					value = value.replace( /\s+/g, " " );
				}

				const { lowerCase, upperCase } = requirements;

				if ( upperCase ) {
					switch ( typeof upperCase ) {
						case "object" :
							if ( Array.isArray( upperCase ) ) {
								value = value.toLocaleUpperCase( upperCase );
							} else {
								value = value.toLocaleUpperCase();
							}
							break;

						case "string" :
							value = value.toLocaleUpperCase( upperCase );
							break;

						default :
							value = value.toLocaleUpperCase();
					}
				}

				if ( lowerCase ) {
					switch ( typeof lowerCase ) {
						case "object" :
							if ( Array.isArray( lowerCase ) ) {
								value = value.toLocaleLowerCase( lowerCase );
							} else {
								value = value.toLocaleLowerCase();
							}
							break;

						case "string" :
							value = value.toLocaleLowerCase( lowerCase );
							break;

						default :
							value = value.toLocaleLowerCase();
					}
				}
			}

			return value;
		}


		/** @inheritDoc */
		static isValid( name, value, requirements, errors ) {
			if ( value == null ) {
				if ( requirements.required ) {
					errors.push( new Error( `${name} is required, but missing` ) );
				}
			} else {
				const { minLength, maxLength, pattern } = requirements;

				if ( maxLength > -1 ) {
					if ( value.length > maxLength ) {
						errors.push( new Error( `${name} exceeds maximum length of ${maxLength} characters` ) );
					}
				}

				if ( minLength > -1 ) {
					if ( value.length < minLength ) {
						errors.push( new Error( `${name} must contain at least ${minLength} characters` ) );
					}
				}

				if ( pattern ) {
					const re = typeof pattern === "string" ? new RegExp( pattern ) : pattern;

					if ( !re.test( value ) ) {
						errors.push( new Error( `${name} does not match required pattern` ) );
					}
				}
			}
		}


		/** @inheritDoc */
		static serialize( value, adapter ) { // eslint-disable-line no-unused-vars
			value = value == null ? null : String( value );

			return value;
		}


		/** @inheritDoc */
		static compare( value, reference, operation ) {
			let result;

			switch ( operation ) {
				case "eq" :
					result = value == null ? reference == null : value === reference;
					break;

				case "neq" :
				case "noteq" :
					result = value == null ? reference != null : value !== reference;
					break;

				case "lt" :
					result = value != null && reference != null && value.localeCompare( reference ) < 0;
					break;

				case "lte" :
					if ( value == null ) {
						result = reference == null;
					} else {
						result = reference != null && value.localeCompare( reference ) < 1;
					}
					break;

				case "gt" :
					result = value != null && reference != null && value.localeCompare( reference ) > 0;
					break;

				case "gte" :
					if ( value == null ) {
						result = reference == null;
					} else {
						result = reference != null && value.localeCompare( reference ) > -1;
					}
					break;

				case "null" :
					result = value == null;
					break;

				case "notnull" :
					result = value != null;
					break;

				case "not" :
					result = !value;
					break;

				default :
					result = false;
					break;
			}

			return result;
		}

		/** @inheritDoc */
		static sort( left, right ) {
			return left == null ? 1 : right == null ? -1 : String( left ).localeCompare( right );
		}
	}

	return OdemModelTypeString;
};
