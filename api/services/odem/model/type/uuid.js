/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Implements scalar type `string` for use with attributes of defined models.
	 *
	 * @name ModelTypeString
	 * @extends OdemModelType
	 */
	class OdemModelTypeUUID extends Services.OdemModelType {
		/** @inheritDoc */
		static get typeName() {
			return "uuid";
		}

		/** @inheritDoc */
		static get aliases() {
			return [ "key", "foreign", "foreign key" ];
		}


		/** @inheritDoc */
		static coerce( value, requirements, defaultMarker ) {
			if ( value === defaultMarker && requirements ) {
				value = requirements.default;
			}

			if ( value == null ) {
				value = null;
			} else {
				if ( !Buffer.isBuffer( value ) ) {
					if ( typeof value === "string" ) {
						value = value.trim();
					} else {
						value = String( value ).trim();
					}

					if ( /^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}$/i.test( value ) ) {
						value = Buffer.from( value.replace( /-/g, "" ), "hex" );
					} else {
						value = null;
					}
				}

				if ( value && value.length !== 16 ) {
					value = null;
				}
			}

			return value;
		}


		/** @inheritDoc */
		static isValid( name, value, requirements, errors ) {
			if ( value == null ) {
				if ( requirements.required ) {
					errors.push( new Error( `${name} is required, but missing` ) );
				}
			}
		}


		/** @inheritDoc */
		static serialize( value, adapter ) {
			if ( value == null ) {
				value = null;
			} else if ( !adapter.supportsBinary ) {
				value = value.toString( "hex" ).replace( /^(.{8})(.{4})(.{4})(.{4})(.{12})$/, "$1-$2-$3-$4-$5" );
			}

			return value;
		}



		/** @inheritDoc */
		static deserialize( value ) {
			if ( value == null ) {
				value = null;
			} else if ( typeof value === "string" && /^[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}$/i.test( value ) ) {
				value = Buffer.from( value.replace( /-/g, "" ), "hex" );
			} else if ( !Buffer.isBuffer( value ) || value.length !== 16 ) {
				value = null;
			}

			return value;
		}


		/** @inheritDoc */
		static compare( value, reference, operation ) {
			let result;

			switch ( operation ) {
				case "eq" :
					if ( value == null ) {
						result = reference == null;
					} else {
						result = reference != null && value.equals( reference );
					}
					break;

				case "neq" :
				case "noteq" :
					if ( value == null ) {
						result = reference != null;
					} else {
						result = reference == null || !value.equals( reference );
					}
					break;

				case "lt" :
					if ( value == null || reference == null ) {
						result = false;
					} else {
						result = value.compare( reference ) < 0;
					}
					break;

				case "lte" :
					if ( value == null ) {
						result = reference == null;
					} else if ( reference == null ) {
						result = false;
					} else {
						result = value.compare( reference ) < 1;
					}
					break;

				case "gt" :
					if ( value == null || reference == null ) {
						result = false;
					} else {
						result = value.compare( reference ) > 0;
					}
					break;

				case "gte" :
					if ( value == null ) {
						result = reference == null;
					} else if ( reference == null ) {
						result = false;
					} else {
						result = value.compare( reference ) > -1;
					}
					break;

				case "null" :
					result = value == null;
					break;

				case "notnull" :
					result = value != null;
					break;

				case "not" :
					result = !value;
					break;

				default :
					result = false;
					break;
			}

			return result;
		}

		/** @inheritDoc */
		static sort( left, right ) {
			if ( left == null ) {
				return 1;
			}

			if ( right == null ) {
				return -1;
			}

			for ( let i = 0; i < 16; i++ ) {
				const diff = left[i] - right[i];

				if ( diff !== 0 ) {
					return diff;
				}
			}

			return 0;
		}
	}

	return OdemModelTypeUUID;
};
