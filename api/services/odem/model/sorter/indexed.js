import { Transform } from "node:stream";

/** */
export default function() {
	/**
	 * Implements stream sorting items with the help of an index.
	 *
	 * @alias this.services.OdemModelSorterIndexed
	 */
	class OdemModelSorterIndexed extends Transform {
		/**
		 * @param {EqualityIndex} index index of values to use
		 * @param {boolean} ascending true for sorting in ascending order
		 */
		constructor( index, ascending ) {
			super( {
				objectMode: true,
			} );


			const generator = index.findBetween( { descending: !ascending } );
			const iterator = generator();


			Object.defineProperties( this, {
				/**
				 * Generates UUIDs covered by index in desired sorting order.
				 *
				 * @name OdemModelSorterIndexed#sortedUuids
				 * @property {Generator}
				 * @readonly
				 */
				sortedUUIDs: { value: iterator },

				/**
				 * Indicates whether sorting in ascending order or not.
				 *
				 * @name NonIndexedSorter#ascending
				 * @property {boolean}
				 * @readonly
				 */
				ascending: { value: Boolean( ascending ) },

				/**
				 * Collects items that could not be sorted yet.
				 *
				 * @name NonIndexedSorter#collector
				 * @property {Model[]}
				 * @readonly
				 */
				collector: { value: [] },
			} );


			this.nextAssumed = iterator.next();
		}

		/** @inheritDoc */
		_transform( item, _, done ) {
			const { done: doneBefore, value: nextUUID } = this.nextAssumed;

			if ( doneBefore ) {
				// met end of sorting property's index before
				// -> property is undefined/null for any further item
				//    -> just pass ...
				this.push( item );
			} else if ( item.$uuid === nextUUID ) {
				// got item which is next in desired sorting
				// -> do not collect, but pass instantly
				this.push( item );

				// prepare to find next assumed record
				const collecteds = this.collector;
				let doneNow = false;
				let foundAgain = true;

				while ( !doneNow && foundAgain ) {
					const iter = this.nextAssumed = this.sortedUUIDs.next();

					doneNow = iter.done;
					if ( !doneNow ) {
						const numCollected = collecteds.length;

						foundAgain = false;

						for ( let i = 0; i < numCollected; i++ ) {
							const collected = collecteds[i];

							if ( collected.$uuid.equals( iter.value ) ) {
								// collected next item in sorting index before
								// -> push instantly
								this.push( collected );

								collecteds.splice( i, 1 );
								foundAgain = true;
								break;
							}
						}
					}
				}

				if ( doneNow ) {
					// met end of index covering actual values of sorting property
					// -> flush all previously collected items for they will not be
					//    sorted any better
					const numCollected = collecteds.length;
					for ( let i = 0; i < numCollected; i++ ) {
						this.push( collecteds[i] );
					}

					this.collector.splice( 0 );
				}
			} else {
				// need to collect this item to be sorted later
				this.collector.push( item );
			}

			done();
		}

		/** @inheritDoc */
		_flush( doneFn ) {
			const collected = this.collector;
			let numCollected = collected.length;

			if ( numCollected > 0 ) {
				// there are some items left collected for being sorted right now
				// -> push every matching item encountered while consuming rest of sorting index
				let done = false, value = this.nextAssumed.value;

				while ( !done ) {
					for ( let i = 0; i < numCollected; i++ ) {
						const item = collected[i];

						if ( item.$uuid.equals( value ) ) {
							this.push( item );

							collected.splice( i, 1 );
							numCollected--;
							break;
						}
					}

					const iter = this.sortedUUIDs.next();
					done = iter.done;
					value = iter.value;
				}
			}

			doneFn();
		}
	}

	return OdemModelSorterIndexed;
};
