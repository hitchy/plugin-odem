/** */
export default function() {
	const api = this;
	const { services: Services } = api;
	const logDebug = api.log( "hitchy:odem:debug" );

	/**
	 * Implements abstract base for any tester supported by model collection
	 * processing.
	 *
	 * @alias this.services.OdemModelTester
	 */
	class OdemModelTester {
		/**
		 * Compiles tester instance according to provided test description.
		 *
		 * @abstract
		 *
		 * @param {class<Model>} ModelClass class of model to be tested
		 * @param {object} description describes test to compile
		 * @param {string} testType names type of test or its actual operation
		 * @param {string} sortBy name of property finally resulting list of matches will be sorted by (provided to help optimizing tester)
		 * @param {boolean} sortAscendingly true if provided property will be used eventually to sort in ascending order (provided to help optimizing tester)
		 * @param {boolean} loadRecords indicates whether caller intends to load matching items afterward or not (as a hint to possibly improve the processing)
		 * @returns {OdemModelTester} compiled tester
		 */
		static fromDescription( ModelClass, description, testType = null, { sortBy = null, sortAscendingly = true, loadRecords = false } = {} ) { // eslint-disable-line no-unused-vars
			if ( !description || typeof description !== "object" ) {
				throw new TypeError( "invalid test description" );
			}

			const operations = Object.keys( description );
			if ( operations.length !== 1 ) {
				throw new TypeError( "invalid test description with no or multiple operations" );
			}

			const operation = operations[0];

			const testerClass = Services.OdemModelTesterMap[operation];
			if ( !testerClass ) {
				throw new TypeError( `unknown test operation: ${operation}` );
			}

			logDebug( "using %s for picking matches of model %s", testerClass.name, ModelClass.name );

			const tester = testerClass.fromDescription( ModelClass, description[operation], operation, { sortBy, sortAscendingly, loadRecords } );
			if ( !( tester instanceof this ) ) {
				throw new TypeError( "invalid type of compiled tester" );
			}

			return tester;
		}

		/**
		 * Creates stream generating instances of provided model's class matching
		 * test.
		 *
		 * @return {ReadableStream<Model>} stream of provided model's instances
		 */
		createStream() {
			throw new Error( "abstract tester does not provide actual stream" );
		}
	}

	return OdemModelTester;
};
