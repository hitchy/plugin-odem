/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Implements code testing whether all tests in a given set are matching.
	 *
	 * @alias this.services.OdemModelTesterCombiningAnd
	 */
	class OdemModelTesterCombiningAnd extends Services.OdemModelTesterCombining {
		/**
		 * @param {class<Model>} ModelClass class of associated model
		 * @param {OdemModelTester[]} tests lists of tests to combine
		 */
		constructor( ModelClass, tests ) {
			super( ModelClass, tests, true );
		}
	}

	return OdemModelTesterCombiningAnd;
};
