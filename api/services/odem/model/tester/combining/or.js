/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Implements code testing whether one of the tests in a given set is
	 * matching.
	 *
	 * @alias this.services.OdemModelTesterCombiningOr
	 */
	class OdemModelTesterCombiningOr extends Services.OdemModelTesterCombining {
		/**
		 * @param {class<Model>} ModelClass class of associated model
		 * @param {OdemModelTester[]} tests lists of tests to combine
		 */
		constructor( ModelClass, tests ) {
			super( ModelClass, tests, false );
		}
	}

	return OdemModelTesterCombiningOr;
};
