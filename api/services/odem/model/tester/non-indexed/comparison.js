import { Transform } from "node:stream";

/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Implements legacy code testing whether some property per record is satisfying
	 * described operation or not.
	 *
	 * @alias this.services.OdemModelTesterNonIndexedComparison
	 */
	class OdemModelTesterNonIndexedComparison extends Services.OdemModelTester {
		/**
		 * @param {class<Model>} ModelClass class of associated model
		 * @param {string} operation name of operation to perform per record
		 * @param {string} propertyName name of property to test per record
		 * @param {string} value value to look for in named property
		 * @param {ModelType} typeHandler handler for defined type of property's values
		 * @param {object} propertyDefinition definition of property
		 * @param {boolean} loadRecords indicates whether caller intends to load matching items afterward or not (as a hint to possibly improve the processing)
		 */
		constructor( ModelClass, operation, propertyName, value, typeHandler, propertyDefinition, loadRecords ) {
			super();

			Object.defineProperties( this, {
				/**
				 * Exposes associated model's class.
				 *
				 * @name OdemModelTesterNonIndexedComparison#ModelClass
				 * @property {class<Model>}
				 * @readonly
				 */
				ModelClass: { value: ModelClass },

				/**
				 * Name of comparison operation to perform per record.
				 *
				 * @name OdemModelTesterNonIndexedComparison#operation
				 * @property {string}
				 * @readonly
				 */
				operation: { value: operation },

				/**
				 * Names property to test.
				 *
				 * @name OdemModelTesterNonIndexedComparison#propertyName
				 * @property {string}
				 * @readonly
				 */
				propertyName: { value: propertyName },

				/**
				 * Value to look for.
				 *
				 * @name OdemModelTesterNonIndexedComparison#value
				 * @property {*}
				 * @readonly
				 */
				value: { value: value },

				/**
				 * Handler for property's type of values.
				 *
				 * @name OdemModelTesterNonIndexedComparison#type
				 * @property {ModelType}
				 * @readonly
				 */
				type: { value: typeHandler },

				/**
				 * Definition of property in model's schema.
				 *
				 * @name OdemModelTesterNonIndexedComparison#definition
				 * @property {object}
				 * @readonly
				 */
				definition: { value: propertyDefinition },

				/**
				 * Exposes information on whether matching items are going to be
				 * fetched completely (so that this tester might prepare for
				 * that).
				 *
				 * @name OdemModelTesterNonIndexedComparison#loadRecords
				 * @property {boolean}
				 * @readonly
				 */
				loadRecords: { value: loadRecords },
			} );
		}

		/** @inheritDoc */
		static fromDescription( ModelClass, description, operation, { sortBy = null, sortAscendingly = true, loadRecords = false } = {} ) { // eslint-disable-line no-unused-vars
			const [ property, _ ] = Object.keys( description ) || [];
			const _description = property && _ == null ? { name: property, [operation === "in" ? "values" : "value"]: description[property] } : description;

			const name = _description.name || _description.property;
			if ( !name ) {
				throw new TypeError( "missing name of property to test" );
			}

			const propDefinition = ModelClass.schema.props[name];
			const computedDefinition = ModelClass.schema.computed[name];

			if ( !propDefinition && !computedDefinition ) {
				throw new TypeError( `no such property: ${name}` );
			}

			const definition = propDefinition || computedDefinition;
			let type;

			type = definition.$type;
			if ( !type ) {
				type = Services.OdemModelType;
			}

			if ( typeof type.compare !== "function" ) {
				throw new TypeError( `invalid type ${definition.type} of property ${name}` );
			}

			let value;

			if ( operation === "in" ) {
				if ( !Array.isArray( _description.values ) ) {
					throw new TypeError( "not a list of values to look for" );
				}

				value = _description.values;

				for ( let i = 0; i < value.length; i++ ) {
					value[i] = type.coerce( value[i], definition, ModelClass.prototype.$default );
				}
			} else {
				value = type.coerce( _description.value, definition, ModelClass.prototype.$default );
			}

			return new this( ModelClass, operation, name, value, type, definition, loadRecords );
		}

		/** @inheritDoc */
		createStream() {
			const { ModelClass, type, operation, propertyName, value, definition, loadRecords } = this;
			const { adapter } = ModelClass;
			const isComputed = !ModelClass.schema.props[propertyName];

			const stream = loadRecords ? ModelClass.instanceStream() : ModelClass.uuidStream();

			const converter = new Transform( {
				objectMode: true,
				transform( source, _, done ) {
					let promise;

					if ( loadRecords ) {
						promise = new ModelClass( source.uuid, { adapter } )
							._injectRecord( source.record );
					} else {
						promise = new ModelClass( source, { adapter } )
							.load();
					}

					promise
						.then( item => {
							const itemValue = type.coerce(
								isComputed ? item[propertyName] : item.$properties[propertyName],
								definition,
								ModelClass.prototype.$default
							);

							if ( operation === "in" ) {
								for ( let i = 0; i < value.length; i++ ) {
									if ( type.compare( itemValue, value[i], "eq" ) ) {
										this.push( item );
										break;
									}
								}
							} else if ( type.compare( itemValue, value, operation ) ) {
								this.push( item );
							}

							done();
						} )
						.catch( done );
				}
			} );

			converter.on( "close", () => {
				stream.unpipe( converter );
				stream.pause();
				stream.destroy();
			} );

			stream.pipe( converter );

			return converter;
		}
	}

	return OdemModelTesterNonIndexedComparison;
};
