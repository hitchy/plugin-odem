import { Readable, Transform } from "node:stream";

/** */
export default function() {
	const api = this;
	const { services: Services } = api;
	const logDebug = api.log( "hitchy:odem:debug" );

	/**
	 * Implements tester considering every record a match.
	 *
	 * @alias this.services.OdemModelTesterNonIndexedAll
	 */
	class OdemModelTesterNonIndexedAll extends Services.OdemModelTester {
		/**
		 * @param {class<Model>} ModelClass class of associated model
		 * @param {OdemModelIndexerEquality} index index to use for retrieving UUIDs
		 * @param {boolean} fetchAscendingly set true to fetch UUIDs from index in ascending order
		 * @param {boolean} loadRecords indicates whether caller intends to load matching items afterward or not (as a hint to possibly improve the processing)
		 */
		constructor( ModelClass, index = null, fetchAscendingly = true, loadRecords = false ) {
			super();

			Object.defineProperties( this, {
				/**
				 * Exposes associated model's class.
				 *
				 * @name OdemModelTesterNonIndexedAll#ModelClass
				 * @property {class<Model>}
				 * @readonly
				 */
				ModelClass: { value: ModelClass },

				/**
				 * Exposes optionally provided index instance.
				 *
				 * @name OdemModelTesterNonIndexedAll#index
				 * @property {OdemModelIndexerEquality}
				 * @readonly
				 */
				index: { value: index },

				/**
				 * Exposes information on whether fetching UUIDs from index in
				 * ascending order.
				 *
				 * @name OdemModelTesterNonIndexedAll#fetchAscendingly
				 * @property {boolean}
				 * @readonly
				 */
				fetchAscendingly: { value: fetchAscendingly },

				/**
				 * Exposes information on whether matching items are going to be
				 * fetched completely (so that this tester might prepare for
				 * that).
				 *
				 * @name OdemModelTesterNonIndexedAll#loadRecords
				 * @property {boolean}
				 * @readonly
				 */
				loadRecords: { value: loadRecords },
			} );
		}

		/** @inheritDoc */
		static fromDescription( ModelClass, description, testType, { sortBy = null, sortAscendingly = true, loadRecords = false } = {} ) {
			if ( ModelClass.indices.length > 0 ) {
				const index = ModelClass.getIndex( sortBy, "eq" );
				if ( index ) {
					logDebug( "fetching all using properly sorted index %s of %s.%s", index.constructor.name, ModelClass.name, sortBy );

					return new this( ModelClass, index, sortAscendingly );
				}

				logDebug( "fetching all using first available index %s of %s.%s",
					ModelClass.indices[0].handler.constructor.name, ModelClass.name, ModelClass.indices[0].property );

				return new this( ModelClass, ModelClass.indices[0].handler, true );
			}

			return new this( ModelClass, null, true, loadRecords );
		}

		/** @inheritDoc */
		createStream() {
			const { ModelClass, index, fetchAscendingly } = this;

			if ( index ) {
				const iter = index.findBetween( {
					descending: !fetchAscendingly,
					appendNullItems: true,
				} )();

				return new Readable( {
					objectMode: true,
					read() {
						const { done, value: uuid } = iter.next();

						logDebug( "retrieving %s match %s", done ? "final" : "next", uuid ? uuid.toString( "hex" ) : "<unset>" );

						if ( done ) {
							this.push( null );
						} else {
							this.push( new ModelClass( uuid ) );
						}
					}
				} );
			}

			const stream = this.loadRecords ? ModelClass.instanceStream() : ModelClass.uuidStream();
			const transform = this.loadRecords ? function( entry, _, done ) {
				const { uuid, record } = entry;

				const item = new ModelClass( uuid );
				item._injectRecord( record );

				this.push( item );

				done();
			} : function( uuid, _, done ) {
				this.push( new ModelClass( uuid ) );

				done();
			};

			const uuidToItem = new Transform( {
				objectMode: true,
				transform,
			} );

			uuidToItem.on( "close", () => {
				stream.unpipe( uuidToItem );
				stream.pause();
				stream.destroy();
			} );

			stream.pipe( uuidToItem );

			return uuidToItem;
		}
	}

	return OdemModelTesterNonIndexedAll;
};
