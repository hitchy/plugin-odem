import { Readable } from "node:stream";

/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Implements code testing whether multiple given tests match.
	 *
	 * @alias this.services.OdemModelTesterCombiningAnd
	 */
	class OdemModelTesterCombining extends Services.OdemModelTester {
		/**
		 * @param {class<Model>} ModelClass class of associated model
		 * @param {OdemModelTester[]} tests lists of tests to combine
		 * @param {boolean} all true if all tests need to match
		 */
		constructor( ModelClass, tests, all ) {
			super();

			Object.defineProperties( this, {
				/**
				 * Exposes associated model's class.
				 *
				 * @name OdemModelTesterCombining#ModelClass
				 * @property {class<Model>}
				 * @readonly
				 */
				ModelClass: { value: ModelClass },

				/**
				 * Lists tests to combine.
				 *
				 * @name OdemModelTesterIndexedEquality#tests
				 * @property {OdemModelTester[]}
				 * @readonly
				 */
				tests: { value: tests },

				/**
				 * Marks if all listed tests have to match for reporting a match
				 * or not.
				 *
				 * @name OdemModelTesterIndexedEquality#value
				 * @property {boolean}
				 * @readonly
				 */
				all: { value: Boolean( all ) },
			} );
		}

		/** @inheritDoc */
		static fromDescription( ModelClass, description, testType, options = {} ) {
			if ( !Array.isArray( description ) ) {
				throw new TypeError( "not a list of tests to combine" );
			}

			const tests = description.map( test => Services.OdemModelTester.fromDescription( ModelClass, test, null, options ) );

			if ( tests.length < 1 ) {
				throw new TypeError( "no test is listed for combining" );
			}

			return new this( ModelClass, tests );
		}

		/** @inheritDoc */
		createStream() {
			const matchCounts = {};
			const matches = {};
			const { tests, all } = this;

			return new Readable( {
				objectMode: true,
				read() {
					// have to consume all listed tests' streams first
					Promise.all( tests.map( test => new Promise( ( resolve, reject ) => {
						const stream = test.createStream();

						stream.once( "end", resolve );
						stream.once( "error", reject );

						stream.on( "data", item => {
							const uuid = item.uuid;

							if ( matchCounts[uuid] ) {
								matchCounts[uuid]++;
							} else {
								matchCounts[uuid] = 1;
								matches[uuid] = item; // collect for eventual use as it might have been prepared to include all its properties already
							}
						} );
					} ) ) )
						.then( () => {
							// provide instances matching at least one or all tests
							const numTests = tests.length;

							for ( const uuid of Object.keys( matchCounts ) ) {
								if ( !all || matchCounts[uuid] === numTests ) {
									this.push( matches[uuid] );
								}
							}

							this.push( null );
						} )
						.catch( cause => {
							this.destroy( cause );
						} );
				}
			} );
		}
	}

	return OdemModelTesterCombining;
};
