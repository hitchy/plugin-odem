import { Readable } from "node:stream";

/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Implements code testing whether some property is equal given value.
	 *
	 * @alias this.services.OdemModelTesterIndexedEquality
	 */
	class OdemModelTesterIndexedIn extends Services.OdemModelTester {
		/**
		 * @param {class<Model>} ModelClass class of associated model
		 * @param {iterator} handler index handler provided by the ModelClass
		 * @param {*[]} values list of values to look for in handled index
		 */
		constructor( ModelClass, handler, values ) {
			super();

			Object.defineProperties( this, {
				/**
				 * Exposes associated model's class.
				 *
				 * @name OdemModelTesterIndexedIn#ModelClass
				 * @property {class<Model>}
				 * @readonly
				 */
				ModelClass: { value: ModelClass },

				/**
				 * Index handler provided by the ModelClass
				 *
				 * @name OdemModelTesterIndexedIn#handler
				 * @property {iterator}
				 * @readonly
				 */
				handler: { value: handler },

				/**
				 * Lists values to look for.
				 *
				 * @name OdemModelTesterIndexedIn#values
				 * @property {*[]}
				 * @readonly
				 */
				values: { value: values },
			} );
		}

		/** @inheritDoc */
		static fromDescription( ModelClass, description, testType, options = {} ) {
			const [ property, _ ] = Object.keys( description ) || [];
			const _description = property && _ == null ? { name: property, values: description[property] } : description;

			const name = _description.name || _description.property;
			if ( !name ) {
				throw new TypeError( "missing name of property to test" );
			}

			const handler = ModelClass.getIndex( name, "eq" );
			if ( handler ) {
				const values = _description.values;

				if ( !Array.isArray( values ) ) {
					throw new TypeError( "not a list of values to look for" );
				}

				if ( typeof ModelClass._coercionHandlers[name] === "function" ) {
					for ( let i = 0; i < values.length; i++ ) {
						values[i] = ModelClass._coercionHandlers[name]( values[i] );
					}
				}

				return new this( ModelClass, handler, values );
			}

			return Services.OdemModelTesterNonIndexedComparison.fromDescription( ModelClass, _description, "in", options );
		}

		/** @inheritDoc */
		createStream() {
			const that = this;
			const { ModelClass, values } = this;
			let valueIndex = 0;
			let iterator = null;

			return new Readable( {
				objectMode: true,
				read() {
					let node;

					if ( iterator == null ) {
						// first invocation -> iterate over UUIDs of first value's index
						iterator = that.handler.find( values[valueIndex] )();
					}

					while ( true ) {
						node = iterator.next();
						if ( node.done ) {
							// met all UUIDs of current value's index

							if ( ++valueIndex >= values.length ) {
								// processed all values' indices -> done
								this.push( null );
								break;
							}

							// switch to next value's set of matching records' UUIDs
							iterator = that.handler.find( values[valueIndex] )();
							continue;
						}

						if ( !this.push( new ModelClass( node.value ) ) ) {
							// pause streaming for a moment
							break;
						}
					}
				}
			} );
		}
	}

	return OdemModelTesterIndexedIn;
};
