import { Readable } from "node:stream";

/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Implements code testing whether some property is equal given value.
	 *
	 * @alias this.services.OdemModelTesterIndexedNotNull
	 */
	class OdemModelTesterIndexedNotNull extends Services.OdemModelTester {
		/**
		 * @param {class<Model>} ModelClass class of associated model
		 * @param {iterator} handler index handler provided by the ModelClass
		 */
		constructor( ModelClass, handler ) {
			super();

			Object.defineProperties( this, {
				/**
				 * Exposes associated model's class.
				 *
				 * @name OdemModelTesterIndexedNotNull#ModelClass
				 * @property {class<Model>}
				 * @readonly
				 */
				ModelClass: { value: ModelClass },

				/**
				 * Index handler provided by the ModelClass
				 *
				 * @name OdemModelTesterIndexedNotNull#handler
				 * @property {iterator}
				 * @readonly
				 */
				handler: { value: handler },
			} );
		}

		/** @inheritDoc */
		static fromDescription( ModelClass, description, operation, options = {} ) {
			const _description = typeof description === "string" ? { name: description } : description;
			const name = _description.name || _description.property;
			if ( !name ) {
				throw new TypeError( "missing name of property to test" );
			}

			const handler = ModelClass.getIndex( name, "eq" );
			if ( handler ) {
				return new this( ModelClass, handler );
			}

			return Services.OdemModelTesterNonIndexedComparison.fromDescription( ModelClass, { name, value: undefined }, operation, options );
		}

		/**
		 * Provides iterator listing matches.
		 *
		 * @returns {*} generated iterator
		 */
		get iterator() {
			// FIXME teach findBetween to create stream to end iterating over output of some iterator
			return this.handler.findBetween()();
		}

		/** @inheritDoc */
		createStream() {
			const iterator = this.iterator;
			const { ModelClass } = this;

			return new Readable( {
				objectMode: true,
				read() {
					let iter;
					do {
						iter = iterator.next();
						if ( iter.done ) {
							this.push( null );
							break;
						}
					} while ( this.push( new ModelClass( iter.value ) ) );
				}
			} );
		}
	}

	return OdemModelTesterIndexedNotNull;
};
