import { Readable } from "node:stream";

/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Implements code testing whether some property is equal given value.
	 *
	 * @alias this.services.OdemModelTesterIndexedBetween
	 */
	class OdemModelTesterIndexedBetween extends Services.OdemModelTester {
		/**
		 * @param {class<Model>} ModelClass class of associated model
		 * @param {iterator} index index handler provided by the ModelClass
		 * @param {string} upper upper limit to look for in named property
		 * @param {string} lower lower limit to look for in named property
		 * @param {boolean} fetchAscendingly set true to fetch UUIDs from index in ascending order
		 */
		constructor( ModelClass, index, upper, lower, fetchAscendingly ) {
			super();

			Object.defineProperties( this, {
				/**
				 * Exposes associated model's class.
				 *
				 * @name OdemModelTesterIndexedBetween#ModelClass
				 * @property {class<Model>}
				 * @readonly
				 */
				ModelClass: { value: ModelClass },

				/**
				 * Index handler provided by the ModelClass
				 *
				 * @name OdemModelTesterIndexedBetween#index
				 * @property {EqualityIndex}
				 * @readonly
				 */
				index: { value: index },

				/**
				 * Upper limit for the tester
				 *
				 * @name OdemModelTesterIndexedBetween#value
				 * @property {*}
				 * @readonly
				 */
				upper: { value: upper },

				/**
				 * Lower limit for the tester
				 *
				 * @name OdemModelTesterIndexedBetween#value
				 * @property {*}
				 * @readonly
				 */
				lower: { value: lower },

				/**
				 * Indicates if UUIDs should be fetched from index in ascending
				 * order.
				 *
				 * @name OdemModelTesterIndexedBetween#fetchAscendingly
				 * @property {boolean}
				 * @readonly
				 */
				fetchAscendingly: { value: fetchAscendingly },
			} );
		}

		/** @inheritDoc */
		static fromDescription( ModelClass, description, testType = null, { sortBy = null, sortAscendingly = true, loadRecords = false } = {} ) { // eslint-disable-line no-unused-vars
			const [ property, _ ] = Object.keys( description ) || [];
			const _description = property && _ == null && Array.isArray( description[property] ) ?
				{ name: property, lower: description[property][0], upper: description[property][1] } : description;

			const name = _description.name || _description.property;
			if ( !name ) {
				throw new TypeError( "missing name of property to test" );
			}

			let { lower, upper } = _description;

			if ( typeof ModelClass._coercionHandlers[name] === "function" ) {
				lower = ModelClass._coercionHandlers[name]( lower );
				upper = ModelClass._coercionHandlers[name]( upper );
			}

			if ( upper == null || lower == null ) {
				throw new TypeError( "missing one or both boundaries for testing values in between" );
			}

			if ( ( typeof lower === "number" || lower instanceof Date ) && isNaN( lower ) ) {
				throw new TypeError( "lower boundary of range is invalid" );
			}

			if ( ( typeof upper === "number" || upper instanceof Date ) && isNaN( upper ) ) {
				throw new TypeError( "upper boundary of range is invalid" );
			}

			const index = ModelClass.getIndex( name, "eq" );
			if ( index ) {
				if ( name === sortBy ) {
					return new this( ModelClass, index, upper, lower, sortAscendingly );
				}

				return new this( ModelClass, index, upper, lower, true );
			}

			// FIXME support searching w/o index here similar to equality.js
			throw new Error( "matching items between two limits not yet supported for non-indexed properties" );
		}

		/** @inheritDoc */
		createStream() {
			// FIXME teach findBetween to create stream to end iterating over output of some iterator
			const iterator = this.index.findBetween( {
				lowerLimit: this.lower,
				upperLimit: this.upper,
				descending: !this.fetchAscendingly,
			} )();

			const { ModelClass } = this;

			return new Readable( {
				objectMode: true,
				read() {
					let iter;
					do {
						iter = iterator.next();
						if ( iter.done ) {
							this.push( null );
							break;
						}
					} while ( this.push( new ModelClass( iter.value ) ) );
				}
			} );
		}
	}

	return OdemModelTesterIndexedBetween;
};
