import { Readable } from "node:stream";

/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Implements code testing whether some property is equal given value.
	 *
	 * @alias this.services.OdemModelTesterIndexedEquality
	 */
	class OdemModelTesterIndexedEquality extends Services.OdemModelTester {
		/**
		 * @param {class<Hitchy.Plugin.Odem.Model>} ModelClass class of associated model
		 * @param {iterator} handler index handler provided by the ModelClass
		 * @param {*} value value to look for in handled index
		 */
		constructor( ModelClass, handler, value ) {
			super();

			Object.defineProperties( this, {
				/**
				 * Exposes associated model's class.
				 *
				 * @name OdemModelTesterIndexedEquality#ModelClass
				 * @property {class<Hitchy.Plugin.Odem.Model>}
				 * @readonly
				 */
				ModelClass: { value: ModelClass },

				/**
				 * Index handler provided by the ModelClass
				 *
				 * @name OdemModelTesterIndexedEquality#handler
				 * @property {iterator}
				 * @readonly
				 */
				handler: { value: handler },

				/**
				 * Value to look for.
				 *
				 * @name OdemModelTesterIndexedEquality#value
				 * @property {*}
				 * @readonly
				 */
				value: { value: value },
			} );
		}

		/** @inheritDoc */
		static fromDescription( ModelClass, description, testType, options = {} ) {
			const [ property, _ ] = Object.keys( description ) || [];
			const _description = property && _ == null ? { name: property, value: description[property] } : description;

			const name = _description.name || _description.property;
			if ( !name ) {
				throw new TypeError( "missing name of property to test" );
			}

			const handler = ModelClass.getIndex( name, "eq" );
			if ( handler ) {
				let value = _description.value;

				if ( typeof ModelClass._coercionHandlers[name] === "function" ) {
					value = ModelClass._coercionHandlers[name]( value );
				}

				return new this( ModelClass, handler, value );
			}

			return Services.OdemModelTesterNonIndexedComparison.fromDescription( ModelClass, _description, testType, options );
		}

		/** @inheritDoc */
		createStream() {
			const iterator = this.handler.find( this.value )();
			const { ModelClass } = this;

			return new Readable( {
				objectMode: true,
				read() {
					let node;

					do {
						node = iterator.next();
						if ( node.done ) {
							this.push( null );
							break;
						}
					} while ( this.push( new ModelClass( node.value ) ) );
				}
			} );
		}
	}

	return OdemModelTesterIndexedEquality;
};
