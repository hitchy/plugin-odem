/** */
export default function() {
	const api = this;
	const { services: Services } = api;

	/**
	 * Maps operator names used in query filters into classes implementing test
	 * for picking records considered matches accordingly.
	 */
	class OdemModelTesterMap {
		/* eslint-disable jsdoc/require-jsdoc */
		static get true() { return Services.OdemModelTesterNonIndexedAll; }

		static get eq() { return Services.OdemModelTesterIndexedEquality; }

		static get in() { return Services.OdemModelTesterIndexedIn; }

		static get neq() { return Services.OdemModelTesterNonIndexedComparison; }

		static get lt() { return Services.OdemModelTesterNonIndexedComparison; }

		static get gt() { return Services.OdemModelTesterNonIndexedComparison; }

		static get lte() { return Services.OdemModelTesterNonIndexedComparison; }

		static get gte() { return Services.OdemModelTesterNonIndexedComparison; }

		static get null() { return Services.OdemModelTesterIndexedNull; }

		static get notnull() { return Services.OdemModelTesterIndexedNotNull; }

		static get between() { return Services.OdemModelTesterIndexedBetween; }

		static get and() { return Services.OdemModelTesterCombiningAnd; }

		static get or() { return Services.OdemModelTesterCombiningOr; }

		static get every() { return Services.OdemModelTesterCombiningAnd; }

		static get some() { return Services.OdemModelTesterCombiningOr; }
		/* eslint-enable jsdoc/require-jsdoc */
	}

	return OdemModelTesterMap;
};
