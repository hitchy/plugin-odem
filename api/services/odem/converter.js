/** */
export default function() {
	const api = this;

	/**
	 * Discovers simplified definitions of models and compiles them into usable
	 * model classes.
	 */
	class OdemConverter {
		/**
		 * Processes discovered definitions of models.
		 *
		 * @param {object<string,object>} models raw definitions of models to be converted
		 * @param {OdemAdapter} adapter provides default adapter to use for either defined model
		 * @return {object} provided object with contained definitions replaced with according implementations
		 */
		static processModelDefinitions( models, adapter ) {
			const { services: Services } = api;

			if ( !models || typeof models !== "object" ) {
				throw new TypeError( "missing or invalid map of model definitions" );
			}

			if ( !adapter || !( adapter instanceof Services.OdemAdapter ) ) {
				throw new TypeError( "missing or invalid adapter" );
			}


			// prepare data to detect either models' weight on being dependant of other models
			const tree = {};
			const { OdemUtilityString } = Services;

			for ( let names = Object.keys( models ), i = 0, numNames = names.length; i < numNames; i++ ) {
				const name = names[i];
				const definition = models[name] || {};

				const modelName = OdemUtilityString.autoKebabToPascal( definition.name || name );
				const parentName = Object.prototype.hasOwnProperty.call( definition, "parent" ) ?
					OdemUtilityString.autoKebabToPascal( definition.parent ) : null;

				tree[modelName] = {
					weight: 0,
					raw: name,
					parent: parentName,
				};
			}


			// traverse either model's sequence of parent models increasing weight on
			// either encountered dependency for defining some model
			const modelNames = Object.keys( tree );
			const numModels = modelNames.length;

			for ( let i = 0; i < numModels; i++ ) {
				const name = modelNames[i];
				let node = tree[name];

				for ( let parentName = node.parent; parentName; parentName = node.parent ) {
					node = tree[parentName];
					if ( node ) {
						node.weight++;
					} else {
						break;
					}
				}
			}


			// sort names of models according to found dependency weights per model
			modelNames.sort( ( l, r ) => tree[r].weight - tree[l].weight );


			// eventually process definitions model by model in a dependency-based order
			for ( let i = 0; i < numModels; i++ ) {
				const name = modelNames[i];
				const { raw, parent } = tree[name];
				const definition = models[raw] || {};

				if ( definition.constructor === Object ) {
					if ( parent && !tree.hasOwnProperty( parent ) ) {
						throw new TypeError( `invalid reference on parent model ${parent} in context of model ${raw}` );
					}

					try {
						models[raw] = Services.Model.define( name, definition, parent ? models[tree[parent].raw] : null, adapter );
					} catch ( error ) {
						throw new TypeError( `definition of model ${name} failed: ${error.message}` );
					}
				}
			}


			return models;
		}
	}

	return OdemConverter;
};
