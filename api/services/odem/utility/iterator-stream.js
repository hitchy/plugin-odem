import { Readable } from "node:stream";

/** */
export default function() {
	/**
	 * Implements readable stream fed from an iterator.
	 */
	class OdemUtilityIteratorStream extends Readable {
		/**
		 * @param {Iterator} iterator iterator generating data to be streamed
		 * @param {object} options options applied to underlying stream
		 */
		constructor( iterator, options = {} ) {
			if ( !iterator || typeof iterator !== "object" || typeof iterator.next !== "function" ) {
				throw new TypeError( "invalid iterator" );
			}

			const _options = Object.assign( {}, options, {
				objectMode: true,
			} );

			super( _options );

			if ( _options.feeder ) {
				if ( typeof _options.feeder !== "function" ) {
					throw new TypeError( "invalid feeder function" );
				}

				this._read = _options.feeder;
			}

			Object.defineProperties( this, {
				/**
				 * @name OdemUtilityIteratorStream#iterator
				 * @property {Iterator}
				 * @readonly
				 */
				iterator: { value: iterator },

				/**
				 * @name OdemUtilityIteratorStream#options
				 * @property {object}
				 * @readonly
				 */
				options: { value: _options },
			} );
		}

		/** @inheritDoc */
		_read() {
			let writable = true;
			let item = {};

			while ( writable && !item.done ) {
				item = this.iterator.next();

				writable = this.push( item.done ? null : item.value );
			}
		}
	}

	return OdemUtilityIteratorStream;
};
