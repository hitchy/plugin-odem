/** */
export default function() {
	/**
	 * Implements date-related utilities.
	 */
	class OdemUtilityDate {
	}

	Object.defineProperties( OdemUtilityDate, {
		/**
		 * Matches any string representing date in ISO-8601 form.
		 *
		 * @type {RegExp}
		 * @readonly
		 */
		ptnISO8601: {
			value: /^\s*(?:\d\d\d\d-\d\d?-\d\d?)(?:T\d\d:\d\d:\d\d(?:[.,]\d+)?)?(?:Z|[+-]\d\d?:\d\d)?\s*$/i,
			enumerable: true,
		},
	} );

	/*
	 * Expose patterns used in re-compiled methods of model attribute type handlers
	 * running w/o their current closure scope in recompiled use cases.
	 */
	global.hitchyPtnISO8601 = OdemUtilityDate.ptnISO8601;

	return OdemUtilityDate;
};

