const ptnCamel = /(\S)([A-Z])/g;
const ptnSnake = /(\S)_+(\S)/g;
const ptnKebab = /(\S)-+(\S)/g;


/** */
export default function() {
	/**
	 * Implements string-related utility functions.
	 */
	class OdemUtilityString {
		/**
		 * Converts string from camelCase to snake_case.
		 *
		 * @param {string} string string to convert
		 * @returns {string} converted string
		 */
		static camelToSnake( string ) {
			return string.replace( ptnCamel, ( all, predecessor, match ) => predecessor + "_" + match.toLocaleLowerCase() );
		}

		/**
		 * Converts string from camelCase to kebab-case.
		 *
		 * @param {string} string string to convert
		 * @returns {string} converted string
		 */
		static camelToKebab( string ) {
			return string.replace( ptnCamel, ( all, predecessor, match ) => predecessor + "-" + match.toLocaleLowerCase() );
		}

		/**
		 * Converts string from snake_case to camelCase.
		 *
		 * @param {string} string string to convert
		 * @returns {string} converted string
		 */
		static snakeToCamel( string ) {
			return string.replace( ptnSnake, ( all, predecessor, match ) => predecessor + match.toLocaleUpperCase() );
		}

		/**
		 * Converts string from snake_case to kebab-case.
		 *
		 * @param {string} string string to convert
		 * @returns {string} converted string
		 */
		static snakeToKebab( string ) {
			return string.replace( ptnSnake, ( all, predecessor, match ) => predecessor + "-" + match );
		}

		/**
		 * Converts string from kebab-case to camelCase.
		 *
		 * @param {string} string string to convert
		 * @returns {string} converted string
		 */
		static kebabToCamel( string ) {
			return string.replace( ptnKebab, ( all, predecessor, match ) => predecessor + match.toLocaleUpperCase() );
		}

		/**
		 * Converts string from kebab-case to PascalCase.
		 *
		 * @param {string} string string to convert
		 * @returns {string} converted string
		 */
		static kebabToPascal( string ) {
			const camel = this.kebabToCamel( string );

			return camel.slice( 0, 1 ).toUpperCase() + camel.slice( 1 );
		}

		/**
		 * Converts string from kebab-case to snake_case.
		 *
		 * @param {string} string string to convert
		 * @returns {string} converted string
		 */
		static kebabToSnake( string ) {
			return string.replace( ptnKebab, ( all, predecessor, match ) => predecessor + "_" + match );
		}

		/**
		 * Converts string from kebab-case to PascalCase ignoring uppercase letters in
		 * provided string unless it looks like PascalCase string already.
		 *
		 * @param {string} string string to convert, uppercase letters are lost unless it's considered PascalCase already
		 * @returns {string} converted string
		 */
		static autoKebabToPascal( string ) {
			if ( ( parseFloat( process.versions.node ) >= 10.3 ? /^\s*(\p{Lu}\p{Ll}*)+\s*$/u : /^\s*([A-Z][a-z]*)+\s*$/ ).test( string ) ) {
				return string;
			}

			const camel = this.kebabToCamel( String( string ).toLocaleLowerCase() );

			return camel.slice( 0, 1 ).toUpperCase() + camel.slice( 1 );
		}
	}

	// keep supporting previous version of API using names with German spelling
	Object.defineProperties( OdemUtilityString, {
		camelToKebap: { value: OdemUtilityString.camelToKebab },
		snakeToKebap: { value: OdemUtilityString.snakeToKebab },
		kebapToCamel: { value: OdemUtilityString.kebabToCamel },
		kebapToPascal: { value: OdemUtilityString.kebabToPascal },
		kebapToSnake: { value: OdemUtilityString.kebabToSnake },
	} );

	Object.defineProperties( OdemUtilityString, {
		/**
		 * Detects if some string contains valid keyword or not.
		 *
		 * @name OdemUtilityString.ptnKeyword
		 * @property {RegExp}
		 * @readonly
		 */
		ptnKeyword: {
			value: /^[a-z][a-z0-9_]*$/i,
			enumerable: true,
		},
	} );

	global.hitchyPtnKeyword = OdemUtilityString.ptnKeyword;

	return OdemUtilityString;
};

