/** */
export default function() {
	/**
	 * Implements utilities for processing numbers.
	 *
	 * @alias this.services.OdemUtilityNumber
	 */
	class OdemUtilityNumber {}

	Object.defineProperties( OdemUtilityNumber, {
		/**
		 * Matches any string representing number.
		 *
		 * @name OdemUtilityNumber.ptnFloat
		 * @property {RegExp}
		 * @readonly
		 */
		ptnFloat: {
			value: /^\s*[+-]?\d+(?:\.\d+)?(?:e[+-]?\d+)?\s*$/i,
			enumerable: true,
		},

		/**
		 * Matches any string representing integer.
		 *
		 * @name OdemUtilityNumber.ptnFloat
		 * @property {RegExp}
		 * @readonly
		 */
		ptnInteger: {
			value: /^\s*[+-]?\d+\*$/,
			enumerable: true,
		},
	} );

	/*
	 * Expose patterns used in re-compiled methods of model attribute type handlers
	 * running w/o their current closure scope in recompiled use cases.
	 */
	global.hitchyPtnFloat = OdemUtilityNumber.ptnFloat;
	global.hitchyPtnInteger = OdemUtilityNumber.ptnInteger;

	return OdemUtilityNumber;
};

