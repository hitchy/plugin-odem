/** */
export default function() {
	/**
	 * Implements boolean-related utility functions.
	 *
	 * @alias this.services.OdemUtilityBoolean
	 */
	class OdemUtilityBoolean {}

	Object.defineProperties( OdemUtilityBoolean, {
		/**
		 * Matches any string representing boolean true.
		 *
		 * @name OdemUtilityBoolean.ptnTrue
		 * @property {RegExp}
		 * @readonly
		 */
		ptnTrue: {
			value: /^(?:y(?:es)?|ja?|on|hi(?:gh)?|true|t|set|x)$/i,
			enumerable: true,
		},

		/**
		 * Matches any string representing boolean false.
		 *
		 * @name OdemUtilityBoolean.ptnFalse
		 * @property {RegExp}
		 * @readonly
		 */
		ptnFalse: {
			value: /^(?:n(?:o|ein)?|off|low?|false|f|cl(?:ea)?r|-)$/i,
			enumerable: true,
		},
	} );

	/*
	 * Expose patterns used in re-compiled methods of model attribute type
	 * handlers running w/o their current closure scope in recompiled use cases.
	 */
	global.hitchyPtnTrue = OdemUtilityBoolean.ptnTrue;
	global.hitchyPtnFalse = OdemUtilityBoolean.ptnFalse;

	return OdemUtilityBoolean;
};

