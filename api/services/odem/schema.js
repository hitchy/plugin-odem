const PtnAcceptsValue = /^\s*(?:function\s*(?:\s\S+\s*)?)?\(\s*[^\s)]/;

/** */
export default function() {
	const api = this;
	const { service } = api;

	/**
	 * Implements schema-related helper functions.
	 *
	 * @name api.services.OdemSchema
	 */
	class OdemSchema {
		/**
		 * Detects if selected model's schema may be promoted to clients or not.
		 *
		 * @param {HitchyIncomingMessage} request request descriptor
		 * @param {class<Model>} model class of model to check
		 * @returns {boolean} true if model's schema may be promoted to clients, false otherwise
		 */
		static mayBePromoted( request, model ) {
			const { schema: { options } } = model;
			const scope = String( options.promote || options.expose || "" ).trim().toLowerCase() || "public";

			return this.mayAccessScope( scope, `@hitchy.odem.model.${model.name}.promote`, request.user );
		}

		/**
		 * Detects if selected model may be exposed to clients or not.
		 *
		 * @param {HitchyIncomingMessage} request request descriptor
		 * @param {class<Model>} model class of model to check
		 * @returns {boolean} true if model may be exposed to clients, false otherwise
		 */
		static mayBeExposed( request, model ) {
			const scope = String( model.schema.options.expose ?? "" ).trim().toLowerCase() || "public";

			return this.mayAccessScope( scope, `@hitchy.odem.model.${model.name}.expose`, request.user );
		}

		/**
		 * Inspects provided user information for describing a user with
		 * administrator's role.
		 *
		 * @param {object} user user information
		 * @returns {boolean} true if user information includes list of roles containing administrators' role
		 */
		static isAdmin( user ) {
			if ( !api.plugins.authentication ) {
				return false;
			}

			const { adminRole } = service.AuthManager;

			if ( !adminRole || !Array.isArray( user?.roles ) ) {
				return false;
			}

			for ( const role of user.roles ) {
				if ( role?.name === adminRole ) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Creates copy of provided serialized record of an item's properties
		 * excluding those properties that may not be exposed on reading or
		 * changed on updating based on provided model's schema.
		 *
		 * @param {Object<string,string|number|boolean|null>} itemRecord serialized record of an item
		 * @param {Hitchy.Core.IncomingMessage} request request descriptor
		 * @param {class<Model>} model class of model provided item is related to
		 * @param {'read'|'write'|'list'|'create'} mode kind of request in which the item is processed
		 * @returns {Object<string,string|number|boolean|null>} copy of provided record optionally missing some of its properties
		 */
		static filterItem( itemRecord, request, model, mode ) {
			const user = request.user;
			const copy = {};

			for ( const [ key, value ] of Object.entries( itemRecord ) ) {
				if ( this.mayAccessProperty( key, user, model, mode ) ) {
					copy[key] = value;
				}
			}

			return copy;
		}

		/**
		 * Decides whether provided scope grant access to selected user.
		 *
		 * @param {'public'|'protected'|'private'} scope scope to test
		 * @param {string|undefined} resource name of resource to check user's authorization in case scope is "protected" and @hitchy/plugin-auth is available
		 * @param {User} user requesting user
		 * @returns {boolean} true if user is granted access on resource with given scope (true if public, false if private, authorization-based if protected)
		 */
		static mayAccessScope( scope, resource, user ) {
			switch ( scope ) {
				case "public" :
					return true;

				case "protected" :
					if ( this.isAdmin( user ) ) {
						return true;
					}

					if ( api.plugins.authentication && typeof resource === "string" ) {
						return service.AuthorizationTree.current.isAuthorized(
							resource,
							user?.name || undefined,
							user?.roles ? user.roles.map( role => role.name ) : undefined,
						);
					}

					return Boolean( user );

				default :
					return false;
			}
		}

		/**
		 * Checks if provided user may access named property in selected mode.
		 *
		 * @note This method does not consider the given model's scope. It is
		 *       meant to be used in combination with mayBeExposed() for that.
		 *
		 * @param {string} propertyName name of property to check
		 * @param {User} user description of user trying to access named property
		 * @param {class<Model>} model class of model containing property to test
		 * @param {'read'|'write'|'list'|'create'} mode kind of request in which the item is processed
		 * @returns {boolean} true if user may access named property, false otherwise
		 */
		static mayAccessProperty( propertyName, user, model, mode ) {
			const definition = model.schema.props[propertyName];

			if ( !definition ) {
				return propertyName != null;
			}

			if ( definition.private ) {
				return false;
			}

			if ( definition.readonly && mode === "write" ) {
				return false;
			}

			if ( definition.protected ) {
				if ( !api.plugins.authentication ) {
					// plugin-auth or similar is missing
					// -> mock previous behavior: any authenticated user can access protected properties
					return Boolean( user );
				}

				if ( this.isAdmin( user ) ) {
					return true;
				}

				let authorization = definition.protected;

				if ( typeof authorization !== "string" ) {
					// use default authorization
					authorization = `@hitchy.odem.model.${model.name}.property.${propertyName}.${mode || "read"}`;
				}

				return service.AuthorizationTree.current.isAuthorized(
					authorization,
					user?.name || undefined,
					user?.roles ? user.roles.map( role => role.name ) : undefined,
				);
			}

			return true;
		}

		/**
		 * Provides given query for finding matching items unless it is
		 * apparently malformed or contains properties the given request's user
		 * lacks permissions to access in which case an exception is thrown.
		 *
		 * @param {Hitchy.Odem.Query} query term to be satisfied by items
		 * @param {Hitchy.Core.IncomingMessage} request request descriptor
		 * @param {class<Model>} model class of model provided item is related to
		 * @returns {Hitchy.Odem.Query} provided query when not including any forbidden property
		 * @throws TypeError with proper HTTP status code attached if provided filter is either malformed or addresses some forbidden property
		 */
		static checkQuery( query, request, model ) {
			if ( !query || typeof query !== "object" ) {
				throw Object.assign( new TypeError( `invalid query term: ${query}` ), { code: 400 } );
			}

			const [ operation, ...extra ] = Object.keys( query );
			if ( !operation || extra.length > 0 ) {
				throw Object.assign( new TypeError( `invalid query term: ${query}` ), { code: 403 } );
			}

			const term = query[operation];

			switch ( operation ) {
				case "true" :
					break;

				case "and" :
				case "or" :
					for ( const sub of term ) {
						this.checkQuery( sub, request, model );
					}
					break;

				default : {
					const keys = Object.keys( term );
					const name = keys.length === 1 ? keys[0] : term.name;

					if ( !this.mayAccessProperty( name, request.user, model, "list" ) ) {
						throw Object.assign( new TypeError( `invalid attempt to filter by forbidden property ${name}` ), { code: 403 } );
					}
				}
			}

			return query;
		}

		/**
		 * Extracts information on selected model's schema for publishing.
		 *
		 * @param {class<Model>} model model to process
		 * @param {boolean} omitComputed set true to omit computed properties
		 * @returns {object} extracted public information on selected model's schema
		 */
		static extractPublicData( model, { omitComputed = false } = {} ) {
			const { schema: { props, computed, options } } = model;

			const extracted = {
				name: model.name,
				props: {},
			};

			const propNames = Object.keys( props );
			const numProps = propNames.length;

			for ( let i = 0; i < numProps; i++ ) {
				const name = propNames[i];
				const prop = props[name];

				const copy = extracted.props[name] = {
					type: prop.type || "string",
				};

				const optionNames = Object.keys( prop );
				const numOptions = optionNames.length;

				for ( let j = 0; j < numOptions; j++ ) {
					const optionName = optionNames[j];

					switch ( optionName.toLowerCase() ) {
						case "type" :
						case "indexes" :
						case "indices" :
						case "index" :
							break;

						default : {
							const option = prop[optionName];

							if ( option != null && typeof option !== "function" ) {
								copy[optionName] = this.cloneSerializable( option );
							}
						}
					}
				}
			}

			if ( !omitComputed ) {
				const computedNames = Object.keys( props );
				const numComputed = computedNames.length;
				extracted.computed = {};

				for ( let i = 0; i < numComputed; i++ ) {
					const name = computedNames[i];
					const fn = computed[name];

					if ( typeof fn === "function" ) {
						extracted.computed[name] = PtnAcceptsValue.test( fn );
					}
				}
			}

			const optionsNames = Object.keys( options );
			const numOptions = optionsNames.length;

			for ( let i = 0; i < numOptions; i++ ) {
				const name = optionsNames[i];

				switch ( name ) {
					case "onUnsaved" :
					case "expose" :
					case "promote" :
						break;

					default : {
						if ( !extracted.options ) {
							extracted.options = {};
						}

						const option = options[name];

						if ( option != null && typeof option !== "function" ) {
							extracted.options[name] = this.cloneSerializable( option );
						}
					}
				}
			}

			return extracted;
		}

		/**
		 * Creates deep clone of provided value omitting any data that can not or
		 * must not be serialized.
		 *
		 * @param {*} input data to be cloned
		 * @returns {*} (partial) clone of provided data
		 */
		static cloneSerializable( input ) {
			switch ( typeof input ) {
				case "object" : {
					if ( Array.isArray( input ) ) {
						const numItems = input.length;
						const clone = new Array( numItems );
						let write = 0;

						for ( let read = 0; read < numItems; read++ ) {
							const value = this.cloneSerializable( input[read] );

							if ( value != null ) {
								clone[write++] = value;
							}
						}

						clone.splice( write );

						return clone;
					}

					if ( !input ) {
						return input;
					}

					if ( input.constructor && input.constructor !== Object ) {
						return null;
					}

					const names = Object.keys( input );
					const numNames = names.length;
					const clone = {};

					for ( let i = 0; i < numNames; i++ ) {
						const name = names[i];
						const value = this.cloneSerializable( input[name] );

						if ( value != null ) {
							clone[name] = value;
						}
					}

					return clone;
				}

				case "function" :
					return null;

				default :
					return input;
			}
		}
	}

	return OdemSchema;
};
