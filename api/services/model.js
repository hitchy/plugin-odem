import { env } from "node:process";
import { Readable, Transform } from "node:stream";
import EventEmitter from "node:events";

import PromiseUtils from "promise-essentials";
import Monitor from "object-monitor";

/**
 * @typedef {object} ModelProperties
 * @property {{changed: Set<string>}} $context
 */

/**
 * @typedef {object} ModelSchema
 */

const PtnModelItemsKey = /^models\/([^/]+)\/items\/([^/]+)(?:\/(\S+))?$/;


/** */
export default function() {
	const api = this;
	const { config, services: Services } = api;

	const logDebug = api.log( "hitchy:odem:debug" );
	const logError = api.log( "hitchy:odem:error" );
	const logAlert = api.log( "hitchy:odem:alert" );

	/**
	 * Implements basic behaviour of a model.
	 *
	 * @alias Model
	 * @alias this.services.Model
	 */
	class Model {
		/**
		 * @param {?(string|Buffer)} itemUUID UUID of model item to be managed by instance, omit for starting new item
		 * @param {boolean|string} onUnsaved set true to omit model logging to stderr on replacing changed property w/o saving first
		 */
		constructor( itemUUID = null, { onUnsaved = null } = {} ) {
			const args = this.beforeCreate( { uuid: itemUUID, options: { onUnsaved } } ) || {};

			const { onUnsaved: __onUnsaved } = args.options || {};
			const _onUnsaved = __onUnsaved == null ? this.constructor.onUnsaved : __onUnsaved;

			let _uuid = null;

			Object.defineProperties( this, {
				/**
				 * Uniquely identifies current instance of model.
				 *
				 * @note UUID can be written only once unless it has been given
				 *       initially for loading some matching instance from storage.
				 *
				 * @note For compatibility reasons this property is always provided
				 *       as string when reading though internal processing of UUIDs
				 *       relies on binary format now to reduce memory consumption.
				 *       The binary format used internally can be read using
				 *       @see Model#$uuid.
				 *
				 * @name Model#uuid
				 * @property {?(string|Buffer)}
				 */
				uuid: {
					get: () => {
						if ( _uuid == null ) {
							return null;
						}

						Object.defineProperties( this, {
							uuid: { value: Services.OdemUtilityUuid.format( _uuid ), }
						} );

						return this.uuid;
					},
					set: newUUID => {
						if ( newUUID != null ) {
							if ( _uuid != null ) {
								throw new Error( "changing UUID of model instance rejected" );
							}

							_uuid = Services.OdemUtilityUuid.normalize( newUUID );
						}
					},
					configurable: true,
				},

				/**
				 * Uniquely identifies current instance of model.
				 *
				 * @note UUID can be set via @see Model#uuid, only.
				 *
				 * @name Model#$uuid
				 * @property {?Buffer}
				 * @readonly
				 */
				$uuid: {
					get: () => _uuid,
				},
			} );

			this.uuid = args.uuid;


			const props = this.constructor.schema.props;
			const propNames = Object.keys( props );

			const customCompare = ( old, next, name ) => {
				const property = props[name];
				const type = property ? property.$type : undefined;

				return type ? type.compare( next, old, "eq" ) : old === next;
			};

			/**
			 * @type {ModelProperties}
			 */
			let data = Monitor( {}, {
				warn: _onUnsaved === "warn",
				fail: _onUnsaved === "fail",
				coercion: this.constructor._coercionHandlers,
				customCompare,
			} );

			// create initial set of properties from either actual property
			for ( let i = 0, l = propNames.length; i < l; i++ ) {
				const name = propNames[i];
				const defaultValue = props[name].default;

				if ( defaultValue !== undefined ) {
					data[name] = props[name].default;
				}
			}

			data.$context.commit();


			let isLoading = null;
			let markLoaded = false;
			let notifications = null;

			Object.defineProperties( this, {
				/**
				 * Promises previously triggered request for loading properties of
				 * current item to have succeeded or failed. This promise is set
				 * as soon as request for loading properties has been triggered.
				 *
				 * @see Model#$isMarkedLoaded for different indicator suitable for
				 * detecting synchronously if properties have been loaded before.
				 *
				 * @name Model#$loaded
				 * @property {?Promise<Model>}
				 * @readonly
				 */
				$loaded: {
					get: () => isLoading,
					set: promise => {
						if ( isLoading ) {
							throw new Error( "must not promise loading multiple times" );
						}

						if ( !( promise instanceof Promise ) ) {
							throw new Error( "not a promise" );
						}

						isLoading = promise
							.then( record => {
								markLoaded = true;

								if ( _uuid != null ) {
									if ( !record || typeof record !== "object" ) {
										throw new TypeError( "invalid set of properties" );
									}

									if ( data.$context.hasChanged && !data.$context.relaxed ) {
										switch ( _onUnsaved ) {
											case "ignore" :
												break;

											case "warn" :

												logError( "WARNING: replacing an item's properties after changing some w/o saving" );
												break;

											case "fail" :
											default :
												throw new Error( "WARNING: replacing an item's properties after changing some w/o saving" );
										}
									}

									const constructor = this.constructor;
									const { _deserializeProperties, schema } = constructor;
									const deserialized = typeof _deserializeProperties === "function" ? _deserializeProperties( record, schema.props ) : record;

									data = Monitor( deserialized, {
										warn: _onUnsaved === "warn",
										fail: _onUnsaved === "fail",
										coercion: this.constructor._coercionHandlers,
										customCompare,
									} );
								}

								return this;
							} );
					}
				},

				/**
				 * Synchronously indicates if current instance's properties have
				 * been loaded before or not.
				 *
				 * @name Model#$isMarkedLoaded
				 * @property {boolean}
				 * @readonly
				 */
				$isMarkedLoaded: { get: () => markLoaded },

				/**
				 * Marks if current model instance is new (thus still lacking UUID).
				 *
				 * @name Model#$isNew
				 * @property {boolean}
				 * @readonly
				 */
				$isNew: { get: () => _uuid == null },

				/**
				 * Fetches data key of current model usually to be used with some
				 * KV-based storage.
				 *
				 * @name Model#$dataKey
				 * @property {string}
				 * @readonly
				 */
				$dataKey: {
					value: this.constructor.uuidToKey( _uuid ),
					configurable: _uuid == null,
				},

				/**
				 * Provides properties of current instance of model.
				 *
				 * @name Model#$properties
				 * @property {ModelProperties}
				 * @readonly
				 */
				$properties: { get: () => data },

				/**
				 * Exposes per-instance notifications emitter.
				 *
				 * The emitter is created on demand, only.
				 *
				 * @name Model#$notifications
				 * @property {EventEmitter}
				 * @readonly
				 */
				$notifications: {
					get: () => {
						if ( _uuid == null ) {
							throw new TypeError( "notifications work on model instances existing in backend, only" );
						}

						const instanceEmitter = new EventEmitter();

						notifications = {
							emitter: instanceEmitter,
							changed: null,
							removed: null,
						};

						// replace current getter function with faster delivery
						// of created emitter
						Object.defineProperty( this, "$notifications", {
							value: instanceEmitter,
						} );

						// monitor listener registration to start listening for
						// notifications to be forwarded on demand, only
						const onNewListener = name => {
							switch ( name ) {
								case "changed" :
								case "removed" :
									if ( !notifications[name] ) {
										// got first listener for named notification
										// -> start actually listening for that notification
										const onPerModelNotification = async( uuid, ...payload ) => {
											if ( _uuid.equals( uuid ) ) {
												if ( name === "changed" ) {
													// remove callback for generating another instance from payload
													payload.splice( 2, 1 );

													if ( !data.$context.hasChanged ) {
														// fake re-loading record from backend to update local properties
														if ( isLoading ) {
															await isLoading.catch( logError );
														}

														isLoading = null;

														this.$loaded = Promise.resolve( payload[0] );

														// wait for the previous assignment to complete
														await this.$loaded.catch( logError );
													}
												}

												// forward notification
												instanceEmitter.emit( name, ...payload );

												if ( name === "removed" ) {
													// backend is notifying on removal of model instance
													// -> do not expect any further notifications for that instance
													instanceEmitter.off();
												}
											}
										};

										this.constructor.notifications.on( name, onPerModelNotification );

										notifications[name] = onPerModelNotification;
									}
							}
						};

						// adapt on listener de-registration to prevent memory leaks
						const onRemoveListener = name => {
							switch ( name ) {
								case "changed" :
								case "removed" :
									if ( notifications[name] && instanceEmitter.listenerCount( name ) === 1 ) {
										// going to remove last listener for named notification
										// -> stop actually listening for that notification, too
										this.constructor.notifications.off( name, notifications[name] );

										notifications[name] = null;
									}
							}
						};

						instanceEmitter.on( "newListener", onNewListener );
						instanceEmitter.on( "removeListener", onRemoveListener );

						return instanceEmitter;
					},
					configurable: true,
				},
			} );



			if ( _uuid == null ) {
				this.$loaded = Promise.resolve();
			}

			this.$properties.$context.relax();
			this.afterCreate();
			this.$properties.$context.relax( false );
		}

		/**
		 * Represents any default value.
		 *
		 * @returns {*} some opaque value used to indicate "default value"
		 */
		get $default() {
			return Object.freeze( {} );
		}

		/**
		 * Fetches defined name of current model.
		 *
		 * @returns {string} defined name of model
		 */
		static get name() {
			return "$$OdemModel$$";
		}

		/**
		 * Exposes default mode for handling multiple value assignments to a
		 * property without saving intermittently.
		 *
		 * @returns {string} default value of model-related option onUnsaved
		 */
		static get onUnsaved() { return "fail"; }

		/**
		 * Lists  definitions of indices extracted from properties of current model.
		 *
		 * @returns {array} list of indices' definitions
		 * @readonly
		 */
		static get indices() {
			return [];
		}

		/**
		 * Normalizes provided input to be UUID as an instance of Buffer with 16
		 * octets at least.
		 *
		 * @param {Buffer|string} uuid UUID to be normalized
		 * @returns {Buffer} normalized UUID
		 */
		static normalizeUUID( uuid ) {
			return Services.OdemUtilityUuid.normalize( uuid );
		}

		/**
		 * Creates string representing provided UUID.
		 *
		 * @param {Buffer|string} uuid binary UUID to be represented as string or string already containing a UUID
		 * @returns {string} string representing UUID
		 */
		static formatUUID( uuid ) {
			if ( typeof uuid === "string" ) {
				if ( Services.OdemUtilityUuid.ptnUuid.test( uuid ) ) {
					return uuid;
				}

				throw new TypeError( `invalid UUID: ${uuid}` );
			}

			return Services.OdemUtilityUuid.format( uuid );
		}

		/**
		 * Generates data key related to given UUID suitable for selecting related
		 * record in data source connected via current adapter.
		 *
		 * @param {?(string|Buffer)} uuid UUID to be converted
		 * @returns {string} backend-compatible key for selecting related record there
		 */
		static uuidToKey( uuid ) {
			const _uuid = Services.OdemUtilityUuid.normalize( uuid );
			if ( _uuid ) {
				return `models/${this.name}/items/${Services.OdemUtilityUuid.format( _uuid )}`;
			}

			return `models/${this.name}/items/%u`;
		}

		/**
		 * Extracts UUID of some addressed instance from provided key.
		 *
		 * @param {string} key key used with data backend
		 * @returns {Buffer} UUID of this model's instance extracted from key, null if no UUID was found
		 */
		static keyToUuid( key ) {
			const result = PtnModelItemsKey.exec( key );
			if ( result ) {
				return Buffer.from( result[2].replace( /-/g, "" ), "hex" );
			}

			throw new Error( "invalid key to extract UUID from" );
		}

		/**
		 * Extracts name of model addressed by provided key.
		 *
		 * @param {string} key key used with data backend
		 * @returns {?string} name of model addressed by given key, null if key does not address any model
		 */
		static keyToModelName( key ) {
			const match = PtnModelItemsKey.exec( key );

			return match ? match[1] : null;
		}

		/**
		 * Tests if backend contains data of current item or not.
		 *
		 * @returns {Promise<boolean>} promises true if data exists and false otherwise
		 */
		get $exists() {
			if ( this.$isNew ) {
				return Promise.resolve( false );
			}

			if ( !this.constructor.adapter.can.test ) {
				throw new Error( "adapter can not test existance of entry" );
			}

			return this.constructor.adapter.has( this.$dataKey );
		}

		/**
		 * Exposes schema of abstract base class.
		 *
		 * @note This schema is basically empty for base class does not handle data
		 *       itself but serves as an abstract base class to actually defined
		 *       models.
		 *
		 * @returns {{computed: {}, hooks: {}, props: {}}} empty schema definition
		 */
		static get schema() {
			return { props: {}, computed: {}, hooks: {} };
		}

		/**
		 * Exposes stub for receiving notifications regarding current model.
		 *
		 * @returns {EventEmitter} emitter for notifications on behalf of current model
		 */
		static get notifications() {
			if ( !this._notifications ) {
				const notifications = new EventEmitter();

				notifications.setMaxListeners( config.database.maxListenersPerModel );

				Object.defineProperty( this, "_notifications", {
					value: notifications
				} );
			}

			return this._notifications;
		}

		/**
		 * Observes current model's adapter for remote changes to be adopted in
		 * locally managed indices.
		 *
		 * @returns {void}
		 */
		static observeBackend() {
			if ( !this._isObservingAdapter ) {
				Object.defineProperty( this, "_isObservingAdapter", { value: true } );

				if ( !this.adapter.can.watch ) {
					throw new Error( "adapter can not watch entries" );
				}

				let prematureUpdates = {};
				let indices;
				let numIndices;

				/**
				 * Updates indices of current model based on change notification
				 * providing updated set of data for item identified by its
				 * binary UUID.
				 *
				 * @param {Buffer} uuid UUID of item that has been created, changed or removed
				 * @param {Object<string,any>|undefined} data serialized properties of created/changed item, falsy on removed item
				 * @returns {void}
				 */
				const updateIndex = ( uuid, data ) => {
					if ( !data ) {
						// item has been removed ... remove item from every
						// index defined on a property
						for ( let i = 0; i < numIndices; i++ ) {
							const { property, handler } = indices[i];

							logDebug( "removing %s from index of %s.%s", uuid, this.name, property );

							handler.remove( uuid );
						}

						return;
					}

					// item has been updated ... need to convert serialized data
					// into model properties with typed values as defined in its
					// schema
					const { computed, props } = this.schema;
					let context = null;

					const propNames = Object.keys( props );
					const numPropNames = propNames.length;
					const record = {};

					for ( let i = 0; i < numPropNames; i++ ) {
						const propName = propNames[i];
						const prop = props[propName];

						record[propName] = prop.$type.deserialize( data[propName] );
					}

					// update every index defined on a property
					for ( let i = 0; i < numIndices; i++ ) {
						const { property, handler } = indices[i];
						const computedInfo = computed[property];
						let newValue;

						logDebug( "updating index of %s.%s after change of %s", this.name, property, uuid );

						if ( computedInfo ) {
							// required: computed value in context of updated record
							if ( !context ) {
								context = new Proxy( new this( uuid ), {
									get( target, prop ) {
										if ( prop === "$properties" ) {
											return record;
										}

										if ( props[prop] ) {
											return record[prop];
										}

										return target[prop];
									},
								} );
							}

							newValue = computedInfo.code.call( context );
						} else {
							newValue = record[property];
						}

						handler.update( uuid, null, newValue, { searchExisting: true, addIfMissing: true } );
					}
				};

				// Postpone all index updates due to remote changes reported by
				// the adapter until after the indices have been created.
				// (needs to be deferred to prevent regression issue on reading indexLoaded)
				setTimeout( () => {
					this.indexLoaded
						.then( _indices => {
							indices = _indices;
							numIndices = indices.length;

							if ( numIndices > 0 ) {
								for ( const key of Object.keys( prematureUpdates ) ) {
									const { uuid, data } = prematureUpdates[key];

									updateIndex( uuid, data );
								}
							}

							prematureUpdates = undefined;
						} )
						.catch( cause => {
							prematureUpdates = undefined;

							logAlert( "indices of %s are broken, updating them on remote change is disabled", this.name, cause );
						} );
				} );

				// Update local indices and cache on items appearing or changing
				// in connected data source.
				this.adapter.on( "change", ( key, data, before ) => {
					const match = PtnModelItemsKey.exec( key );
					if ( !match || match[1] !== this.name ) {
						// ignore notifications not related to current model
						return;
					}

					logDebug( "NOTIFICATION: %s has changed in backend", match[2] );

					const uuid = Buffer.from( match[2].replace( /-/g, "" ), "hex" );

					if ( uuid.length !== 16 ) {
						logError( "ignoring adapter's change notification with invalid size of UUID" );
						return;
					}

					uuid.toString = Services.OdemUtilityUuid.toString;

					const formattedUuid = String( uuid );

					if ( prematureUpdates ) {
						prematureUpdates[formattedUuid] = { uuid, data };
					} else if ( numIndices > 0 ) {
						updateIndex( uuid, data );
					}

					if ( this._cache ) {
						this._cache[formattedUuid] = data;
					}

					if ( this._notifications ) {
						const asyncGeneratorFn = async() => {
							const instance = new this( uuid );

							await instance._injectRecord( data );

							return instance;
						};

						if ( before ) {
							this._notifications.emit( "changed", uuid, data, before.value, asyncGeneratorFn );
						} else {
							this._notifications.emit( "created", uuid, data, asyncGeneratorFn );
						}
					}
				} );

				// Update local indices and cache on items vanishing in
				// connected data source.
				this.adapter.on( "delete", key => {
					const match = PtnModelItemsKey.exec( key );
					if ( !match || match[1] !== this.name ) {
						return;
					}

					logDebug( "NOTIFICATION: %s has been removed from backend", match[2] );

					const uuid = Buffer.from( match[2].replace( /-/g, "" ), "hex" );

					if ( uuid.length !== 16 ) {
						logError( "invalid size of UUID in adapter's change notification" );
						return;
					}

					uuid.toString = Services.OdemUtilityUuid.toString;

					const formattedUuid = String( uuid );

					if ( prematureUpdates ) {
						prematureUpdates[formattedUuid] = { uuid };
					} else if ( numIndices > 0 ) {
						updateIndex( uuid, false );
					}

					if ( this._cache ) {
						// unset cache record ...
						this._cache[formattedUuid] = undefined;

						// ... leaving a "hole" to be removed by compacting the cache from time to time
						if ( ++this._cacheHoleCounter >= 100 ) {
							this._cache = true; // cache gets compacted on assigning something
						}
					}

					if ( this._notifications ) {
						this._notifications.emit( "removed", uuid );
					}
				} );
			}
		}

		/**
		 * Implements default hook invoked first in every item's constructor.
		 *
		 * @note Creating item does not refer to creating record in an attached data
		 *       storage, but constructing new instance of `Model` at runtime.
		 *
		 * @note Returning promise is not available here due to being invoked in
		 *       constructor of model instance.
		 *
		 * @param {?(Buffer|string)} uuid UUID of item to be represented by instance, null for starting new item
		 * @param {Object<string,string>} options options provided for new instance
		 * @returns {{uuid:(Buffer|string), options:Object<string,string>}} provided information, probably adjusted
		 */
		beforeCreate( { uuid = null, options = {} } = {} ) {
			return { uuid, options };
		}

		/**
		 * Implements default hook invoked after having created item.
		 *
		 * @note Creating item does not refer to creating record in an attached data
		 *       storage, but constructing new instance of `Model` at runtime.
		 *
		 * @note Returning promise is not available here due to being invoked in
		 *       constructor of model instance.
		 *
		 * @returns {void}
		 */
		afterCreate() {} // eslint-disable-line no-empty-function

		/**
		 * Implements default hook invoked before loading item's record from attached
		 * data storage.
		 *
		 * @returns {undefined|Promise} optional promise settled when hook has finished
		 */
		beforeLoad() {} // eslint-disable-line no-empty-function

		/**
		 * Implements default hook invoked after having loaded item's record from
		 * attached data storage.
		 *
		 * @param {object} record raw record as read from data storage
		 * @returns {object} record to use eventually for setting properties
		 */
		afterLoad( record ) { return record; }

		/**
		 * Implements default hook invoked before validating properties of items.
		 *
		 * @returns {undefined|Error[]|Promise<Error[]>} list of errors encountered by hook
		 */
		beforeValidate() {} // eslint-disable-line no-empty-function

		/**
		 * Implements default hook invoked after having validated properties of
		 * items.
		 *
		 * @param {Error[]} errors lists error encountered while validating
		 * @returns {Error[]} probably filtered list of validation errors
		 */
		afterValidate( errors ) { return errors; }

		/**
		 * Implements default hook invoked before saving validated properties of
		 * item.
		 *
		 * @param {boolean} existsAlready true if item exists in backend already
		 * @param {object} record record of current item's serialized property values to be written in backend
		 * @param {boolean} isNew true if item has been created with a freshly assigned UUID
		 * @returns {object} item's record to be written in backend eventually
		 */
		beforeSave( existsAlready, record, isNew ) { // eslint-disable-line no-unused-vars
			return record;
		}

		/**
		 * Implements default hook invoked after saving validated properties of
		 * item.
		 *
		 * @param {boolean} existsAlready true if item exists in backend already
		 * @param {boolean} isNew true if item has been created with a freshly assigned UUID
		 * @returns {undefined|Promise} optional promise settled when hook finished
		 */
		afterSave( existsAlready, isNew ) {} // eslint-disable-line no-unused-vars,no-empty-function

		/**
		 * Implements default hook invoked before removing item from backend.
		 *
		 * @returns {undefined|Promise} optional promise settled when hook has finished
		 */
		beforeRemove() {} // eslint-disable-line no-empty-function

		/**
		 * Implements default hook invoked after removing item from backend.
		 *
		 * @returns {undefined|Promise} optional promise settled when hook has finished
		 */
		afterRemove() {} // eslint-disable-line no-empty-function

		/**
		 * Adjusts current instance to use provided record instead of fetching
		 * it from a data source attached via some adapter.
		 *
		 * @param {Object<string,any>} record custom record item's properties are fetched from instead of attached data source
		 * @returns {Promise<Model>} promise for current item with properties fetched
		 */
		_injectRecord( record ) {
			if ( !this.$loaded ) {
				if ( !this.uuid ) {
					return Promise.reject( new Error( "invalid injection of record into item without UUID" ) );
				}

				this.$loaded = Promise.resolve()
					.then( () => this.beforeLoad() )
					.then( () => this.afterLoad( record ) );
			}

			return this.$loaded;
		}

		/**
		 * Loads data of item from backend.
		 *
		 * @returns {Promise<Model>} promises model instance with properties loaded from storage
		 */
		load() {
			if ( !this.$loaded ) {
				if ( !this.uuid ) {
					return Promise.reject( new Error( "can not load item without UUID" ) );
				}

				const { adapter, _cache } = this.constructor;

				this.$loaded = Promise.resolve()
					.then( () => this.beforeLoad() )
					.then( () => {
						if ( _cache && _cache[this.uuid] && ( !adapter.isInATransaction || !adapter.can.read ) ) {
							return _cache[this.uuid];
						}

						if ( !adapter.can.read ) {
							throw new Error( "adapter can not read entry" );
						}

						return adapter.read( this.$dataKey ).then( record => {
							if ( _cache && record ) {
								// successively feed the cache
								_cache[this.uuid] = record;
							}

							return record;
						} );
					} )
					.then( record => this.afterLoad( record ) );
				// NOTE Properties are replaced in setter of `this.$loaded`.
			}

			return this.$loaded;
		}

		/**
		 * Writes data of item to backend.
		 *
		 * @param {boolean} ignoreUnloaded set true to permit saving record w/o loading first
		 * @returns {Promise<Model>} promises instance of model with its properties saved to persistent storage
		 */
		save( { ignoreUnloaded = false } = {} ) {
			const { constructor, $isNew: isNew } = this;
			const adapter = constructor.adapter;
			let load;

			if ( !isNew && !this.$loaded && !ignoreUnloaded ) {
				if ( this.$properties.$context.changed.size > 0 ) {
					return Promise.reject( new Error( "saving unloaded item rejected" ) );
				}

				return Promise.resolve( this );
			}

			if ( !isNew && this.$loaded ) {
				load = this.$loaded.then( () => this.$properties.$context.changed.size > 0 );
			} else {
				load = Promise.resolve( true );
			}

			return load.then( hasChanged => {
				if ( !hasChanged ) {
					return this;
				}

				if ( !adapter.can.write ) {
					throw new Error( "adapter can not write entry" );
				}

				return this.validate()
					.catch( severeError => [severeError] )
					.then( validationErrors => {
						if ( validationErrors && validationErrors.length > 0 ) {
							throw Object.assign( new Error( `saving invalid properties rejected (${validationErrors.map( e => e.message ).join( ", " )})` ), {
								validationErrors,
							} );
						}

						if ( isNew ) {
							return false;
						}

						return this.$exists;
					} )
					.then( existsAlready => Promise.resolve()
						.then( () => {
							if ( typeof constructor._serializeProperties === "function" ) {
								return constructor._serializeProperties( this.$properties );
							}

							return this.$properties;
						} )
						.then( record => {
							this.$properties.$context.relax();
							return this.beforeSave( existsAlready, record, isNew );
						} )
						.then( record => {
							if ( existsAlready ) {
								return constructor.indexLoaded
									.then( indices => {
										const length = indices.length;
										if ( length ) {
											const { computed, props } = constructor.schema;
											let oldContext = null;

											for ( let i = 0; i < length; i++ ) {
												const { property, handler } = indices[i];
												const computedInfo = computed[property];
												let newProp, oldProp;

												if ( computedInfo ) {
													// required: old value of computed property for updating index
													// -> need to re-compute it when bound to old set of actual properties
													if ( !oldContext ) {
														const oldProperties = this.$properties.$context.clone().$context.rollBack();

														oldContext = new Proxy( this, {
															get( target, prop ) {
																if ( prop === "$properties" ) {
																	return oldProperties;
																}

																if ( props[prop] ) {
																	return oldProperties[prop];
																}

																return target[prop];
															},
														} );
													}

													oldProp = computedInfo.code.call( oldContext );
													newProp = this[property];
												} else {
													newProp = this.$properties[property];
													oldProp = this.$properties.$context.changed.has( property ) ?
														this.$properties.$context.changed.get( property ) :
														this.$properties[property];
												}

												if ( newProp !== oldProp ) {
													handler.update( this.$uuid, oldProp, newProp );
												}
											}
										}

										return adapter.write( this.$dataKey, record );
									} );
							}

							return constructor.indexLoaded.then( indices => {
								return ( isNew ? adapter.create( constructor.uuidToKey(), record ) : adapter.write( this.$dataKey, record ) )
									.then( dataKey => {
										if ( isNew ) {
											const uuid = constructor.keyToUuid( dataKey );
											if ( !uuid ) {
												throw new Error( "first-time saving instance in backend did not yield proper UUID" );
											}

											this.uuid = uuid;

											Object.defineProperties( this, {
												$dataKey: { value: constructor.uuidToKey( uuid ) },
											} );
										}

										const $uuid = this.$uuid;
										const numIndices = indices.length;

										for ( let i = 0; i < numIndices; i++ ) {
											const { property, handler } = indices[i];
											const { computed } = constructor.schema;

											const newProp = computed[property] ? this[property] : this.$properties[property];

											handler.add( $uuid, newProp );
										}
									} );
							} );
						} )
						.then( () => this.afterSave( existsAlready, isNew ) )
						.then( () => {
							// clear marks on changed properties for having
							// saved them right before
							this.$properties.$context.commit();
							this.$properties.$context.relax( false );
						} )
						.catch( error => {
							this.$properties.$context.relax( false );
							throw error;
						} )
						.then( () => this )
					);
			} );
		}

		/**
		 * Removes item from backend.
		 *
		 * @returns {Promise<Model>} promises model instance being removed from backend
		 */
		remove() {
			if ( !this.constructor.adapter.can.remove ) {
				throw new Error( "adapter can not remove entry" );
			}

			return Promise.resolve()
				.then( () => {
					this.$properties.$context.relax();

					return this.beforeRemove();
				} )
				.then( () => {
					this.$properties.$context.relax( false );

					return this.constructor.indexLoaded;
				} )
				.then( indices => {
					const length = indices.length;

					if ( length ) {
						return this.load().then( () => {
							for ( let i = 0; i < length; i++ ) {
								const { handler, property } = indices[i];

								if ( this.$isMarkedLoaded ) {
									handler.removeValue( this.$uuid, this[property] );
								} else {
									// do not know the properties' values -> have to
									// search the whole index for removing UUID
									handler.remove( this.$uuid );
								}
							}
						} );
					}

					return undefined;
				} )
				.then( () => this.constructor.adapter.remove( this.$dataKey ) )
				.then( () => this.afterRemove() )
				.then( () => this );
		}

		/**
		 * Validates current set of properties.
		 *
		 * @returns {Promise<Error[]>} promises list of validation errors
		 */
		validate() {
			return Promise.resolve( [] );
		}

		/**
		 * Extracts item's values per attribute and computed attribute as well as
		 * its UUID to regular object.
		 *
		 * @param {boolean} omitComputed set true to extract actual properties and UUID, only
		 * @param {boolean} serialized set true to get all properties of resulting object suitable for serialisation e.g. as stringified JSON
		 * @returns {object} object providing item's UUID and values of its properties
		 */
		toObject( { omitComputed = false, serialized = false } = {} ) {
			const { props, computed } = this.constructor.schema;
			const output = {};
			let names;

			if ( !omitComputed ) {
				names = Object.keys( computed );
				for ( let i = 0, length = names.length; i < length; i++ ) {
					const name = names[i];
					const property = this[name];

					if ( property != null ) {
						if ( serialized && computed[name].$type ) {
							output[name] = computed[name].$type.serialize( property, Services.OdemAdapter );
						} else {
							output[name] = property;
						}
					}
				}
			}

			names = Object.keys( props );
			for ( let i = 0, length = names.length; i < length; i++ ) {
				const name = names[i];
				const property = this.$properties[name];

				if ( property != null ) {
					if ( serialized ) {
						output[name] = props[name].$type.serialize( property, Services.OdemAdapter );
					} else {
						output[name] = property;
					}
				}
			}

			output.uuid = this.uuid;

			return output;
		}

		/**
		 * Imports item's values per property and computed property from provided
		 * regular object.
		 *
		 * @param {object} data regular object to import properties from
		 * @param {boolean} omitComputed set true to process actual properties and UUID, only
		 * @param {boolean} serialized set true to indicate that object contains all properties in a form suitable for serialisation e.g. as stringified JSON
		 * @returns {this} fluent interface
		 */
		fromObject( data, { omitComputed = false, serialized = false } = {} ) {
			const { props, computed } = this.constructor.schema;
			let names, targets;

			names = Object.keys( props );
			targets = this.$properties;

			for ( let i = 0, length = names.length; i < length; i++ ) {
				const name = names[i];
				const source = data[name];

				if ( source !== undefined ) {
					targets[name] = serialized ? props[name].$type.deserialize( source ) : source;
				}
			}

			if ( !omitComputed ) {
				this.$properties.$context.relax();

				names = Object.keys( computed );
				targets = this; // eslint-disable-line consistent-this

				for ( let i = 0, length = names.length; i < length; i++ ) {
					const name = names[i];
					const source = data[name];

					if ( source !== undefined ) {
						targets[name] = serialized && computed[name].$type ? computed[name].$type.deserialize( source ) : source;
					}
				}

				this.$properties.$context.relax( false );
			}

			return this;
		}

		/**
		 * Wraps execution of provided callback in a transaction. The callback
		 * is invoked with all available models bound to that transaction. It is
		 * mandatory to only use those models for the transaction to work.
		 *
		 * @param {(models: Object<string,Hitchy.Odem.Model>) => (void|Promise<void>)} handler callback invoked to execute the transaction
		 * @param {Object} options transaction configuration
		 * @returns {Promise<void>} promise resolved if transaction succeeded, rejected otherwise
		 */
		static async transaction( handler, options = {} ) {
			if ( !this.adapter.can.transact ) {
				throw new Error( "adapter does not support transactions" );
			}

			await this.indexLoaded;

			await this.adapter.transaction( async tx => {
				const boundModels = {};

				for ( const name of Object.keys( api.models ) ) {
					boundModels[name] = ( ( modelName, OriginalModel ) => {
						let boundModel;

						// eslint-disable-next-line no-eval
						eval( `boundModel = class ${modelName} extends OriginalModel {};` );

						Object.defineProperty( boundModel, "adapter", { value: tx } );

						return boundModel;
					} )( name, api.models[name] );
				}

				await handler( boundModels );
			}, options );
		}

		/**
		 * Creates new item instance initialized with values per property and
		 * computed property provided as regular object.
		 *
		 * Resulting item's UUID can be provided in second argument or in property
		 * `uuid` of provided object.
		 *
		 * @param {object} data regular object to import properties from
		 * @param {string|Buffer} uuid UUID of item to use in preference over some property `uuid` in provided data object
		 * @param {boolean} omitComputed set true to process actual properties and UUID, only
		 * @param {boolean} serialized set true to indicate that object contains all properties in a form suitable for serialisation e.g. as stringified JSON
		 * @returns {Model} created item with properties set
		 */
		static fromObject( data, { uuid = null, omitComputed = false, serialized = false } = {} ) {
			const item = uuid || data.uuid ? new this( uuid || data.uuid ) : new this();

			return item.fromObject( data, { omitComputed, serialized } );
		}

		/**
		 * Fetches index handler matching named property and type of index.
		 *
		 * @param {string} property name of property to be covered by index
		 * @param {string} type test operation supported by index
		 * @returns {OdemModelIndexer|undefined} found index
		 */
		static getIndex( property, type = "eq" ) {
			const indices = this.indices;
			const numIndices = indices.length;

			for ( let i = 0; i < numIndices; i++ ) {
				const index = indices[i];

				if ( index.property === property && index.type === type ) {
					return index.handler;
				}
			}

			return undefined;
		}

		/**
		 * Creates stream of UUIDs with or without their entries from local
		 * cache unless caching has not been enabled.
		 *
		 * @note The stream does not produce any output prior to completely
		 *       setting up the cache on start.
		 *
		 * @param {boolean} withEntries if true, stream provides UUID and record of every cache entry, otherwise only UUIDs are provided
		 * @returns {undefined|module:stream.internal.Readable} readable stream of UUIDs or cache entries, undefined if cache has not been enabled
		 */
		static streamFromCache( withEntries = false ) {
			const { _cacheLive, _cache } = this;

			if ( _cacheLive && ( !this.adapter.isInATransaction || !this.adapter.can.stream ) ) {
				const uuids = Object.keys( _cache );
				let index = 0;

				return new Readable( {
					objectMode: true,
					read() {
						while ( index < uuids.length ) {
							const uuid = uuids[index++];
							const record = _cache[uuid];

							if ( record && this.push( withEntries ? { uuid, record } : uuid ) === false ) {
								return;
							}
						}

						this.push( null );
					}
				} );
			}

			if ( !this.adapter.can.stream ) {
				throw new Error( "adapter can not list entries" );
			}

			return undefined;
		}

		/**
		 * Retrieves stream of UUIDs of current model's instances.
		 *
		 * @returns {Readable} readable stream with UUIDs of model's instances
		 */
		static uuidStream() {
			const { OdemUtilityUuid } = Services;
			const { keyToUuid } = this;
			const fromCache = this.streamFromCache();
			const keyStream = fromCache ?? this.adapter.stream( {
				prefix: `models/${this.name}/items/`,
				target: "key"
			} );

			const uuidStream = new Transform( {
				objectMode: true,
				transform: fromCache ? function( key, _, done ) {
					this.push( key );
					done();
				} : function( key, _, done ) {
					this.push( OdemUtilityUuid.format( keyToUuid( key ) ) );
					done();
				},
			} );

			uuidStream.on( "close", () => {
				keyStream.unpipe( uuidStream );
				keyStream.pause();
				keyStream.destroy();
			} );

			keyStream.pipe( uuidStream );

			return uuidStream;
		}

		/**
		 * Retrieves stream of UUIDs and related record of current model's
		 * instances.
		 *
		 * @returns {Readable} readable stream objects each providing an item's UUID and record
		 */
		static instanceStream() {
			const { OdemUtilityUuid } = Services;
			const { keyToUuid, _cache, _cacheLive } = this;
			const fromCache = this.streamFromCache( true );
			const entryStream = fromCache ?? this.adapter.stream( {
				prefix: `models/${this.name}/items/`,
				target: "entry"
			} );

			if ( !fromCache && !_cacheLive && _cache ) {
				// after streaming all instances via the adapter the cache is
				// assumed to cover all items of data source and thus is marked
				// "live" for being ready to use eventually
				entryStream.once( "end", () => {
					this._cacheLive = true;
				} );
			}

			const instanceStream = new Transform( {
				objectMode: true,
				transform( entry, _, done ) {
					if ( fromCache ) {
						this.push( entry );
					} else {
						const uuid = OdemUtilityUuid.format( keyToUuid( entry.key ) );
						const record = entry.value;

						if ( !_cacheLive && _cache && record ) {
							_cache[uuid] = record;
						}

						this.push( { uuid, record } );
					}

					done();
				},
			} );

			instanceStream.on( "close", () => {
				entryStream.unpipe( instanceStream );
				entryStream.pause();
				entryStream.destroy();
			} );

			entryStream.pipe( instanceStream );

			return instanceStream;
		}

		/**
		 * Unconditionally lists existing items of model.
		 *
		 * @note This method is basically an alias for finding records using special
		 *       test selecting every record of model.
		 *
		 * @param {int} offset number of items to skip
		 * @param {int} limit maximum number of items to retrieve
		 * @param {string} sortBy names property of model matching records shall be sorted by
		 * @param {boolean} sortAscendingly set true to sort matches in ascending order, set false for descending order
		 * @param {boolean} loadRecords set false to omit loading properties per matching item prior to returning them
		 * @param {?{count:int}} metaCollector object receiving meta information on return
		 * @returns {Promise<Model[]>} promises fetched instances of model
		 */
		static list( { offset = 0, limit = Infinity, sortBy = null, sortAscendingly = true } = {}, { loadRecords = true, metaCollector = null } = {} ) {
			return this.find(
				{ true: {} },
				{ offset, limit, sortBy, sortAscendingly },
				{ loadRecords, metaCollector }
			);
		}

		/**
		 * Aliases Model#find().
		 *
		 * @note This method was exposed in previous versions and is kept for
		 * compatibility reasons. Is is meant to vanish in a future release.
		 *
		 * @param {string} name names attribute/property to check per record
		 * @param {*} value provides value to compare with per record
		 * @param {string} operation names operation to use for comparing per record
		 * @param {int} offset number of leading matches to skip
		 * @param {int} limit maximum number of matches to retrieve
		 * @param {?{count:int}} metaCollector object receiving meta information on return
		 * @param {boolean} loadRecords set false to get instances of matching records left to be loaded later
		 * @returns {Promise<Model[]>} resulting matches
		 * @deprecated
		 */
		static findByAttribute( name, value = null, operation = "eq",
			{ offset = 0, limit = Infinity } = {},
			{ metaCollector = null, loadRecords = true } = {} ) {
			return this.find( { [operation]: { name, value } }, { offset, limit }, { loadRecords, metaCollector } );
		}

		/**
		 * Searches collection of current model for items matching described test.
		 *
		 * @param {object} test description of test operation to identify records to fetch
		 * @param {int} offset number of leading matches to skip
		 * @param {int} limit maximum number of matches to retrieve
		 * @param {string} sortBy names property resulting matches should be sorted by
		 * @param {boolean} sortAscendingly set true to sort in ascending order, false for descending order
		 * @param {?{count:int}} metaCollector object receiving meta information on return
		 * @param {boolean} loadRecords set false to get instances of matching records left to be loaded later
		 * @returns {Promise<Model[]>} resulting matches
		 */
		static async find( test,
			{ offset = 0, limit = Infinity, sortBy = null, sortAscendingly = true } = {},
			{ metaCollector = null, loadRecords = true } = {} ) {
			await this.indexLoaded;

			return new Promise( ( resolve, reject ) => {
				let source = this.processTerm( test, sortBy, sortAscendingly, loadRecords );

				if ( sortBy ) {
					const sortIndex = this.getIndex( sortBy, "eq" );
					let sorted;

					if ( sortIndex ) {
						sorted = new Services.OdemModelSorterIndexed( sortIndex, sortAscendingly );
					} else {
						sorted = new Services.OdemModelSorterNonIndexed( this, sortBy, sortAscendingly );
					}

					const origSource = source;

					sorted.on( "close", () => {
						origSource.unpipe( sorted );
						origSource.pause();
						origSource.destroy();
					} );

					source.pipe( sorted );

					source = sorted;
				}

				const collected = new Array( Math.min( limit, 1000 ) );
				let count = 0;
				let written = 0;
				let _offset = offset;
				let _limit = limit;

				source.on( "data", item => {
					count++;

					if ( _offset > 0 ) {
						_offset--;
					} else if ( _limit > 0 ) {
						_limit--;

						collected[written++] = item;
					} else if ( !metaCollector ) {
						process.nextTick( () => {
							source.pause();
							source.destroy();

							// destroying source prevents emission of end event
							source.emit( "end" );
						} );
					}
				} );

				source.on( "end", () => {
					if ( metaCollector ) {
						metaCollector.count = count;
					}

					collected.splice( written );

					if ( loadRecords ) {
						// collect all unloaded items
						let unloaded;
						let write = 0;

						for ( let i = 0; i < written; i++ ) {
							const item = collected[i];

							if ( !item.$isMarkedLoaded ) {
								if ( !unloaded ) {
									const size = written - i;
									unloaded = new Array( Math.min( size, 1000 ) );
								}

								unloaded[write++] = item.load();
							}
						}

						if ( unloaded ) {
							unloaded.splice( write );

							Promise.all( unloaded )
								.then( () => resolve( collected ) )
								.catch( reject );
							return;
						}
					}

					resolve( collected );
				} );

				source.on( "error", reject );
			} );
		}

		/**
		 * Compiles test from provided description and returns its stream of matching items.
		 *
		 * @param {object} testDescription description of test used to pick instances
		 * @param {string} sortBy name of property finally resulting list of matches will be sorted by (provided to help optimizing tester)
		 * @param {boolean} sortAscendingly true if provided property will be used eventually to sort in ascending order (provided to help optimizing tester)
		 * @param {boolean} loadRecords indicates whether caller intends to load matching items afterward or not (as a hint to possibly improve the processing)
		 * @returns {Readable<Model>} stream of instances matching given test
		 */
		static processTerm( testDescription, sortBy = null, sortAscendingly = true, loadRecords = false ) {
			return Services.OdemModelTester.fromDescription( this, testDescription, null, { sortBy, sortAscendingly, loadRecords } ).createStream();
		}

		/**
		 * Resolves as soon as all defined indices of model are available.
		 *
		 * @return {Promise<Hitchy.Plugin.Odem.ModelIndexer[]>} promises list of model's prepared indices
		 */
		static get indexLoaded() {
			if ( !this.indexPromise ) {
				const { adapter, indices, _cache } = this;
				const numIndices = indices.length;
				let promise;

				if ( !adapter.can.stream ) {
					throw new Error( "adapter can not stream entries" );
				}

				this.observeBackend();

				if ( numIndices > 0 || config.database.eagerCache ) {
					const stream = adapter.stream( {
						prefix: `models/${this.name}/items/`,
						invalidPolicy: "skip",
						target: "entry",
					} );
					let count = 0;
					let step = 100;

					promise = PromiseUtils.process( stream, async( { key, value } ) => {
						const uuid = this.keyToUuid( key );

						if ( !value ) {
							logError( "ignoring falsy record %s of model %s", Services.OdemUtilityUuid.format( uuid ), this.name );
							return;
						}

						const item = new this( uuid );
						const { $uuid } = item;

						await item._injectRecord( value );

						for ( let i = 0; i < numIndices; i++ ) {
							const { property, handler } = indices[i];

							handler.add( $uuid, item[property] );
						}

						if ( _cache ) {
							_cache[item.uuid] = value;
						}

						if ( ++count % step === 0 ) {
							logDebug( "having indexed%s %d records of model %s", _cache ? "/cached" : "", count, this.name );

							if ( count / step >= 10 ) {
								step *= 10;
							}
						}
					} )
						.then( () => {
							logDebug( "having indexed%s %d records of model %s eventually", _cache ? "/cached" : "", count, this.name );

							if ( _cache ) {
								this._cacheLive = true;
							}

							if ( env.NODE_ENV !== "production" ) {
								const failed = [];

								for ( let i = 0; i < numIndices; i++ ) {
									const index = indices[i];

									try {
										index.handler.checkIntegrity();
									} catch ( error ) {
										failed.push( `${index.type} on ${this.name}.${index.property} (${this.schema.computed[index.property] ? `${this.schema.computed[index.property].type} computed` : this.schema.props[index.property].type})` ); // eslint-disable-line max-len
										logError( "integrity check of %s index on %s.%s failed: %s", index.type, this.name, index.property, error.message );
									}
								}

								if ( failed.length > 0 ) {
									throw new Error( "failed integrity check(s) on: " + failed.join( ", " ) );
								}
							}

							return indices;
						} );
				} else {
					promise = Promise.resolve( indices );
				}

				Object.defineProperty( this, "indexPromise", { value: promise } );
			}

			return this.indexPromise;
		}

		/**
		 * Compiles provided schema into model class derived from Model or
		 * some explicitly provided model class.
		 *
		 * @param {string} modelName name of model
		 * @param {object} schema definition of model's schema
		 * @param {class} customBaseClass model class inheriting from Model
		 * @param {OdemAdapter} adapter selects adapter to use on instances of resulting model by default
		 * @returns {class} compiled model class
		 */
		static define( modelName, schema, customBaseClass = null, adapter = null ) {
			return Services.OdemModelCompiler.compileModel( modelName, schema, customBaseClass, adapter );
		}
	}

	return Model;
};
